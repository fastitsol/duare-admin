<?php
 include_once 'init.php';
/* $test_data = '{"item":{"data_1":{"_id":1,"res_id":1,"pro_id":1,"pro_name":"Sokaler Nasta d","p_quantity":2,"price":100,"discount":0,"tPrice":200},"data_2":{"_id":2,"res_id":18,"pro_id":9,"pro_name":"Beef Shick Kebab","p_quantity":1,"price":70,"discount":0,"tPrice":70},"data_3":{"_id":3,"res_id":18,"pro_id":8,"pro_name":"Chicken Tandoori","p_quantity":3,"price":80,"discount":0,"tPrice":240}},"invoice":{"address1":"roongmohl tower","address2":"room 313","csid":"88"}}';*/

  $json = file_get_contents('php://input');
  $data = json_decode($json);
    $invoice_items = null;

//  $array = json_decode(json_encode($data), true);
    
    // $invo = [];
    // $total_bill=0;
    $subTotal=0;
    // $total_discount=0;
    // $delivery_charge=0;
    // $payment_type='';
   
    if($data and $data->item){
        
        foreach($data->item as $single_cart){
            // $total_bill+=$single_cart->tPrice;
            $subTotal+=$single_cart->tPrice;
            // $total_discount+=$single_cart->discount;
        }

        // $invo = $data->invoice;
        // $delivery_charge = $invo->delivery_charge;
        // $payment_type = $invo->payment_type;
    }
   
    // echo"<pre>";
    // print_r($data);
    // echo "<pre>";
    // return;
    function send_notification ($tokens, $message)
    {

       
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
             'registration_ids' => $tokens,
             'notification' => $message

            );
        $headers = array(
            'Authorization:key = AAAA7tJ2NhU:APA91bGFaX2F37cibcQxX1kiZqr-C3rWwRc6qOsLhNuPJqGXsIzXkpuV3RaXxtlO6RoSiHOtB8tPJzXSGlY3aPA2kf48FfEfB69vgVlDrz79II0y21lBxXe1_0PHexRjUu7GjIPfVzmi',
            'Content-Type: application/json'
            );
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
    }

function make_nvoice($orderdetails, $subTotal, $shiping_info, $prescriptions){
    global $con;
    date_default_timezone_set("Asia/Dhaka");
    $datetime = date("Y-m-d H:i:s");
    $orderdetail = json_encode($orderdetails);
    $prescription = json_encode($prescriptions);
    $s_info = json_encode($shiping_info);

    // echo"<pre>";
    // print_r($shiping_info);
    // echo "<pre>";
    // return;
    
    $query="INSERT INTO `invoice` (`id`,`customer_id`,`orderdetails`,`prescriptionDetails`,`shiping_info`,`date_time`,`discount`,`subTotal`,`total_bill`,`delivery_charge`,`status`,`take_by`,`payment_type`,`lat_val`,`lang_val`) VALUES (NULL,'$shiping_info->csid','$orderdetail','$prescription','$s_info','$datetime','$shiping_info->discount','$subTotal','$shiping_info->total_bill','$shiping_info->delivery_charge','Placed',NULL,'$shiping_info->payment_type',NULL,NULL)" ;
    
    $exeQuery = mysqli_query($con, $query);
     $insert_id = mysqli_insert_id($con);
     return $insert_id;
}

function make_invoice_items($invoice_id, $items){
    global $con;
    $values=null;
    if($items){
        $counter=0;

        
        $total_item = count($items);
        foreach($items as $single_cart){
            $counter++;
            $res_id=$single_cart->res_id;
            $pro_id = $single_cart->pro_id;
            $qty = $single_cart->p_quantity;
            $price = $single_cart->price;
            $discount = $single_cart->discount;
            $total_price = $single_cart->tPrice;
            $query="INSERT INTO `invoice_items` (`id` ,`invoice_id`, `restaurant_id`, `product_id`, `qty`, `price`, `discount`, `total_amount`) VALUES (NULL, '$invoice_id', '$res_id', '$pro_id', '$qty', '$price', '$discount', '$total_price'); ";
             
            $exeQuery = mysqli_multi_query($con, $query) ;
        }
    }
}

if($data and $data->item and $data->invoice){

    if($invoice_id=make_nvoice($data->item, $subTotal, $data->invoice, $data->prescription)){
        make_invoice_items($invoice_id, $data->item);
        
        

        if($invoice_id){
            
                // $restaurant_id = "";
                // if(mysqli_num_rows($result) > 0 ){
                //     while ($row = mysqli_fetch_array($result)) {
                //     $restaurant_id = $row["restaurant_id"];
                //     echo"<pre>";
                // print_r($restaurant_id);
                // echo"<pre>";
                //     exit;
                //     }
                    // if($user_id){
                    //     $sql="SELECT * FROM `users` WHERE `id` = '$user_id'" ;
                    //     $result = mysqli_query($con,$sql);
                    //     while($row = mysqli_fetch_array($result))
                    //     {
                    //         array_push($response,array(
                    //             "user_id"=>$row[0],
                    //             "user_name"=>$row[3]
                    //         ));
                    //     }
                    // }
                // }

                // SELECT `restaurant_id` FROM `invoice_items` WHERE `invoice_id`=398 GROUP BY `restaurant_id`
                // $message = array("body" => "Please Process your Order", "title"=> "New Order Arrived");
                // $message_status = send_notification($tokens, $message);
                // echo $message_status;
            
            
            echo (json_encode(array('code' =>1, 'message' => 'Order Placed Success')));
        }
        else {echo(json_encode(array('code' =>2, 'message' => 'Something wrong')));
        }
    }
    else{
        echo(json_encode(array('code' =>2, 'message' => 'can not insert')));
    }

}else{
    echo(json_encode(array('code' =>3, 'message' => 'Something wrong')));
}

 ?>