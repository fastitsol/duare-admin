<?php


include_once 'init.php';

$sql = "SELECT tbl_products.name, tbl_products.product_type, tbl_products.image, tbl_products.description, tbl_products.price,tbl_restaurant.name FROM `tbl_products` INNER JOIN tbl_restaurant on tbl_products.restaurant_id = tbl_restaurant.id" ;

$result = mysqli_query($con,$sql);
$response = array();

while($row = mysqli_fetch_array($result))

{
 array_push($response,array("item_name"=>$row[0],"product_type"=>$row[1],"i_image"=>$row[3],"i_description"=>$row[5],"i_price"=>$row[6],"r_name"=>$row[7]));
}

echo json_encode(array("item_list"=>$response));

mysqli_close($con);

?>