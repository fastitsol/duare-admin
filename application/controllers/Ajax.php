<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('order_model');
        $this->load->model('restaurant_model');
        $this->load->model('Dashboard_model');
        $this->load->model('MedicineModel');
    }
    
    // this function will return all categor and products for this category
    public function get_cat_foods($restaurant_id=null){
        $data=array();
        $cats=array();
        $products=array();
        if($restaurant_id and is_numeric($restaurant_id) and $categories = $this->product_model->category($restaurant_id)){
            if($categories){
                foreach($categories as $cat){
                    $data[]=array(
                        'id'             =>$cat->id,
                        'restaurant_id'  =>$cat->restaurant_id,
                        'name'           =>$cat->name,
                        'products'       =>$this->category_products($cat->id, 'food'),
                        'total_products' =>$this->category_products_count($cat->id, 'food'),
                    );
                }
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
    private function category_products($cat, $type){
        $products=array();
        if($products = $this->product_model->products_by_category($cat, $type)){
            return $products;
        }else{
            return null;
        }
    }

    
    private function category_products_count($cat, $type){
        $products=array();
        if($product = $this->product_model->products_by_category_count($cat, $type)){
            return $product;
        }else{
            return 0;
        }
    }
    
    
    // this method for get delivery man
    public function get_delivery_man(){
        $data=null;
        $data=$this->user_model->get_all_user('deliver_man', null,  null);
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
    
    //this function for get placed order
    //Status 
    //Placed
    //Processing
    //Pickup
    //Delivered
    public function orders($status=null, $restaurant_id=null){
        $data['server_response'] = $this->order_model->orders($status, null, null, $restaurant_id);
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
    public function orders_by_status($restaurant_id=null){
        
        $data['server_response']=array();
        $data['server_response']['placed'] =  $this->order_model->orders('Placed', null, null, $restaurant_id);
        $data['server_response']['delivered'] =  $this->order_model->orders('Delivered', null, null, $restaurant_id);
        $data['server_response']['pickup'] =  $this->order_model->orders('Pickup', null, null, $restaurant_id);
        $data['server_response']['processing'] =  $this->order_model->orders('Processing', null, null, $restaurant_id);
        
        header('Content-Type: application/json');
        echo json_encode($data);
        
    }
    
    //get Invoice items
    public function order_items($order_id=null){
        header('Content-Type: application/json');
        $data=null;
        if(!$order_id or !is_numeric($order_id)){
            echo json_encode($data);
            return null;
        }
        $data = $this->order_model->invoice_items($order_id);
        echo json_encode($data);
    }
    
    
    public function get_delivery_area_places(){
        $places=array();
        $delivery_places=array();

        $datas = $this->restaurant_model->delivery_areas();
       
        foreach($datas as $data){
                    $places[]=array(
                        'area_id'       =>$data->area_id,
                        'area_name'  =>$data->area_name,
                        'delivery_places'  =>$this->restaurant_model->delivery_areas_place($data->area_id),
                        
                    );
              
        }
        
        header('Content-Type: application/json');
        echo json_encode($places);
    }

    public function shopDashboard($restaurant_id=null)
	{
         
        // $restaurant_id = $this->restaurant_id;
        
        // echo "<pre>";
        // print_r($restaurant_id);
        // exit;

        $restaurant_data = $this->restaurant_model->restaurant_by_id($restaurant_id);


        $order_received = $this->restaurant_model->orders_count('placed', $restaurant_id);

        $order_processing = $this->restaurant_model->orders_count('processing', $restaurant_id);

        $order_delivered = $this->restaurant_model->orders_count('delivered', $restaurant_id);


        $total_products = $this->restaurant_model->restaurants_product_counts_for_discount($restaurant_id);

        $total_available_products = $this->restaurant_model->total_available_products($restaurant_id);

        $all_order_javas = $this->restaurant_model->show_all_order_for_java_script($restaurant_id);
        

        $lastYearIncome = $this->restaurant_model->last_yearIncome($restaurant_id);

        $thisMonthIncome = $this->restaurant_model->thisMonthIncome($restaurant_id);

          // echo "<pre>";
          // print_r($lastYearIncome);
          // exit;
        $data  = array(
            'restaurant_data' => $restaurant_data ,
            'order_received' => $order_received ,
            'order_processing' => $order_processing,
            'order_delivered' => $order_delivered,
            'all_order_javas' => $all_order_javas,
            'total_products' => $total_products,
            'total_available_products' => $total_available_products,
            'thisYearIncomes'    => $lastYearIncome,
            'thisMonthIncomes'  => $thisMonthIncome

        );
        header('Content-Type: application/json');
        echo json_encode($data);

       
    }
    public function ResYearReport($restaurant_id=null)
    {
        $yearTotal=0;
        $orders = $this->restaurant_model->resthisYearReport($restaurant_id);
        
        if($orders){
            foreach($orders as $order){
                $invoiceTotal = $order->total_bill - $order->delivery_charge;
                $yearTotal = $yearTotal + $invoiceTotal;
            }
        }
        
        $data  = array(
            'orders' => $orders, 
            'yearTotal' => $yearTotal, 
        );

        header('Content-Type: application/json');
        echo json_encode($data);

    }
    public function ResMonthlyReport($restaurant_id=null)
    
    {
        $monthTotal=0;
        $orders = $this->restaurant_model->resthisMonthReport($restaurant_id);
        if($orders){
            foreach($orders as $order){
                $invoiceTotal = $order->total_bill - $order->delivery_charge;
                $monthTotal = $monthTotal + $invoiceTotal ;
            }
        }
        // echo "<pre>";
        // print_r($this->restaurant_id);
        // echo "<pre>";
        // return;

        $data  = array(
            'orders' => $orders, 
            'monthTotal' => $monthTotal,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function ordersListByRestaurant($restaurant_id=null)
    
    {
        $Placed = 'Placed';
        $Processing  = 'Processing ';
        $Delivered = 'Delivered';
        $Pickup = 'Pickup';

        
        $placed  = $this->order_model->ordersByStatus($Placed, $restaurant_id);
        
        // foreach($placed as $p){
        //     // $placedOrderdetail = $p->invoice_id;
        //     $pp  = $this->order_model->ordersFromInvoiceItems($p->invoice_id, $restaurant_id);
        // }
        $processing  = $this->order_model->ordersByStatus($Processing, $restaurant_id);
        $pickup  = $this->order_model->ordersByStatus($Pickup, $restaurant_id);
        $delivered  = $this->order_model->ordersByStatus($Delivered, $restaurant_id);
       
        $data  = array(
            'placed' => $placed,
            // 'placedOrderdetail' => $placedOrderdetail,
            'processing' => $processing,
            'pickup' => $pickup,
            'delivered' => $delivered,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function ordersView($order_id = null,$shop_id=null)
    {

        $total_amount=0;
    	if ($order_id != null) {
    		$orders = $this->MedicineModel->orderInfoAllforApi($order_id,$shop_id);
    	}
        if($orders){
            foreach( $orders as $order){
                $invoiceTotal = $order->total_amount;
                $total_amount = $total_amount + $invoiceTotal ;
            }
        }
    	$data  = array(
    		'orders' => $orders, 
    		'priceTotal' => $total_amount, 
    	);

    	//  echo "<pre>";
    	//  print_r($data );
    	//  exit;
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function ordersViewForShop($order_id = null,$shop_id=null)
    {

        $total_amount=0;
    	if ($order_id != null) {
    		$orders = $this->MedicineModel->orderInfoForShop($order_id,$shop_id);
    	}
        if($orders){
            foreach( $orders as $order){
                $invoiceTotal = $order->purchasePrice * $order->qty;
                $total_amount = $total_amount + $invoiceTotal ;
            }
        }
    	$data  = array(
    		'orders' => $orders, 
    		'purchasePriceTotal' => $total_amount, 
    	);

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function editPriceForMedicine()
    {
        $Shop_id =  $this->input->post('restaurant_id');
        $invoice_id =  $this->input->post('invoice_id');

        $product_id  =  $this->input->post('product_id');
        $purchasePrice  =  $this->input->post('purchasePrice');

    	$product_price =  $this->input->post('sellingPrice');

        $qty  =  $this->input->post('qty');

        if(($invoice_id != null) &&($product_id != null) && ($purchasePrice != null) && ($product_price != null) && ($qty != null)&& ($Shop_id != null)){

            $product_price_for_all_productTable = $product_price;
            $purchasePrice_for_all_productTable = $purchasePrice;

            $product_price_for_InvoiceItem_withqty = 0;

            $product_price_for_InvoiceItem_withqty = ($product_price * $qty);


                   
            $updateAllProductTable = $this->MedicineModel->updateAllProductTable($product_id, $product_price_for_all_productTable, $purchasePrice_for_all_productTable);

            if ($updateAllProductTable == 1) {
            $updateInvoiceItemAmount = $this->MedicineModel->updateInvoiceItemAmount($invoice_id,$Shop_id,$product_id, $product_price_for_all_productTable, $product_price_for_InvoiceItem_withqty);

            echo (json_encode(array(
                "message" => 'updated successfully'
            )));
            
            }else{
                echo (json_encode(array("message" => 'something wrong')));
            } 
        }else{
            echo (json_encode(array("message" => 'Invalid Request')));
        }
        
    }

    public function orderListsForDeliveryMan($take_by_id)
    
    {
        $Placed = 'Placed';
        $Processing  = 'Processing ';
        $Delivered = 'Delivered';
        $Pickup = 'Pickup';

        // $placed  = $this->order_model->orders($Placed);
        // $processing  = $this->order_model->orders($Processing);
        // $pickup  = $this->order_model->orders($Pickup);
        // $delivered  = $this->order_model->orders($Delivered);
       
        $placed  = $this->order_model->ordersStatusByTake_by_id($Placed);
        $processing  = $this->order_model->ordersStatusByTake_by_id($Processing);
        $pickup  = $this->order_model->ordersStatusByTake_by_id($Pickup,$take_by_id);
        $delivered  = $this->order_model->ordersStatusByTake_by_id($Delivered,$take_by_id);
       
        $data  = array(
            'placed' => $placed,
            'processing' => $processing,
            'pickup' => $pickup,
            'delivered' => $delivered,
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function ordersViewForDeliveryMan($order_id = null)
    {

        $total_amount=0;
    	if ($order_id != null) {
    		$orders = $this->order_model->orderInfoAll($order_id);
    	}
        // if($orders){
        //     foreach( $orders as $order){
        //         $invoiceTotal = $order->total_amount;
        //         $total_amount = $total_amount + $invoiceTotal ;
        //     }
        // }
    	$data  = array(
    		'orders' => $orders, 
    		// 'priceTotal' => $total_amount, 
    	);

    	//  echo "<pre>";
    	//  print_r($data );
    	//  exit;
        header('Content-Type: application/json');
        echo json_encode($data);
    }


    public function pharmacyDashboard($restaurant_id=null)
	{
         
        // $restaurant_id = $this->restaurant_id;
        
        // echo "<pre>";
        // print_r($restaurant_id);
        // exit;

        $restaurant_data = $this->restaurant_model->restaurant_by_id($restaurant_id);


        $order_received = $this->restaurant_model->orders_count('placed', $restaurant_id);

        $order_processing = $this->restaurant_model->orders_count('processing', $restaurant_id);

        $order_delivered = $this->restaurant_model->orders_count('delivered', $restaurant_id);


        $total_products = $this->restaurant_model->restaurants_product_counts_for_discount($restaurant_id);

        $total_available_products = $this->restaurant_model->total_available_products($restaurant_id);

        $all_order_javas = $this->restaurant_model->show_all_order_for_java_script($restaurant_id);
        

        $lastYearIncome = $this->restaurant_model->last_yearIncome($restaurant_id);

        $thisMonthIncome = $this->restaurant_model->thisMonthIncome($restaurant_id);

          // echo "<pre>";
          // print_r($lastYearIncome);
          // exit;
        $data  = array(
            'restaurant_data' => $restaurant_data ,
            'order_received' => $order_received ,
            'order_processing' => $order_processing,
            'order_delivered' => $order_delivered,
            'all_order_javas' => $all_order_javas,
            'total_products' => $total_products,
            'total_available_products' => $total_available_products,
            'thisYearIncomes'    => $lastYearIncome,
            'thisMonthIncomes'  => $thisMonthIncome

        );
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function pharmacyYearReport($pharmacy_id=null)
    {
        $yearTotal= 0;
        $orders = $this->restaurant_model->resthisYearReport($pharmacy_id);

        // echo"<pre>";
        // print_r($orders);

        // if($orders){
        //     foreach($orders as $order){
        //         $invoiceTotal = $order->total_bill - $order->delivery_charge;
        //         $yearTotal = $yearTotal + $invoiceTotal;
        //     }
        // }

        $data  = array(
            'orders' => $orders, 
            // 'yearTotal' => $yearTotal, 
        );
    }
    public function viewDiscountOnMedicine($id)
    {

        $discount = $this->MedicineModel->discountInAll($id);

        $data  = array(
            'discounts' => $discount, 
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function AddDiscount()
    {
        $id = $this->input->post('pharmacy_id');
        $discountValue =  $this->input->post('discountValue');

        $date =  date('Y-m-d H:i');

        $data =  array(
            'restaurant_id' => $id, 
            'discountValue' => $discountValue, 
            'created_at' => $date 
        );
        $discount = $this->MedicineModel->discountInAll($id);

        if($discount == null)
        {   
            $add = "add";
            $insertDiscount = $this->MedicineModel->AddDiscountModel($data, $add, $id);
            if ($insertDiscount == 1) {
                $updateInAllProduct = $this->MedicineModel->updateInAllProduct($discountValue, $id);
                if ($updateInAllProduct == 1) {
                    echo (json_encode(array("message" => 'success')));
                }else{
                    echo (json_encode(array("message" => 'something wrong')));
                }  
            }else{
                echo (json_encode(array("message" => 'Invalid Request')));
            }
        }else{
            $add = "update";
            $update = $this->MedicineModel->AddDiscountModel($data, $add, $id);
            if ($update == 1) {
                $updateInAllProduct = $this->MedicineModel->updateInAllProduct($discountValue, $id);
                if ($updateInAllProduct == 1) {
                    echo (json_encode(array("message" => 'success')));
                }else{
                    echo (json_encode(array("message" => 'something wrong')));
                }
            }else{
                echo (json_encode(array("message" => 'Invalid Request')));
            }   
        }
    }

    public function deliveryBoyDashboard($deliveryBoyId=null)
	{
         
        // $restaurant_id = $this->restaurant_id;
        
        // echo "<pre>";
        // print_r($restaurant_id);
        // exit;

        // $restaurant_data = $this->restaurant_model->restaurant_by_id($restaurant_id);


        $order_received = $this->restaurant_model->deliveryBoy_orders_count('Placed', $deliveryBoyId);

        $order_processing = $this->restaurant_model->deliveryBoy_orders_count('Processing', $deliveryBoyId);

        $order_pickedup = $this->restaurant_model->deliveryBoy_orders_count('Pickup', $deliveryBoyId);

        $order_delivered = $this->restaurant_model->deliveryBoy_orders_count('Delivered', $deliveryBoyId);



        // $total_products = $this->restaurant_model->restaurants_product_counts_for_discount($restaurant_id);

        // $total_available_products = $this->restaurant_model->total_available_products($restaurant_id);

        // $all_order_javas = $this->restaurant_model->show_all_order_for_java_script($restaurant_id);
        

        // $lastYearIncome = $this->restaurant_model->last_yearIncome($restaurant_id);

        // $thisMonthIncome = $this->restaurant_model->thisMonthIncome($restaurant_id);

          // echo "<pre>";
          // print_r($lastYearIncome);
          // exit;
        $data  = array(
            // 'restaurant_data' => $restaurant_data ,
            'order_received' => $order_received ,
            'order_processing' => $order_processing,
            'order_pickedup' => $order_pickedup,
            'order_delivered' => $order_delivered
            // 'all_order_javas' => $all_order_javas,
            // 'total_products' => $total_products,
            // 'total_available_products' => $total_available_products,
            // 'thisYearIncomes'    => $lastYearIncome,
            // 'thisMonthIncomes'  => $thisMonthIncome

        );
        header('Content-Type: application/json');
        echo json_encode($data);

       
    }


}
