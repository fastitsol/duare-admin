<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forget extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }
	public function index(){
        $data=array();
        if($this->input->post('reset_email')){
            $email = $this->input->post('email');
            $this->db->where_not_in('type', 'admin');
            $user = $this->user_model->get_user_by_email($email);
            if($user){
                $this->load->helper('string');
                $key = random_string('alnum', 60);
                
                $this->session->set_userdata('reset_email', $email);
                $this->session->set_userdata('reset_key', $key);
                $message = 'To reset your password click the link bellow. <br /> <a style="color:#FFF;background:blue;text-decoration:none;border:1px solid #DDD;border-radius:4px;display:inline-block;padding:5px 15px" href="'.site_url('forget/reset_password/').$key.'">Click me to reset your password</a>';
                
                $this->email("Duare Password Reset", $message, $email);
                $this->session->set_userdata('success_msg', 'Check Your Email'); 
                
            }else{
                $this->session->set_userdata('error_msg', '('.$email.') This email is not in our records.');
            }
        }
		$this->load->view('forget-email', $data);
	}
    
    public function reset_password($key=null){
        $errors=null;
        if($key!=null and $key==$this->session->userdata('reset_key')){
            if($this->input->post('reset_password')){
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
                $this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|required|matches[password]');
                $email = $this->session->userdata('reset_email');
                $password = $this->input->post('password');
                if($this->form_validation->run()){
                    if($this->db->where('email', $email)->update('users', array('password'=>md5($password)))){
                        $this->session->set_userdata('success_msg', 'Your Password reset with your new password');
                        redirect('login');
                    }else{
                        $this->session->set_userdata('error_msg', 'Something wrong please try again.');
                    }
                }else{
                    $errors .= validation_errors('<p>', '</p>');
                    $this->session->set_userdata('error_msg', $errors);
                }
            }
        }else{
            $this->session->set_userdata('error_msg', 'Your Password reset authentation is not valid');
            redirect('forget');
        }
        
        $this->load->view('reset-password', array('key'=>$key));
    }
    
    
    public function email($subject, $message, $to_email){
        $this->load->library('email');
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'mail.duare.net';
        $config['smtp_port']    = '587';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'passreset@duare.net';
        $config['smtp_pass']    = 'duare@123';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);

        $this->email->from('passreset@duare.net', 'Duare | Password Reset');
        $this->email->to($to_email); 

        $this->email->subject($subject);
        $this->email->message($message);  
        $this->email->send();

    }
    
}
