<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        if($this->user_model->is_user_logd_in()){
            
            if($this->session->userdata('current_user_status')=='inactive' and $this->session->userdata('current_user_type')=='restaurant'){
                redirect('restaurant/users/restaurant');
            }else{
                redirect('admin/dashboard');
            }
        }
        
        if($this->user_model->get_setting_data('site_language')=='bn'){
            $this->lang->load('bn_lang', 'bn');
        }
        
         if(!$this->user_model->is_captcha_log()){
            redirect('pre_login');
        }
    }
	public function index($error=null)
	{
        $data=array();
        if($error){
            $data['login_error']=$error;
        }
		$this->load->view('admin-login', $data);
	}
    
    public function try_login(){
        $user_name = $this->input->post('user_name');
        $password= $this->input->post('password');
        if($this->user_model->is_user_available( $user_name, md5($password) )){
            
            if($this->session->userdata('current_user_type')=='restaurant' or 
                $this->session->userdata('current_user_type')=='pharmacy'){

                
                redirect('restaurant/Dashboard/');
            }else{
                redirect('admin/dashboard');
            }
            
        }else{
            redirect('login/index/error');
        }
    }
    
    public function destroy(){
        $sesattr = array(
						'captcha_code' => false,
						'current_user_id' => '',
						'current_username' => '',
						'current_user_type' => '',
						'base_url' => ''
					);
       $this->session->unset_userdata($sesattr);
        redirect('login');
    }
}
