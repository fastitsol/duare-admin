<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MedicineController extends Admin_Controller{
    public function __construct(){
        parent::__construct();
         $this->user_id = $this->session->userdata('current_user_id');
        $this->user_type = $this->session->userdata('current_user_type');
        $this->Medicine_id = $this->session->userdata('restaurant_id');
        $this->load->model('MedicineModel', "m_m");
        $this->load->model('product_model');
    }
    public function search($current_page=null)
	{
        $this->load->library('form_validation');
        $this->form_validation->set_rules('query', 'Query', 'required');
        if($this->form_validation->run()){   
        }
        $query = $this->input->post('query');
        $data['medicines'] = $this->m_m->searchByName($query, $this->Medicine_id);
        
            
        $this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineProducts/search_result', $data);
        $this->load->view('Medicine/inc/footer');
	}
    public function index($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'Medicine/MedicineController/index/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->m_m->medicine_count('medicine', $this->Medicine_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['medicines']=$this->m_m->medicines('medicine', $config['per_page'],  $current_page, $this->Medicine_id);

		$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineProducts/all_products_medicine', $data);
        $this->load->view('Medicine/inc/footer');
    }

    public function editMedicine($medicine_id=null){

        if($this->input->post('save_product') and $this->permission->can()){
            

         $this->form_validation->set_rules('Medicine_name', $this->lang->line('product_name'), 'trim|required');
         $this->form_validation->set_rules('price', $this->lang->line('price'), 'trim|is_numeric|required');
            
          
            
            // if this form validation working
            if($this->form_validation->run()){
                $attr=array(
                    'name'            => $this->input->post('Medicine_name'),
                    'food_type'           => $this->input->post('Medicine_type'),
                    'size'            => $this->input->post('Medicine_size'),
                    'medicine_mg_ml'     => $this->input->post('Medicine_power'),
                    'description'     => $this->input->post('Medicine_catagory'),
                    'medicine_company'     => $this->input->post('Medicine_company'),
                    'purchasePrice'            => $this->input->post('purchasePrice'),
                    'price'     => $this->input->post('price'),
                    'availability'   => $this->input->post('availability')
                );
                
                //file upload
                if($_FILES['product_image']["name"]){
                    if($product_image = $this->do_upload('./uploads/products/', 'product_image')){
                        $attr['image']=$product_image;
                        
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                    }
                }
                
                
                // insert the product to database
                if($this->db->where('id', $medicine_id)->update('all_products', $attr)){
                    

                    $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    // if(isset($attr['image']) and $attr['image']){
                    //     $path='./uploads/products/'.$medicine->image;
                    //     $this->remove_file($path);
                    // }
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }

        
        $data['medicine'] = $this->m_m->Medicine_by_id($medicine_id);
        $this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineProducts/editMedicine', $data);
        $this->load->view('Medicine/inc/footer');
    }



    public function deleteMedicine($medicine_id=null){
        if(!$medicine_id or !is_numeric($medicine_id)){
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('Medicine/MedicineController/index/');
        }
        
        $product = $this->product_model->product_by_id($medicine_id);
        if($this->Medicine_id != $product->restaurant_id){redirect('Medicine/MedicineController/index/');}
        
        
        if($this->permission->can()){
            if($this->db->where('id', $medicine_id)->delete('all_products')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                
                if($product->image){
                   $path='./uploads/products/'.$product->image;
                    $this->remove_file($path); 
                }
                
                
                redirect('Medicine/MedicineController/index/');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('Medicine/MedicineController/index/');
            }
        }
    }
    

    
    public function AddMedicine()
    {   

         if($this->input->post('save_new_product') and $this->permission->can()){
           
            $this->form_validation->set_rules('product_name', $this->lang->line('product_name'), 'trim|required');
            $this->form_validation->set_rules('price', $this->lang->line('price'), 'trim|is_numeric|required');
            
            $opening_time  = date("H:i", strtotime($this->input->post('opening_time')));
            $closing_time  = date("H:i", strtotime($this->input->post('closing_time')));
            
            // if this form validation working
            if($this->form_validation->run()){

                $attr=array(
                    'name'            => $this->input->post('product_name'),
                    'product_type'           => "medicine",
                    'restaurant_id'            => $this->input->post('restaurant_id'),
                    'description'     => $this->input->post('Medicine_Catagory'),
                    'food_type'            => $this->input->post('Medicine_Type'),
                    'size'           => $this->input->post('Medicine_Size'),
                    'medicine_mg_ml'            => $this->input->post('Medicine_Power'),
                    'medicine_company'     => $this->input->post('medicine_company'),
                    'purchasePrice'            => $this->input->post('purchasePrice'),
                    'price'            => $this->input->post('price'),
                    'discount_price' => '0',
                    'discount_percent' => 'nan',
                    'opening_time'           => $this->input->post('opening_time'),
                    'closing_time'            => $this->input->post('closing_time'),
                    'availability'     => $this->input->post('availability')

                );
                
                //file upload
                if($_FILES['product_image']["name"]){
                    if($product_image = $this->do_upload('./uploads/products/', 'product_image')){
                        $attr['image']=$product_image;
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                    }
                }
                
                if($product_id = $this->db->insert('all_products', $attr)){
                    $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    $product_id = $this->db->insert_id();
                    
                    
                    redirect('Medicine/MedicineController/AddMedicine');
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                    redirect('Medicine/MedicineController/AddMedicine');
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }
        


        $data['Medicine_id'] = $this->Medicine_id;

        // echo "<pre>";
        // print_r($this->Medicine_id);
        // exit;


        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineProducts/AddMedicines', $data);
        $this->load->view('Medicine/inc/footer');
    }

    



     /*File upload method*/
    public function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
    //This function for remove file from location
    public function remove_file($file){
        if(is_file($file)){
            unlink($file);
        }
    }
    


}
