<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderMedicine extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->user_id = $this->session->userdata('current_user_id');
        $this->user_type = $this->session->userdata('current_user_type');
        $this->Medicine_id = $this->session->userdata('restaurant_id');
        $this->load->model('MedicineModel', "m_m");
        $this->load->model('product_model');
        $this->load->model('restaurant_model');
    }


    public function newOrderMedicine($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'Medicine/OrderMedicine/newOrderMedicine/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->m_m->orders_count('placed', $this->Medicine_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->m_m->orders('placed', $config['per_page'],  $current_page, $this->Medicine_id);
        // echo "<pre>";
        // print_r($data['orders']);
        // echo"<pre>";
        // return;
		$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/new_order', $data);
        $this->load->view('Medicine/inc/footer');
    }

    public function ProcessingOrderMedicine($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'Medicine/OrderMedicine/ProcessingOrderMedicine/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->m_m->orders_count('Processing', $this->Medicine_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->m_m->orders('Processing', $config['per_page'],  $current_page, $this->Medicine_id);
        
		$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/processing_order', $data);
        $this->load->view('Medicine/inc/footer');
    }

    public function PickupOrderMedicine($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'Medicine/OrderMedicine/PickupOrderMedicine/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->m_m->orders_count('Pickup', $this->Medicine_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->m_m->orders('Pickup', $config['per_page'],  $current_page, $this->Medicine_id);
        
		$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/Pickup_order', $data);
        $this->load->view('Medicine/inc/footer');
    }



    public function DeliveredOrderMedicine($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'Medicine/OrderMedicine/DeliveredOrderMedicine/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->m_m->orders_count('Delivered', $this->Medicine_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->m_m->orders('Delivered', $config['per_page'],  $current_page, $this->Medicine_id);
        
		$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/Delivered_order', $data);
        $this->load->view('Medicine/inc/footer');
    }


    public function ordersView($order_id = null)
    {


    	if ($order_id != null) {
    		$order = $this->m_m->orderInfoAll($order_id,$this->Medicine_id);
    	}

    	$data  = array(
    		'orders' => $order, 
    	);

    	//  echo "<pre>";
    	//  print_r($data );
    	//  exit;

    	$this->load->view('Medicine/inc/header');
		$this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/orders_view', $data);
        $this->load->view('Medicine/inc/footer');
    }

    public function reiciveTK()
    {


        $invoice_id =  $this->input->post('invoice_id');

        $product_id  =  $this->input->post('product_id');
        $purchasePrice  =  $this->input->post('purchasePrice');

    	$product_price =  $this->input->post('product_price');

        $qty  =  $this->input->post('qty');

        if(($invoice_id != null) &&($product_id != null) && ($purchasePrice != null) && ($product_price != null) && ($qty != null)){

            $product_price_for_all_productTable = $product_price;
            $purchasePrice_for_all_productTable = $purchasePrice;

            $product_price_for_InvoiceItem_withqty = 0;

            $product_price_for_InvoiceItem_withqty = ($product_price * $qty);


                   
                   $updateAllProductTable = $this->m_m->updateAllProductTable($product_id, $product_price_for_all_productTable, $purchasePrice_for_all_productTable);

                   if ($updateAllProductTable == 1) {
                    $updateInvoiceItemAmount = $this->m_m->updateInvoiceItemAmount($invoice_id,$this->Medicine_id,$product_id, $product_price_for_all_productTable, $product_price_for_InvoiceItem_withqty);

                      $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                       redirect('Medicine/OrderMedicine/ordersView/'.$invoice_id);
                   }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                    redirect('Medicine/OrderMedicine/ordersView/'.$invoice_id);
                   }
                    
                

        }else{

            
            $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
            redirect('Medicine/OrderMedicine/ordersView/'.$invoice_id);
        }
        
    }



    public function penddingToProcessing($invoice_id)
    {

        $data =  array(
            'status' => 'Processing', 

        );

        $updatePenddingToProcessing = $this->m_m->updatePenddingToProcessing($invoice_id, $data);

        if ($updatePenddingToProcessing) {
                
            $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            redirect('Medicine/OrderMedicine/ProcessingOrderMedicine/');
        }
    } 




    public function ProcessingOrderView($order_id = null)
    {


        if ($order_id != null) {
            $order = $this->m_m->orderInfoAll($order_id,$this->Medicine_id);
        }

        $data  = array(
            'orders' => $order, 
        );

         // echo "<pre>";
         // print_r($data );
         // exit;

        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/processing_order_view', $data);
        $this->load->view('Medicine/inc/footer');
    }



    public function ProcessingToPickup($invoice_id)
    {

        $data =  array(
            'status' => 'Pickup', 

        );

        $updateProcessingToPickup = $this->m_m->updatePenddingToProcessing($invoice_id, $data);

        if ($updateProcessingToPickup) {
                
            $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            redirect('Medicine/OrderMedicine/PickupOrderMedicine/');
        }
    } 


    public function PickupOrderView($order_id = null)
    {


        if ($order_id != null) {
            $order = $this->m_m->orderInfoAll($order_id,$this->Medicine_id);
        }

        $data  = array(
            'orders' => $order, 
        );

         // echo "<pre>";
         // print_r($data );
         // exit;

        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineOrder/processing_order_view', $data);
        $this->load->view('Medicine/inc/footer');
    }





    public function discountMedicine()
    {


        $duscount = $this->m_m->discountInAll($this->Medicine_id);

        $data  = array(
            'duscounts' => $duscount, 
        );

        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MedicineDiscount/discountAllMedicine', $data);
        $this->load->view('Medicine/inc/footer');
    }


    public function AddDiscount()
    {

        $id = $this->Medicine_id;
        $discountValue =  $this->input->post('discountValue');

        $date =  date('Y-m-d H:i');

        $data =  array(
            'restaurant_id' => $id, 
            'discountValue' => $discountValue, 
            'created_at' => $date 
        );

        $duscount = $this->m_m->discountInAll($this->Medicine_id);

      

        if($duscount == null)
        {   
            $add = "add";
            $insertDiscount = $this->m_m->AddDiscountModel($data, $add, $id);
            if ($insertDiscount == 1) {

                $updateInAllProduct = $this->m_m->updateInAllProduct($discountValue, $id);
                if ($updateInAllProduct == 1) {
                   
                   $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                   redirect('Medicine/OrderMedicine/discountMedicine/');
                }else{

                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                    redirect('Medicine/OrderMedicine/discountMedicine/');
                }

                
            }else{
                 $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                redirect('Medicine/OrderMedicine/discountMedicine/');
            }

        }else{
            $add = "update";
            $update = $this->m_m->AddDiscountModel($data, $add, $id);

            if ($update == 1) {

                $updateInAllProduct = $this->m_m->updateInAllProduct($discountValue, $id);
                if ($updateInAllProduct == 1) {
                   
                   $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                   redirect('Medicine/OrderMedicine/discountMedicine/');
                }else{

                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                    redirect('Medicine/OrderMedicine/discountMedicine/');
                }
                
            }else{
                
                 $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                redirect('Medicine/OrderMedicine/discountMedicine/');
            }   
        }
    }






    public function YearReport()
    {
        $orders = $this->m_m->thisYearReport($this->Medicine_id);

        $data  = array(
            'orders' => $orders, 
        );

        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MredicineReport/yearReport', $data);
        $this->load->view('Medicine/inc/footer');
    }


    public function MonthlyReport()
    {
        $orders = $this->m_m->thisMonthReport($this->Medicine_id);

        $data  = array(
            'orders' => $orders, 
        );

        $this->load->view('Medicine/inc/header');
        $this->load->view('Medicine/inc/sidebar');
        $this->load->view('Medicine/MredicineReport/monthReport', $data);
        $this->load->view('Medicine/inc/footer');
    }






 }




