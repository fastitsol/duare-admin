<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pre_login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        if($this->user_model->is_user_logd_in()){
            redirect('admin/dashboard');
        }
        if($this->user_model->is_captcha_log()){
            redirect('login');
        }
    }
	public function index($error=null)
	{
        
        $data=array();
        if($error){
            $data['login_error']=$error;
        }
		$this->load->view('frontend/pre_login', $data);
	}
    
    public function try_login(){
        $user_name = $this->input->post('user_name');
        $captcha_code= $this->input->post('captcha_code');
        if($this->user_model->is_captcha_available( $user_name, $captcha_code )){
            if($this->session->userdata('current_user_type')=='customer'){
                redirect('customer');
            }else{
                redirect('admin/dashboard');
            }
        }else{
            redirect('pre_login/index/error');
        }
    }
    
    public function destroy(){
        $sesattr = array(
						'captcha_code' => false,
						'current_user_id' => '',
						'current_username' => '',
						'current_user_type' => '',
						'base_url' => ''
					);
       $this->session->unset_userdata($sesattr);
        redirect('login');
    }
}
