<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_up extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        if($this->user_model->is_user_logd_in()){
            redirect('restaurant/dashboard');
        }
        
        if($this->user_model->get_setting_data('site_language')=='bn'){
            $this->lang->load('bn_lang', 'bn');
        }
        
    }
	public function index()
	{
        $data=array();
         if($this->input->post('sign_up')){
             
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
             $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|is_unique[users.user_name]');
             $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|is_unique[users.phone]');
             $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
             $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
             
            $update_attr=array(
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'user_name'=>$this->input->post('user_name'),
                'password'=>md5($this->input->post('password')),
                'image'=>'demo-avater.png',
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'type'=>'restaurant',
            );
            
             if($this->form_validation->run()){
                   if($this->db->insert('users', $update_attr)){
                    $user_id = $this->db->insert_id();
                    $create_restaurant = $this->create_restaurant($user_id, $this->input->post('restaurant_name'));
                    if($create_restaurant){
                        $this->session->set_userdata('success_msg', $this->lang->line('sign_up_success'));
                        redirect('login');
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                        $tis->db->where('id', $user_id)->delete('users');
                    }
                    
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                }  
             }else{
                 $data['validation_errors']=validation_errors('<p>', '</p>');
                  $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
             }
            
        }
        
		$this->load->view('sign_up', $data);
	}
    
    
    // create restaurant row
    public function create_restaurant($user_id, $restaurant_name){
        $restaurant_id = null;
        if($this->db->insert('tbl_restaurant', array('name'=>$restaurant_name))){
            $restaurant_id = $this->db->insert_id();
        }
        if($user_id and $restaurant_id){
            if($this->db->insert('tbl_restaurant_users', array('restaurant_id'=>$restaurant_id, 'user_id'=>$user_id))){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
}
