<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('customer_model');
        $this->load->model('importer_model');
        $this->load->model('product_model');
        $this->load->model('sr_model');
    }
	public function index()
	{
       
	}
    
    //return customer data by customer id
    public function get_customer(){
        $customer = $this->customer_model->customer_by_customer_id($this->input->post('customer_id'));
        if(!$customer){
            $c_phone = $this->input->post('customer_id'); 
            $customer = $this->customer_model->customer_by_customer_phone($c_phone);
        }
        if($customer){
            $this->session->set_userdata('cart_help_customer_id', $customer->customer_id);
            
            $date['customer'] =$customer;
            $date['report'] =$this->get_customer_due($customer->customer_id);
            echo json_encode($date);
        }else{
            $this->session->set_userdata('cart_help_customer_id', '');
            echo json_encode(null);
        }
    }
    
    // this function for get customer due list 
    public function get_customer_due($customer_id = null){
        $result = $this->db->get_where('invoice', array('type'=>'sale', 'customer_id'=>$customer_id, 'due_bill>'=>0));
        if($result->result()){
            $this->load->model('sale_model');
            
            $data['sales']=$result->result();
            $data['due_bill']=$this->sale_model->due_bill(null, null, null, $customer_id);
            return $data;
        }else{
             return null;
        }
    }
    
    //return customer data by importer id
    public function get_importer(){
        $importer = $this->importer_model->importer_by_importer_id($this->input->post('importer_id'));
        if(!$importer){
            $importer = $this->importer_model->importer_by_importer_phone($this->input->post('importer_id'));
        }
        if($importer){
            $this->session->set_userdata('cart_help_importer_id', $importer->importer_id);
            echo json_encode($importer);
        }else{
            $this->session->set_userdata('cart_help_importer_id', '');
            echo json_encode(null);
        }
    }
    //return sr data by customer id
    public function get_sr(){
        $sr = $this->sr_model->sr_by_sr_id($this->input->post('sr_id'));
        if($sr){
            $this->session->set_userdata('cart_help_sr_id', $sr->sr_id);
            echo json_encode($sr);
        }else{
            $this->session->set_userdata('cart_help_sr_id', '');
            echo json_encode(null);
        }
    }
    
    // get cart help paymented amount
    public function paymented_amount($amount=null){
        if($amount!=''){
            $this->session->set_userdata('cart_help_paymented', $amount);
        }
        $data['paymented_bill']=$this->session->userdata('cart_help_paymented');
        echo json_encode($data);
    }
    
    
    // function for return cart helper values
    public function cart_help_session(){
        $data['customer_id'] = $this->session->userdata('cart_help_customer_id');
        $data['importer_id'] = $this->session->userdata('cart_help_importer_id');
        $data['sr_id'] = $this->session->userdata('cart_help_sr_id');
        $data['paymented_bill']=$this->session->userdata('cart_help_paymented');
        $data['memo_id']=$this->session->userdata('cart_help_memo_id');
        echo json_encode($data);
    }
    
    //this method for cart 
    public function add_cart($product_id=null){
        $product = $this->product_model->get_product_by_product_id($product_id);
        if(!$product){
            echo json_encode(null); return;
        }
   
        $data = array(
        'id'      => $product->id,
        'qty'     => 1,
        'price'   => $product->sale_rate,
        'name'    => ' ',
            'options' => array(
                'product_name' => $product->name, 
                'buy_price' => $product->buy_rate, 
                'sale_price' => $product->sale_rate, 
                'product_id' => $product->alt_id,
                'size' => $product->size,
                'discount_percent' => sprintf('%.2f',0),
                'discount_price' => sprintf('%.2f',0),
                'emi' => '',
                'warranty' => ''
            )
        );
        $this->load->library('cart');
        $this->cart->insert($data);
        echo json_encode($this->cart->contents());
    }
    public function order_details($csid=null){
        $product = $this->product_model->get_product_by_product_id($csid);
        if(!$product){
            echo json_encode(null); return;
        }
   
        $data = array(
        'id'      => $product->id,
        'qty'     => 1,
        'price'   => $product->sale_rate,
        'name'    => ' ',
            'options' => array(
                'product_name' => $product->name, 
                'buy_price' => $product->buy_rate, 
                'sale_price' => $product->sale_rate, 
                'product_id' => $product->alt_id,
                'size' => $product->size,
                'discount_percent' => sprintf('%.2f',0),
                'discount_price' => sprintf('%.2f',0),
                'emi' => '',
                'warranty' => ''
            )
        );
        $this->load->library('cart');
        $this->cart->insert($data);
        echo json_encode($this->cart->contents());
    }
    //this method for cart 
    public function add_service_cart(){
        $product_name = $this->input->post('product_name');
        $price = $this->input->post('price');
        if(!$product_name and !$price){
            echo json_encode(null); return;
        }
   
          $data = array(
            'id'      => 123,
            'qty'     => 1,
            'price'   => $price,
            'name'    => ' ',
            'options' => array(
                'product_name' => $product_name, 
                'buy_price' => 0, 
                'sale_price' => 0, 
                'product_id' => 0,
                'size' => '',
                'discount_percent' => sprintf('%.2f',0),
                'discount_price' => sprintf('%.2f',0),
                'emi' => '',
                'warranty' => ''
            )
        );
        $this->load->library('cart');
        $this->cart->insert($data);
        echo json_encode($this->cart->contents());
    }
    
    //this method for cart 
    public function add_cart_import($product_id=null){
        $product = $this->product_model->get_product_by_product_id($product_id);
        if(!$product){
            echo json_encode(null); return;
        }
   
        $data = array(
        'id'      => $product->id,
        'qty'     => 1,
        'price'   => $product->buy_rate,
        'name'    => ' ',
            'options' => array(
                'product_name' => $product->name, 
                'buy_price' => $product->buy_rate, 
                'sale_price' => $product->sale_rate, 
                'product_id' => $product->alt_id,
                'size' => $product->size,
                'discount_percent' => sprintf('%.2f',0),
                'discount_price' => sprintf('%.2f',0),
                'emi' => '',
                'warranty' => ''
            )
        );
        $this->load->library('cart');
        $this->cart->insert($data);
        echo json_encode($this->cart->contents());
    }
    
    //return all cart
    public function cart(){
        $this->load->library('cart');
        echo json_encode($this->cart->contents());
    }
    
    // update single cart
    public function update_cart($rowid, $qty){
        $data = array(
            'rowid'=>$rowid,
            'qty'=>$qty,
        );
        $this->load->library('cart');
        $this->cart->update($data);
        if($qty<=0){
            echo json_encode($this->cart->contents());
            return;
        }
        $carts = $this->cart->contents();
        $cart =$carts[$rowid];
        $subtotal = $cart['subtotal'];
        $discount_price = $subtotal/100 * $cart['options']['discount_percent'];
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'], 
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$cart['options']['discount_percent']),
                'discount_price' => sprintf('%.2f',$discount_price),
                'emi' => $cart['options']['emi'],
                'warranty' => $cart['options']['warranty']
            )
        );
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    // update cart price
    public function update_cart_price($rowid, $price){
        $data = array(
            'rowid'=>$rowid,
            'price'=>$price,
        );
        $this->load->library('cart');
        $this->cart->update($data);
        $carts =$this->cart->contents();
        $cart =$carts[$rowid];
        $subtotal = $cart['subtotal'];
        $discount_price = $subtotal/100 * $cart['options']['discount_percent'];
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'],
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$cart['options']['discount_percent']),
                'discount_price' => sprintf('%.2f',$discount_price),
                'emi' => $cart['options']['emi'],
                'warranty' => $cart['options']['warranty']
            ),
        );
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    
    // update cart price
    public function update_cart_percent($rowid, $percent){
        $this->load->library('cart');
        $carts = $this->cart->contents();
        $cart =$carts[$rowid];
        $subtotal = $cart['subtotal'];
        $discount_price = $subtotal/100 * $percent;
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'], 
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$percent),
                'discount_price' => sprintf('%.2f', $discount_price),
                'emi' => $cart['options']['emi'],
                'warranty' => $cart['options']['warranty']
            ),
        );
        
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    
    // update cart price
    public function update_cart_emi($rowid, $emi=null){
        $this->load->library('cart');
        $carts = $this->cart->contents();
        $cart =$carts[$rowid];
        
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'], 
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$cart['options']['discount_percent']),
                'discount_price' => sprintf('%.2f',$cart['options']['discount_price']),
                'emi' => $emi,
                'warranty' => $cart['options']['warranty']
            ),
        );
        
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    
    // update cart price
    public function update_cart_warranty($rowid){
        $this->load->library('cart');
        $carts = $this->cart->contents();
        $cart =$carts[$rowid];
        $warranty = $this->input->post('warranty');
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'], 
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$cart['options']['discount_percent']),
                'discount_price' => sprintf('%.2f',$cart['options']['discount_price']),
                'emi' => $cart['options']['emi'],
                'warranty' => $warranty
            ),
        );
        
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    
    
    // update cart price
    public function update_cart_discount_price($rowid, $price){
        $this->load->library('cart');
        $carts = $this->cart->contents();
        $cart =$carts[$rowid];
        $subtotal = $cart['subtotal'];
        $discount_percent = $price*100 / $subtotal;
        $data = array(
            'rowid'=>$rowid,
            'options'=>array(
                'product_name' => $cart['options']['product_name'],
                'buy_price' => $cart['options']['buy_price'],
                'sale_price' => $cart['options']['sale_price'],
                'product_id' => $cart['options']['product_id'],
                'size' => $cart['options']['size'],
                'discount_percent' => sprintf('%.2f',$discount_percent),
                'discount_price' => sprintf('%.2f',$price),
                'emi' => $cart['options']['emi'],
                'warranty' => $cart['options']['warranty']
            ),
        );
        
        $this->cart->update($data);
        echo json_encode($this->cart->contents());
    }
    
    
    // this function for invoice items
    public function invoice_items($invoice_id){
        $this->load->model('sale_model');
        $this->load->model('product_model');
        $invoice_list = $this->sale_model->invoice_items($invoice_id);
        $data=array();
        if($invoice_list){
            $counter=0;
            foreach($invoice_list as $list){
                $counter++;
                $product = $this->product_model->product_by_id($list->product_id);
                $data['invoice_'.$counter]=array(
                    'id'=> $list->product_id,
                    'name'=> 1,
                    'options'=> array(
                        'discount_percent'=>$list->discount_percent,
                        'discount_price'=>$list->discount_price,
                        'product_id'=>0,
                        'product_name'=>0,
                        'buy_price'=>$list->buy_price,
                        'sale_price'=>$list->sale_price,
                        'size'=>0,
                        'emi' => $list->emi,
                        'warranty' => $list->warranty
                    ),
                    'rowid'=> 'invoice_'.$counter,
                    'price'=> $list->sale_price,
                    'qty'=> $list->qty,
                    'subtotal'=> $list->amount,
                );
                if($product){
                    $data['invoice_'.$counter]['options']['product_id']=$product->alt_id;
                    $data['invoice_'.$counter]['options']['size']=$product->size;
                    $data['invoice_'.$counter]['options']['product_name']=$product->name;
                }
            }
            echo json_encode($data);
        }else{
             echo json_encode(null);
        }
    }
    
    // this function for invoice items
    public function invoice_to_cart($invoice_id){
        $this->remove_all_carts();
        $this->load->model('sale_model');
        $this->load->model('product_model');
        $invoice = $this->sale_model->invoice_by_id($invoice_id);
        if(!$invoice){
            echo json_encode(null);
            return;
        }
        $invoice_list = $this->sale_model->invoice_items($invoice_id);
        $data=array();
        $this->session->set_userdata('cart_help_customer_id', $invoice->customer_id);
        $this->session->set_userdata('cart_help_sr_id', $invoice->sr_id);
        if($invoice_list){
            $this->session->set_userdata('cart_help_memo_id', $invoice_id);
            $this->load->library('cart');
            $counter=0;
            foreach($invoice_list as $list){
                $counter++;
                $product = $this->product_model->product_by_id($list->product_id);
                
                $data = array(
                    'id'      => $list->product_id,
                    'qty'     => $list->qty,
                    'price'   => $list->sale_price,
                    'name'    => ' ',
                    'options' => array( 
                        'product_id' => '', 
                        'size' => '', 
                        'product_name' => '', 
                        'buy_price' => $list->buy_price, 
                        'sale_price' => $list->sale_price, 
                        'discount_percent' => sprintf('%.2f',$list->discount_percent),
                        'discount_price' => sprintf('%.2f',$list->discount_price),
                        'emi' => $list->emi,
                        'warranty' => $list->warranty
                    )
                );
                if($product){
                    $data['options']['product_id']=$product->alt_id;
                    $data['options']['size']=$product->size;
                    $data['options']['product_name']=$product->name;
                }
                $this->cart->insert($data);
            }
            echo json_encode($this->cart->contents());
        }else{
             echo json_encode(null);
        }
    }
    
    
    
    // this method for remove all carts items 
    public function remove_all_carts(){
        $this->load->library('cart');
        $carts = $this->cart->contents();
        if($carts){foreach($carts as $cart){
           $this->cart->update(array('rowid'=>$cart['rowid'], 'qty'=>0));
        }}
        $this->session->set_userdata('cart_help_customer_id', '');
        $this->session->set_userdata('cart_help_sr_id', '');
        $this->session->set_userdata('cart_help_paymented', '');
        $this->session->set_userdata('cart_help_memo_id', '');
        return true;
        
    }
    
    
    
    
    // this function for invoice items
    public function service_items($service_id){
        $this->load->model('sale_model');
        $this->load->model('product_model');
        $service_list = $this->sale_model->service_items($service_id);
        $data=array();
        if($service_list){
            $counter=0;
            foreach($service_list as $list){
                $counter++;
                $data['invoice_'.$counter]=array(
                    'id'=> 123,
                    'name'=> 1,
                    'options'=> array(
                        'discount_percent'=>0,
                        'discount_price'=>0,
                        'product_id'=>0,
                        'product_name'=>$list->service_name,
                        'buy_price'=>0,
                        'sale_price'=>0,
                        'size'=>0,
                        'emi' => '',
                        'warranty' => ''
                    ),
                    'rowid'=> 'invoice_'.$counter,
                    'price'=> $list->price,
                    'qty'=> $list->qty,
                    'subtotal'=> $list->total_price,
                );
                
            }
            echo json_encode($data);
        }else{
             echo json_encode(null);
        }
    }
    
    
    
    
    
    // this method for add new customer by ajax
    public function add_customer(){
        $data=array();
        $errors=null;
        // set form validation roles
            if(1){
                $attr=array(
                    'name'         => $this->input->post('customer_name'),
                    'customer_id'  => $this->input->post('customer_id'),
                    'phone'        => $this->input->post('phone'),
                    'email'        => $this->input->post('email'),
                    'institute'    => $this->input->post('institute'),
                    'address'      => $this->input->post('address')
                );
                if($this->db->insert('customers', $attr)){
                    $customer_id = $this->db->insert_id();
                    $this->db->where('id', $customer_id)->update('customers', array('customer_id'=>$customer_id));
                    $data['customer'] = true;
                    $data['customer_id'] = $customer_id;
                    $data['success_msg'] = $this->lang->line('save_success_msg');
                }else{
                    $data['customer'] = false;
                    $data['errors_msg'] = $this->lang->line('save_error_msg');
                    
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $data['customer'] = false;
                $data['errors_msg'] = $errors;
            }
        
        echo json_encode($data);
    }
    
    
    // this method for add new importer by ajax
    public function add_importer(){
        $data=array();
        $errors=null;
        // set form validation roles
            if(1){
                $attr=array(
                    'name'         => $this->input->post('importer_name'),
                    'importer_id'  => $this->input->post('importer_id'),
                    'phone'        => $this->input->post('phone'),
                    'email'        => $this->input->post('email'),
                    'institute'    => $this->input->post('institute'),
                    'address'      => $this->input->post('address')
                );
                if($this->db->insert('importers', $attr)){
                    $importer_id = $this->db->insert_id();
                    $this->db->where('id', $importer_id)->update('importers', array('importer_id'=>$importer_id));
                    $data['importer'] = true;
                    $data['importer_id'] = $importer_id;
                    $data['success_msg'] = $this->lang->line('save_success_msg');
                }else{
                    $data['importer'] = false;
                    $data['errors_msg'] = $this->lang->line('save_error_msg');
                    
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $data['importer'] = false;
                $data['errors_msg'] = $errors;
            }
        
        echo json_encode($data);
    }
    
    
    // this method for add new customer by ajax
    public function add_sr(){
        $data=array();
        $errors=null;
            if(1){
                $attr=array(
                    'name'         => $this->input->post('sr_name'),
                    'sr_id'        => $this->input->post('sr_id'),
                    'phone'        => $this->input->post('phone'),
                    'email'        => $this->input->post('email'),
                    'zone'         => $this->input->post('zone'),
                    'address'      => $this->input->post('address')
                );
                if($this->db->insert('srs', $attr)){
                    $sr_id = $this->db->insert_id();
                    $this->db->where('id', $sr_id)->update('srs', array('sr_id'=>$sr_id));
                    $data['sr'] = true;
                    $data['sr_id'] = $sr_id;
                    $data['success_msg'] = $this->lang->line('save_success_msg');
                }else{
                    $data['sr'] = false;
                    $data['errors_msg'] = $this->lang->line('save_error_msg');
                    
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $data['sr'] = false;
                $data['errors_msg'] = $errors;
            }
        
        echo json_encode($data);
    }
    
    
    // this method for get all customer
    public function get_all_customer(){
        $customers = $this->customer_model->customers();
        if($customers){
             echo json_encode($customers);
        }else{
             echo json_encode(null);
        }
    }
    
    // this method for get all sr
    public function get_all_sr(){
        $srs = $this->sr_model->srs();
        if($srs){
             echo json_encode($srs);
        }else{
             echo json_encode(null);
        }
    }
    
    // this method for get all product
    public function get_all_product(){
        $prdouct = $this->product_model->products();
        if($prdouct){
             echo json_encode($prdouct);
        }else{
             echo json_encode(null);
        }
    }
    
    // this method for get all importer
    public function get_all_importer(){
        $importers = $this->importer_model->importers();
        if($importers){
             echo json_encode($importers);
        }else{
             echo json_encode(null);
        }
    }



    public function get_order_details($csid){
        $order_details = $this->product_model->order_details($csid);

        if($order_details){
             echo json_encode($order_details);
        }else{
             echo json_encode(null);
        }
    }
    
    
}

