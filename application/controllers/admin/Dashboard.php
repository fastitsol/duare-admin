<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('restaurant_model');
        $this->load->model('Order_model', "order");
        if($this->session->userdata('current_user_type')=='deliver_man'){
            redirect('admin/orders/placed');
        }
    }
	public function index()
	{
       
        //$this->dashboard_model->total_invoice_product_qty();

        $all_order_javas = $this->dashboard_model->show_all_order_for_java_script();


        $total_earnings =  $this->dashboard_model->total_earns();

        $users = $this->dashboard_model->all_users();
        $restaurants = $this->dashboard_model->all_restaurant_active();
        $restaurants_inactive = $this->dashboard_model->all_restaurant_inactive();
        $deliver_mans = $this->dashboard_model->all_deliver_man();



        $order_received = $this->order->orders_count('placed');
        $order_processing = $this->order->orders_count('processing');
        $order_delivered = $this->order_model->orders_count('delivered');
        $data = array(
                "users" => $users,
                "restaurants" => $restaurants,
                "deliver_mans" => $deliver_mans,
                "restaurants_inactive" => $restaurants_inactive,

                "order_received" => $order_received,
                "order_processing" => $order_processing,
                "order_delivered" => $order_delivered,

                "total_earnings" => $total_earnings,

                "all_order_javas" => $all_order_javas

                );
          // echo "<pre>";
          // print_r($data);
        
         if ($this->session->userdata('current_user_type') == 'admin') {
          
           $this->load->view('admin/inc/header');
           $this->load->view('admin/inc/sidebar');
           $this->load->view('admin/dashboard', $data);
           $this->load->view('admin/inc/footer');
        }else{
            $sesattr = array(
                        'current_user_id' => '',
                        'current_username' => '',
                        'current_user_type' => '',
                        'base_url' => ''
            );
           $this->session->set_userdata($sesattr);
            redirect('login');
        }
	}
    
    
     public function backup_db(){
        $prefs = array(
        'ignore'        => array(),                     // List of tables to omit from the backup
        'format'        => 'txt',                       // gzip, zip, txt
        'filename'      => 'mybackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
        'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
        'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
        'newline'       => "\n"                         // Newline character used in backup file
        );
        // Load the DB utility class
        $this->load->dbutil();

        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup($prefs); 

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('application/cache/db/pos_database_backup_'.date('d_m_Y').'_.sql', $backup); 

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download('pos_database_backup_'.date('d_m_Y').'_.sql', $backup); 
    }
    
    
    
    public function all_customer_user($current_page=null)
    {
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Dashboard/all_customer_user/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->dashboard_model->all_users();
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['users']= $this->dashboard_model->all_customer($config['per_page'],  $current_page);
        
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/users/all_customer_user', $data);
        $this->load->view('admin/inc/footer');
    }
    


     public function all_restaurant_users($current_page=null)
    {
        // $this->load->library('pagination');
        // $config['base_url'] = base_url().'admin/Dashboard/all_restaurant_users/';
        // $config['use_page_numbers'] = FALSE;
        // $config['total_rows'] = $this->dashboard_model->all_restaurants();
        // $config['per_page'] = 10;
        // $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        // $config['full_tag_close'] = '</ul>';
        // $config['last_link'] = 'LAST →';
        // $config['first_tag_open'] = '<li>';
        // $config['first_tag_close'] = '</li>';
        // $config['first_link'] = '← FIRST';
        // $config['last_tag_open'] = '<li>';
        // $config['last_tag_close'] = '</li>';
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';
        // $config['cur_tag_open'] = '<a>';
        // $config['cur_tag_close'] = '</a>';
        // $config['next_link'] = '';
        // $config['prev_link'] = '';
        // $config['cur_tag_open'] = '<li class="active"><a href="#">';
        // $config['cur_tag_close'] = '</a></li>';
        
        // $this->pagination->initialize($config); 
        // $data['links']=$this->pagination->create_links();
        
        $data['restaurant_users']= $this->dashboard_model->all_restaurnt_users();
        
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/restaurants/all_restaurant_user', $data);
        $this->load->view('admin/inc/footer');
    }

    public function monthlyReport($restaurant_id=null)
    {
        
        $data['orders']= $this->restaurant_model->showthisMonthReporttoAdmin($restaurant_id);
        
        // echo"<pre>";
        // print_r($data['orders']);
        // return;
      
        $data['restaurants']= $this->restaurant_model->all_restaurants_for_coupon();

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        // $this->load->view('admin/report/monthlyReport', $data);
        $this->load->view('admin/report/monthlyReport', $data);
        $this->load->view('admin/inc/footer');
    }
    public function yearlyReport($restaurant_id=null)
    {
        
        $data['orders']= $this->restaurant_model->showthisYearReportToAdmin($restaurant_id);
        
        
      
        $data['restaurants']= $this->restaurant_model->all_restaurants_for_coupon();
        // echo"<pre>";
        // print_r($data);
        // return;

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        // $this->load->view('admin/report/monthlyReport', $data);
        $this->load->view('admin/report/yearlyReport', $data);
        $this->load->view('admin/inc/footer');
    }
    

}

