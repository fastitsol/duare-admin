<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Restaurant_model', 'd_m');
         $this->load->helper('url');
       
    }

    public function Add_Delivery()
    {
    	if($this->session->userdata('current_user_type')=='admin'){

            $ares_alls = $this->d_m->area_delivery_all();

            $data  = array(
                'ares_alls' => $ares_alls, 
            );

    		$this->load->view('admin/inc/header');
	        $this->load->view('admin/inc/sidebar');
	        $this->load->view('admin/Delivery/add_Delivery_place', $data);
	        $this->load->view('admin/inc/footer');
    	}

        
    }
    public function add_new_Delivery_place()
    {
        $area_id = $this->input->post('area_id');
    	$Delivery_place_name = $this->input->post('Delivery_place_name');
        $Delivery_charge = $this->input->post('Delivery_charge');
        $active_date = $this->input->post('active_date');



        if($Delivery_charge != NULL){

            if ($area_id != 0 ) {
                 $result_Delivery_place_name = $this->d_m->addDelivery_place_name($area_id , $Delivery_place_name,$Delivery_charge , $active_date);

                if($result_Delivery_place_name){

                    $this->session->set_flashdata('successfully', 'successfully added this place!!');
                    redirect('admin/Delivery/Add_Delivery');
                }else{

                    $this->session->set_flashdata('Error', 'Error to added this place!!');
                    redirect('admin/Delivery/Add_Delivery');
                }
               
            }else{
                $this->session->set_flashdata('Error', 'Enter Area Name!!');
                redirect('admin/Delivery/Add_Delivery');
            }

           
        }else{

                $this->session->set_flashdata('Error', 'Error to added this place!!');
               redirect('admin/Delivery/Add_Delivery');
        }
    }

    public function allDelivery_place($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Delivery/allDelivery_place/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->d_m->allPlacesDelivery();
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['places']=$this->d_m->Delivery_allPlaces($config['per_page'],  $current_page);

        $this->load->model('Restaurant_model');
        $data['Restaurant_model'] = $this->Restaurant_model; 
    	
    		$this->load->view('admin/inc/header');
	        $this->load->view('admin/inc/sidebar');
			$this->load->view('admin/Delivery/all_delivery_place', $data);
			$this->load->view('admin/inc/footer');
    	
    }

    public function delete_place($delivery_id)
    {
    	$result_Delivery_place_delete = $this->d_m->delivery_place_delete($delivery_id);
        if($result_Delivery_place_delete){
            $this->session->set_flashdata('coupon_done', 'Delivery Place successfully delete!!');
            redirect('admin/Delivery/allDelivery_place/');

        }else{
            $this->session->set_flashdata('error_coupon', 'Error to Delivery Place delete!!');
            redirect('admin/Delivery/allDelivery_place/');
        }
    }



    // add area

    public function delivery_area()
    {
        if($this->session->userdata('current_user_type')=='admin'){

            $ares_alls = $this->d_m->area_delivery_all();

            $data  = array(
                'ares_alls' => $ares_alls, 
            );

            $this->load->view('admin/inc/header');
            $this->load->view('admin/inc/sidebar');
            $this->load->view('admin/Delivery/add_Delivery_area', $data);
            $this->load->view('admin/inc/footer');
        }

    }

    public function add_delivery_area()
    {
        $area_name = $this->input->post('area_name');
        $area_Charge = $this->input->post('area_Charge'); 

        if($area_name != NULL){

            $result_Delivery_area_name = $this->d_m->add_delivery_area_model($area_name, $area_Charge);

            if($result_Delivery_area_name){

                $this->session->set_flashdata('successfully', 'successfully added this Area!!');
                redirect('admin/Delivery/delivery_area');
            }else{

                $this->session->set_flashdata('Error', 'Error to added this Area!!');
                redirect('admin/Delivery/delivery_area');
            }
        }else{

                $this->session->set_flashdata('Error', 'Error to added this Area!!');
               redirect('admin/Delivery/delivery_area');
        }
    }


    public function delete_delivery_area($delivery_area_id)
    {
        $result_delete_delivery_area = $this->d_m->delete_delivery_area_model($delivery_area_id);
        if($result_delete_delivery_area){
            $this->session->set_flashdata('delete_area', 'Delivery Area successfully deleted!!');
            redirect('admin/Delivery/delivery_area');

        }else{
            $this->session->set_flashdata('error_delete_area', 'Error to Delivery Area delete!!');
            redirect('admin/Delivery/delivery_area');
        }
    }



}