<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('restaurant_model');
    }

    public function index($current_page=null)
    {
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Discount/index/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->restaurants_counts_for_discount();
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['restaurants']=$this->restaurant_model->restaurants_for_discount($config['per_page'],  $current_page);
    	
    		$this->load->view('admin/inc/header');
	        $this->load->view('admin/inc/sidebar');
			$this->load->view('admin/discount/discount_restaurant', $data);
			$this->load->view('admin/inc/footer');
    	
    }

    public function restaurants_all_product($id=null, $current_page=null)
    {
        $id_1 = $id;
    	$this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Discount/restaurants_all_product/'.$id_1;
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->restaurants_product_counts_for_discount($id_1);
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['products']=$this->restaurant_model->restaurants_product_for_discount($id_1, $config['per_page'],  $current_page);
    	
    		$this->load->view('admin/inc/header');
	        $this->load->view('admin/inc/sidebar');
			$this->load->view('admin/discount/restaurant_products', $data);
			$this->load->view('admin/inc/footer');

    }

    public function discount_edit($id=null){

        $products = $this->restaurant_model->get_products($id);


        $data  = array(
            'products' => $products 
        );

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/discount/edit_discount', $data);
        $this->load->view('admin/inc/footer');

    }


    public function get_descount($real_price, $discount_number){

        $discount_price = (($discount_number/100)*$real_price);

        $price_after_discount = ($real_price - $discount_price);

        return $price_after_discount;

    }

    public function update_discount()
    {
        $product_id = $this->input->post('product_id');
        $real_price = $this->input->post('real_price');
        $discount_number = $this->input->post('discount');

        if($discount_number != NULL){

            $price_after_discount = $this->get_descount($real_price, $discount_number);

            $result_discount = $this->restaurant_model->discount_Update_model( $product_id,$price_after_discount , $discount_number);

            if($result_discount){

                $this->session->set_flashdata('discount_done', 'Discount successfully done!!');
                redirect('admin/Discount/discount_edit/'.$product_id);
            }else{

                $this->session->set_flashdata('error_discount', 'Error to Discount!!');
                redirect('admin/Discount/discount_edit/'.$product_id);
            }
        }else{

                $this->session->set_flashdata('error_discount', 'Error to Discount!!');
                redirect('admin/Discount/discount_edit/'.$product_id);
        }
    }



    // /Discount by rastaurant done
    ///////////////////////////////////////////
    ////////////////////////////////////////
    //???????????????????????????????????????//////////////////



    public function all_product($current_page=null)
    {


        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Discount/all_product/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->all_food_product();
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['products']=$this->restaurant_model->get_all_food_product($config['per_page'],  $current_page);
        
            $this->load->view('admin/inc/header');
            $this->load->view('admin/inc/sidebar');
            $this->load->view('admin/discount/discount_by_product', $data);
            $this->load->view('admin/inc/footer');
    }
    
    
    
     //add Coupon form their

    
     public function Coupon_add()
    {   

        $all_restaurants = $this->restaurant_model->all_restaurants_for_coupon();

        $data  = array(
            'all_restaurants' => $all_restaurants 
        );

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/Coupon/add_coupon', $data);
        $this->load->view('admin/inc/footer');
    }
     public function Coupon_add_img()
    {   

        $all_restaurants = $this->restaurant_model->all_restaurants_for_coupon();

        $data  = array(
            'all_restaurants' => $all_restaurants 
        );

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/Coupon/add_coupon_img', $data);
        $this->load->view('admin/inc/footer');
    }
    public function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 

        
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        // echo"<pre>";
        // print_r($file_name);
        // exit;
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF','webp' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
   public function category($category_id=null){
    $data=array();
    // if($this->input->post('save_new_cat')){
        
    //     $server= "https://admin.duare.net/uploads/products/";
    //     $attr=array(
    //         'category_name' => $this->input->post('category_name'),
            
    //     );
    //     if($_FILES['category_image']["name"]){
           
    //         if($category_image = $this->do_upload('uploads/products/','category_image')){
    //             $attr['category_image']=$server.$category_image;
    //         }else{
    //             $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
    //         }
    //     }
    //     if($attr){
    //        if($this->db->insert('category', $attr)){
    //            $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
    //        }else{
    //             $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
    //        } 
    //     }
    // }
    // for update category
    if($this->input->post('save_cat')){
        $server= "https://admin.duare.net/uploads/";
        // $attr=array(
        //     'category_name' => $this->input->post('category_name'),
            
        // );
        //file upload
        if($_FILES['img_name']["name"]){
            if($coupon_image = $this->do_upload('./uploads/','img_name')){
                $attr['img_name']=$server.$coupon_image;
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
            }
        }
        // if($_FILES['category_image']["name"]){
           
        //     if($category_image = $this->do_upload('uploads/products/','category_image')){
        //         $attr['category_image']=$server.$category_image;
        //     }else{
        //         $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
        //     }
        // }
        if($attr and $category_id){
           if($this->db->where('id', $category_id)->update('tbl_coupon_img', $attr)){
               $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
           }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
           } 
        }
    }
    $data['categories'] = $this->product_model->total_category();
    if($category_id){
        $data['cat_id'] = $category_id;
        $data['single_cat']=$this->product_model->edit_category_by_id($category_id);
    }
    $this->load->view('admin/products/category', $data);
}
    public function add_new_coupon_img(){
        if($this->input->post('save_user')){
            // $attr=array(
            //     'name' => $this->input->post('product_name')
            // );
            if($_FILES['img_name']["name"]){
                if($coupon_image = $this->do_upload('./uploads/products/', 'img_name')){
                    $attr['img_name']=$coupon_image;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
            if($coupon_id = $this->db->insert('tbl_coupon_img', $attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                $coupon_id = $this->db->insert_id();
                redirect('admin/Discount/Coupon_add_img');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
            }
        }
        $data['shopList'] = $this->product_model->shopList();
        $data['subcategoryList'] = $this->product_model->subCategoryList();
        $this->load->view('admin/products/add_new', $data);
    }

     public function new_add_coupon()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('coupon', 'Coupon', 'required');

        if($this->form_validation->run()){
            $restaurant_id = $this->input->post('restaurant_id');
            $coupon = $this->input->post('coupon');
            $coupon_value = $this->input->post('coupon_value');
            $coupon_start_date = $this->input->post('coupon_start_date');
            $coupon_end_date = $this->input->post('coupon_end_date');

            if($coupon_value != NULL){

                $coupon_percentage = $coupon_value." % discount";

                $result_coupon = $this->restaurant_model->new_coupon_add( $restaurant_id, $coupon , $coupon_value , $coupon_percentage, $coupon_start_date, $coupon_end_date);

                if($result_coupon){

                    $this->session->set_flashdata('coupon_done', 'coupon successfully added!!');
                    redirect('admin/Discount/Coupon_add/');
                }else{

                    $this->session->set_flashdata('error_coupon', 'Error to add coupon!!');
                    redirect('admin/Discount/Coupon_add/');
                }
            }else{

                    $this->session->set_flashdata('error_coupon', 'Error to add coupon!!');
                    redirect('admin/Discount/Coupon_add/');
            }
        }else{
            $this->session->set_flashdata('error_coupon', 'Please Enter 8 length Coupon!!');
            redirect('admin/Discount/Coupon_add/');
        }
    }
    
    
    // all coupon

    public function show_all_coupon($current_page=null)
    {
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/Discount/show_all_coupon/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->coupon_count();
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['coupons']=$this->restaurant_model->all_coupon($config['per_page'],  $current_page);
        
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/Coupon/show_all_coupons', $data);
        $this->load->view('admin/inc/footer');
    }

    public function delete_coupon($coupon_id)
    {
        $result_coupon_delete = $this->restaurant_model->coupons_delete($coupon_id);
        if($result_coupon_delete){
            $this->session->set_flashdata('coupon_done', 'coupon successfully delete!!');
            redirect('admin/Discount/show_all_coupon/');

        }else{
            $this->session->set_flashdata('error_coupon', 'Error to add delete!!');
            redirect('admin/Discount/show_all_coupon/');
        }
    }
    
}