<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->restaurant_id = $this->session->userdata('restaurant_id');
        $this->user_id = $this->session->userdata('current_user_id');
        $this->user_type= $this->session->userdata('current_user_type');
        $this->load->model('restaurant_model');
        $this->load->model('product_model');
    }
	public function index()
	{
        $this->placed();
	}
    
    // load all placed order
    public function placed($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/orders/placed';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->order_model->orders_count('placed');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->order_model->orders('placed', $config['per_page'],  $current_page);
		// echo"<pre>";
        // print_r($data);
        // echo"<pre>";
        // return;
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/orders/placed', $data);
        $this->load->view('admin/inc/footer');
    }
    
    // load all processing order
    public function processing($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/orders/processing/';
        $config['use_page_numbers'] = FALSE;
        $this->add_query();
        
        $config['total_rows'] = $this->order_model->orders_count('processing');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';

		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        $this->add_query();
        $data['orders']=$this->order_model->orders('processing', $config['per_page'],  $current_page);
        // echo"<pre>";
        // print_r($data);
        // echo"<pre>";
        // return;
		
        $this->load->view('admin/orders/processing', $data);
    }
    
    // load all pickup order
    public function pickup($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/oreders/pickup/';
        $config['use_page_numbers'] = FALSE;
        $this->add_query();
        $config['total_rows'] = $this->order_model->orders_count('pickup');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        $this->add_query();
        $data['orders']=$this->order_model->orders('pickup', $config['per_page'],  $current_page);
		
        $this->load->view('admin/orders/pickup', $data);
    }
    
    // load all delivered order
    public function delivered($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/orders/delivered/';
        $config['use_page_numbers'] = FALSE;
        $this->add_query();
        $config['total_rows'] = $this->order_model->orders_count('delivered');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        $this->add_query();
        $data['orders']=$this->order_model->orders('delivered', $config['per_page'],  $current_page);
		
        $this->load->view('admin/orders/delivered', $data);
    }
    
    
    // this extra query add for deliver man
    public function add_query(){
        if($this->user_type!='admin'){
            $this->db->where('take_by', $this->user_id);
        }
    }
    
    // load all delivered order
    public function taken($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/orders/taken/';
        $config['use_page_numbers'] = FALSE;
        $this->add_query();
        $config['total_rows'] = $this->order_model->orders_count('placed');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        $this->add_query();
        $data['orders']=$this->order_model->orders('placed', $config['per_page'],  $current_page);
		
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/orders/placed', $data);
        $this->load->view('admin/inc/footer');
    }
    
    
    // public function take order for deliver man
    public function take($order_id=null){
        if(!$order_id or !is_numeric($order_id)){redirect('admin/orders/placed');}
        $order = $this->order_model->order_by_id($order_id);
        if(!$order or $order->take_by){
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('admin/orders/placed');
        }
        if($this->db->where('id', $order_id)->update('invoice', array('take_by'=>$this->user_id))){
            $this->session->set_userdata('success_msg', $this->lang->line('save_success_message'));
            redirect('admin/orders/taken/');
        }else{
            $this->session->set_userdata('error_msg', $this->lang->line('save_error_message'));
            redirect('admin/orders/placed');
        }
    }
    
    
    //this function for view order
    public function view($order_id=null, $type=null){
        if(!$order_id or !is_numeric($order_id)){redirect('admin/orders/placed');}
        $order = $this->order_model->order_by_id($order_id);
        if(!$order){redirect('admin/orders/placed');}
        
        //change status
        if($this->input->post('change_status')){
            $status = $this->input->post('status');
            
            if($this->session->userdata('current_user_type')=='deliver_man' and $type=='delivered'){
                $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
                redirect('admin/orders/view/'.$order_id.'/'.$type);
            }
            
            if($this->db->where('id', $order_id)->update('invoice', array('status'=>$status))){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_message'));
                redirect('admin/orders/view/'.$order_id.'/'.$status);
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_message'));
            }
        }
        
        
        $data['type']=$type;
        // $data['restaurant_data'] = $this->restaurant_model->restaurant_by_id($this->restaurant_id);
        // $data['order'] = $this->order_model->order_by_id($order_id);
        $data['orders'] = $this->order_model->orderInfoAll($order_id);
        // echo"<pre>";
        // print_r($data);
        // echo"<pre>";
        // return;
        $this->load->view('admin/orders/view', $data);
    }
    
    
    //this function for view order
    public function printpreview($order_id=null){
        if(!$order_id or !is_numeric($order_id)){
            redirect('admin/orders/placed');
        }
        $order = $this->order_model->order_by_id($order_id);
        if(!$order){
            redirect('admin/orders/placed');
        }
       
        
        $data['restaurant_data'] = $this->restaurant_model->restaurant_by_id($this->restaurant_id);
        $data['order'] = $this->order_model->order_by_id($order_id);
        $this->load->view('admin/orders/print', $data);
    }
    
    
    
    //public function delete
    public function delete($order_id=null, $type=null){
        if(!$order_id or !is_numeric($order_id)){redirect('admin/orders/'.$type);}
        $order = $this->order_model->order_by_id($order_id);
        if(!$order){redirect('admin/orders/'.$type);}
        if($this->session->userdata('current_user_type')!='admin'){
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('admin/orders/'.$type);
        }
        
        if($this->permission->can()){
            if($this->db->where('id', $order_id)->delete('invoice')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                redirect('admin/orders/'.$type);
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('admin/orders/'.$type);
            }
        }
    }

    public function OrderView($order_id = null)
    {


        if ($order_id != null) {
            $order = $this->order_model->orderInfoAll($order_id);
        }

        $data  = array(
            'orders' => $order, 
        );

        //  echo "<pre>";
        //  print_r($data);
        //  exit;

        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar'); 
        $this->load->view('admin/orders/rest_processing_view', $data);
        $this->load->view('admin/inc/footer');
    }
    
    
    
    
}

