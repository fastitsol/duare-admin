<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('restaurant_model');
        if($this->user_type=$this->session->userdata('current_user_type')!='admin'){
            redirect('admin/deshboard');
        }
        
    }
	public function index()
	{
        $this->active();
	}
    
    // load all active resturants
    public function active($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/restaurants/active/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->restaurants_count('active');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['restaurants']=$this->restaurant_model->restaurants($config['per_page'],  $current_page, 'active');
		
        $this->load->view('admin/inc/header');
        $this->load->view('admin/inc/sidebar');
        $this->load->view('admin/restaurants/active', $data);
        $this->load->view('admin/inc/footer');
    }
    
    
    
    // load all product
    public function inactive($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/restaurants/inactive/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->restaurants_count('inactive');
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['restaurants']=$this->restaurant_model->restaurants($config['per_page'],  $current_page, 'inactive');
		
        $this->load->view('admin/restaurants/inactive', $data);
    }
    
    
    
    //this function for change status
    public function change_status($restaurant_id=null, $status=null){
        if(!$restaurant_id or !is_numeric($restaurant_id)){redirect('admin/restaurants/'.$status);}
        if($this->db->where('id', $restaurant_id)->update('tbl_restaurant', array('status'=>$status))){
            $this->session->set_userdata('success_msg', $this->lang->line('save_success_message'));
            redirect('admin/restaurants/'.$status);
        }else{
            $this->session->set_userdata('error_msg', $this->lang->line('save_error_message'));
            redirect('admin/restaurants/'.$status);
        }
    }
    
    
    
}

