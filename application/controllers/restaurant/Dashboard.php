<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Restaurant_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('restaurant_model');
        $this->restaurant_id = $this->session->userdata('restaurant_id');

        
    }
	public function index()
	{
         
        $restaurant_id = $this->restaurant_id;
        
        // echo "<pre>";
        // print_r($restaurant_id);
        // exit;

        $restaurant_data = $this->restaurant_model->restaurant_by_id($this->session->userdata('restaurant_id'));


        $order_received = $this->restaurant_model->orders_count('placed', $restaurant_id);

        $order_processing = $this->restaurant_model->orders_count('processing', $restaurant_id);

        $order_delivered = $this->restaurant_model->orders_count('delivered', $restaurant_id);


        $total_products = $this->restaurant_model->restaurants_product_counts_for_discount($restaurant_id);

        $total_available_products = $this->restaurant_model->total_available_products($restaurant_id);

        $all_order_javas = $this->restaurant_model->show_all_order_for_java_script($restaurant_id);
        

        $lastYearIncome = $this->restaurant_model->last_yearIncome($restaurant_id);

        $thisMonthIncome = $this->restaurant_model->thisMonthIncome($restaurant_id);

          // echo "<pre>";
          // print_r($lastYearIncome);
          // exit;
        $data  = array(
            'restaurant_data' => $restaurant_data ,
            'order_received' => $order_received ,
            'order_processing' => $order_processing,
            'order_delivered' => $order_delivered,
            'all_order_javas' => $all_order_javas,
            'total_products' => $total_products,
            'total_available_products' => $total_available_products,
            'lastYearIncomes'    => $lastYearIncome,
            'thisMonthIncomes'  => $thisMonthIncome

        );

        // echo "<pre>";
        // print_r($data);
         
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
		$this->load->view('restaurant/dashboard', $data);
        $this->load->view('restaurant/inc/footer');
	}
    
    
    public function play_push($availability=null){
        if($availability=='unavailable' or $availability='available'){
            if($this->db->where('id', $this->restaurant_id)->update('tbl_restaurant', array('availability'=>$availability))){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            }
        }
        redirect('restaurant/dashboard');
    }
    
    


    public function discount_resaurant($current_page=null)
    {
        $id_1 = $this->restaurant_id;

        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/Dashboard/discount_resaurant';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->restaurants_product_counts_for_discount($id_1);
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['products']=$this->restaurant_model->restaurants_product_for_discount($id_1, $config['per_page'],  $current_page);
        
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/discount/restaruant_product', $data);
        $this->load->view('restaurant/inc/footer');

    }

    public function discount_edit($id=null){


        
        $products = $this->restaurant_model->get_products($id);


        $data  = array(
            'products' => $products 
        );

        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/discount/discount_edit_rest', $data);
        $this->load->view('restaurant/inc/footer');

    }



    public function get_descount($real_price, $discount_number){

        $discount_price = (($discount_number/100)*$real_price);

        $price_after_discount = ($real_price - $discount_price);

        return $price_after_discount;

    }

    public function update_discount()
    {
        $product_id = $this->input->post('product_id');
        $real_price = $this->input->post('real_price');
        $discount_number = $this->input->post('discount');

        if($discount_number != NULL){

            $price_after_discount = $this->get_descount($real_price, $discount_number);

            $result_discount = $this->restaurant_model->discount_Update_model( $product_id,$price_after_discount , $discount_number);

            if($result_discount){

                $this->session->set_flashdata('discount_done', 'Discount successfully done!!');
                redirect('restaurant/Dashboard/discount_resaurant');
            }else{

                $this->session->set_flashdata('error_discount', 'Error to Discount!!');
                redirect('restaurant/Dashboard/discount_resaurant');
            }
        }else{

                $this->session->set_flashdata('error_discount', 'Error to Discount!!');
                redirect('restaurant/Dashboard/discount_resaurant');
        }
    }




    // new
    public function show_all_discount_product($current_page=null)
    {
        $id_1 = $this->restaurant_id;

        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/Dashboard/show_all_discount_product';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->restaurant_model->discount_all_products($id_1);
        $config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['last_link'] = 'LAST →';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '← FIRST';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<a>';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['products']=$this->restaurant_model->Discount_productsForShowing($id_1, $config['per_page'],  $current_page);
        
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/discount/allDiscountProduct', $data);
        $this->load->view('restaurant/inc/footer');

    }

        // new today
    public function ResYearReport()
    {
        $orders = $this->restaurant_model->resthisYearReport($this->restaurant_id);

        $data  = array(
            'orders' => $orders, 
        );
        // echo"<pre>";
        // print_r($data);
        // return;
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/RestaurantReport/yearReport', $data);
        $this->load->view('restaurant/inc/footer');

    }


    public function ResMonthlyReport()
    {
        $orders = $this->restaurant_model->resthisMonthReport($this->restaurant_id);
        // $data['orders']= $this->restaurant_model->resthisMonthReport($this->restaurant_id);

        $data  = array(
            'orders' => $orders, 
        );
// echo "<pre>";
//         print_r($data);
//         echo "<pre>";
//         return;
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/RestaurantReport/monthReport', $data);
        $this->load->view('restaurant/inc/footer');
    }

    
    
}

