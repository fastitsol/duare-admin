<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Restaurant_Controller{
    public function __construct(){
        parent::__construct();
        $this->restaurant_id = $this->session->userdata('restaurant_id');
        $this->load->model('restaurant_model');
        $this->load->model('product_model');
    }
	public function index()
	{
        $this->placed();
	}
    
    // load all placed order
    public function placed($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/orders/placed/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->order_model->orders_count('placed', $this->restaurant_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        // $data['restaurant_id']=$this->restaurant_id;
        
        $data['orders']=$this->order_model->orderss('placed', $config['per_page'],  $current_page, $this->restaurant_id);
        
        // echo"<pre>";
        // print_r($data);
        // echo"<pre>";
        // return;
		$this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar'); 
        $this->load->view('restaurant/orders/placed', $data);
        $this->load->view('restaurant/inc/footer');
    }
    
    // load all processing order
    public function processing($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/oreders/processing/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->order_model->orders_count('processing', $this->restaurant_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->order_model->orderss('processing', $config['per_page'],  $current_page, $this->restaurant_id);
		
		$this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/orders/processing', $data);
        $this->load->view('restaurant/inc/footer');
    }
    
    // load all pickup order
    public function pickup($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/oreders/pickup/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->order_model->orders_count('pickup', $this->restaurant_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->order_model->orderss('pickup', $config['per_page'],  $current_page, $this->restaurant_id);
		
		$this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/orders/pickup', $data);
        $this->load->view('restaurant/inc/footer');
    }
    
    // load all delivered order
    public function delivered($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/oreders/delivered/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->order_model->orders_count('delivered', $this->restaurant_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['orders']=$this->order_model->orderss('delivered', $config['per_page'],  $current_page, $this->restaurant_id);
		
		$this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/orders/delivered', $data);
        $this->load->view('restaurant/inc/footer');
    }
    
    
    //this function for view order
    public function view($order_id=null, $type=null){
        if(!$order_id or !is_numeric($order_id)){redirect('restaurant/orders/placed');}
        $order = $this->order_model->order_by_id($order_id, $this->restaurant_id);
        if(!$order){redirect('restaurant/orders/placed');}
        
        //change status
        if($this->input->post('change_status')){
            $status = $this->input->post('status');
            if($this->db->where('id', $order_id)->update('invoice', array('status'=>$status))){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_message'));
                redirect('restaurant/orders/view/'.$order_id.'/'.$status);
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_message'));
            }
        }
        
        
        $data['type']=$type;
        $data['restaurant_data'] = $this->restaurant_model->restaurant_by_id($this->restaurant_id);
        $data['order'] = $this->order_model->order_by_id($order_id, $this->restaurant_id);
        $this->load->view('restaurant/orders/view', $data);
    }
    
    
    //this function for view order
    public function printpreview($order_id=null){
        if(!$order_id or !is_numeric($order_id)){redirect('restaurant/orders/placed');}
        $order = $this->order_model->order_by_id($order_id, $this->restaurant_id);
        if(!$order){redirect('restaurant/orders/placed');}
       
        
        $data['restaurant_data'] = $this->restaurant_model->restaurant_by_id($this->restaurant_id);
        $data['order'] = $this->order_model->order_by_id($order_id);
        $this->load->view('restaurant/orders/print', $data);
    }
    
    
    
    //public function delete
   /* public function delete($order_id=null, $type=null){
        if(!$order_id or !is_numeric($order_id)){redirect('restaurant/orders/'.$type);}
        $order = $this->order_model->order_by_id($order_id);
        if(!$order){redirect('restaurant/orders/'.$type);}
        
        
        if($this->permission->can()){
            if($this->db->where('id', $order_id)->delete('invoice')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                redirect('restaurant/orders/'.$type);
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('restaurant/orders/'.$type);
            }
        }
    }*/
    
     // new code

    public function RestordersView($order_id = null)
    {
        if ($order_id != null) {
            $data['orders'] = $this->restaurant_model->orderInfoAll($order_id, $this->restaurant_id);
        }
        // $data  = array(
        //     'orders' => $order,  
        // );
        // echo"<pre>";
        // print_r($orders);
        // return;
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar'); 
        $this->load->view('restaurant/orders/order_new_view', $data);
        $this->load->view('restaurant/inc/footer');
    }
    
    public function restProcessingOrderView($order_id = null)
    {
        if ($order_id != null) {
            // $order = $this->restaurant_model->orderInfoAll($order_id);
            $order = $this->restaurant_model->orderInfoAll($order_id, $this->restaurant_id);
        }
        $data  = array(
            'orders' => $order, 
        );
        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar'); 
        $this->load->view('restaurant/orders/rest_processing_view', $data);
        $this->load->view('restaurant/inc/footer');
    }
    // new totady

     public function rest_penddingToProcessing($invoice_id)
    {

        $data =  array(
            'status' => 'Processing', 

        );

        $updatePenddingToProcessing = $this->restaurant_model->rest_updatePenddingToProcessing($invoice_id, $data);

        if ($updatePenddingToProcessing) {
                
            $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            redirect('restaurant/orders/processing/');
        }
    } 
    
}

