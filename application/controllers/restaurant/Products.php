<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Restaurant_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('MedicineModel', "m_m");
        $this->restaurant_id = $this->session->userdata('restaurant_id');
    }
	public function index()
	{
        $this->all_product();
	}
    public function search($current_page=null)
	{
        $this->load->library('form_validation');
        $this->form_validation->set_rules('query', 'Query', 'required');
        if($this->form_validation->run()){   
        }
        $query = $this->input->post('query');
        $data['products'] = $this->product_model->searchByRestaurant($query, $this->restaurant_id);
        $this->load->view('restaurant/products/search_result', $data);
            
	}
    // load all product
    public function all_product($current_page=null){
        $this->load->library('pagination');
        $config['base_url'] = base_url().'restaurant/products/all_product/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->product_model->products_count('food', $this->restaurant_id);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        
        $data['products']=$this->product_model->products('food', $config['per_page'],  $current_page, $this->restaurant_id);
		// echo "<pre>";
        // print_r($data['products']);
        $this->load->view('restaurant/products/all_products', $data);
    }
    
    //products add new function
    public function add_new(){
        if($this->input->post('save_new_product') and $this->permission->can()){
            // set form validation roles
            $this->form_validation->set_rules('product_name', $this->lang->line('product_name'), 'trim|required');
            // $this->form_validation->set_rules('company_name', $this->lang->line('company_name'), 'trim|required');
            $this->form_validation->set_rules('price', $this->lang->line('price'), 'trim|is_numeric|required');
            
            $opening_time  = date("H:i", strtotime($this->input->post('opening_time')));
            $closing_time  = date("H:i", strtotime($this->input->post('closing_time')));
            
            $price=$this->input->post('price');
            $discount_percent=$this->input->post('discount_percent');
            if($discount_percent == ""){
                $discount_percent = 0;
            }
            $discounted_price=$this->product_model->price_in_discount($price, $discount_percent);
            // if this form validation working
            if($this->form_validation->run()){
                $attr=array(
                    'name'            => $this->input->post('product_name'),
                    'medicine_company'=> $this->input->post('company_name'),
                    'subCategory_id'=> $this->input->post('subCategory_id'),
                    'purchasePrice'   => $this->input->post('purchasePrice'),
                    'price'           => $this->input->post('price'),
                    'discount_percent'=> $this->input->post('discount_percent'),
                    'discount_price'  => $discounted_price,
                    'size'            => $this->input->post('size'),
                    'description'     => $this->input->post('description'),
                    'product_type'    => 'food',
                    'restaurant_id'   => $this->restaurant_id,
                    'opening_time'    => $opening_time,
                    'closing_time'    => $closing_time,
                    'availability'    => $this->input->post('availability'),
                );
                
                //file upload
                if($_FILES['product_image']["name"]){
                    if($product_image = $this->do_upload('./uploads/products/', 'product_image')){
                        $attr['image']=$product_image;
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                    }
                }
                // else{
                //     $product_image = $this->do_upload('./uploads/products/', 'grocery.png');
                //         $attr['image']=$product_image;
                    
                //     $attr['image']='grocery.png';
                // }
                
                if($product_id = $this->db->insert('all_products', $attr)){
                    $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    $product_id = $this->db->insert_id();
                    // update categories
                    if($p_categories = $this->input->post('cat_id') and $product_id){
                        $this->update_category($product_id, $p_categories);
                    }
                    
                    redirect('restaurant/products/all_product');
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }
        // $data['categories'] = $this->product_model->category($this->restaurant_id);
        $data['subcategoryList'] = $this->product_model->subCategoryList();
        $this->load->view('restaurant/products/add_new', $data);
    }
    
    //products add new function
    public function edit_product($product_id=null){
        if(!$product_id or !is_numeric($product_id)){redirect('restaurant/products/all_product');}
        $product = $this->product_model->product_by_id($product_id);
        
        if($this->restaurant_id != $product->restaurant_id){redirect('restaurant/products/all_product');}
        
        if($this->input->post('save_product') and $this->permission->can()){
            // set form validation roles
            $this->form_validation->set_rules('product_name', $this->lang->line('product_name'), 'trim|required');
            $this->form_validation->set_rules('price', $this->lang->line('price'), 'trim|is_numeric|required');
            
            $opening_time  = date("H:i", strtotime($this->input->post('opening_time')));
            $closing_time  = date("H:i", strtotime($this->input->post('closing_time')));

            $price=$this->input->post('price');
            $discount_percent=$this->input->post('discount_percent');
            if($discount_percent == ""){
                $discount_percent = 0;
            }
            $discounted_price=$this->product_model->price_in_discount($price, $discount_percent);
            // if this form validation working
            if($this->form_validation->run()){
                $attr=array(
                    'name'            => $this->input->post('product_name'),
                    'medicine_company'=> $this->input->post('company_name'),
                    'subCategory_id'=> $this->input->post('subCategory_id'),
                    'purchasePrice'   => $this->input->post('purchasePrice'),
                    'price'           => $this->input->post('price'),
                    'discount_percent'           => $this->input->post('discount_percent'),
                    'discount_price'  => $discounted_price,
                    'size'            => $this->input->post('size'),
                    'description'     => $this->input->post('description'),
                    'opening_time'   => $opening_time,
                    'closing_time'   => $closing_time,
                    'availability'   => $this->input->post('availability'),
                );
                
                //file upload
                if($_FILES['product_image']["name"]){
                    if($product_image = $this->do_upload('./uploads/products/', 'product_image')){
                        $attr['image']=$product_image;
                        
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                    }
                }
                // else{
                //     $attr['image']='grocery.png';
                // }
                
                // insert the product to database
                if($this->db->where('id', $product_id)->update('all_products', $attr)){
                    
                    // update categories
                    if($p_categories = $this->input->post('cat_id') and $product_id){
                        $this->update_category($product_id, $p_categories);
                    }
                    
                    
                    $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    if(isset($attr['image']) and $attr['image']){
                        $path='./uploads/products/'.$product->image;
                        $this->remove_file($path);
                    }
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                }
            }else{
                $errors = '<p>'.$this->lang->line('save_error_msg').'</p>';
                $errors .= validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }
        $data['categories'] = $this->product_model->category($this->restaurant_id);
        $p_categories = $this->product_model->product_categories($product_id);
        $product_categories = array();
        if($p_categories){
            foreach($p_categories as $item){
                $product_categories[]=$item->category_id;
            }
        }
        
        $data['product_categories'] = $product_categories;
        $data['product'] = $this->product_model->product_by_id($product_id);
        $data['product_id'] = $product_id;
        $data['subcategoryList'] = $this->product_model->subCategoryList();
        $this->load->view('restaurant/products/edit_product', $data);
    }
    
    
    //This function for update porodect category
    public function update_category($product_id, $categories){
        if($categories){
            $this->db->where('product_id', $product_id)->delete('tbl_products_category');
            $data=array();
            foreach($categories as $category){
                $data[] = array('product_id'=>$product_id, 'category_id'=>$category);
            }
            if($this->db->insert_batch('tbl_products_category', $data)){
                return true;
            }else{
                return false;
            }
        }
    }
    
    //public function delete
    public function delete($product_id=null){
        if(!$product_id or !is_numeric($product_id)){
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('restaurant/products/all_product');
        }
        $product = $this->product_model->product_by_id($product_id);
        if($this->restaurant_id != $product->restaurant_id){redirect('restaurant/products/all_product');}
        
        
        if($this->permission->can()){
            if($this->db->where('id', $product_id)->delete('all_products')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                
                $path='./uploads/products/'.$product->image;
                $this->remove_file($path);
                
                redirect('restaurant/products/all_product');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('restaurant/products/all_product');
            }
        }
    }
    
    
    //validate unique id
    /*public function unique_alt_id($alt_id, $perams){
        $this->db->where_not_in('id', $perams);
        $this->db->where_in('alt_id', $alt_id);
        $results = $this->db->get('products');
        if($results->result()){
            $this->form_validation->set_message('unique_alt_id', $this->lang->line('unique_alt_id'));
            return false;
        }else{
            return true;
        }
    }*/
    
    
    //product category 
    public function category($category_id=null){
        $data=array();
        if($this->input->post('save_new_cat')){
            $this->form_validation->set_rules('category_name', 'category_name', 'trim|unique|required');
            if($this->form_validation->run()){
                $server= "https://admin.duare.net/uploads/products/";
                $attr=array(
                    'category_name' => $this->input->post('category_name'),
                    
                );
                //file upload
                if($_FILES['category_image']["name"]){
                
                    if($category_image = $this->do_upload('./uploads/products/','category_image')){
                        $attr['category_image']=$server.$category_image;
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                    }
                }
                // $category_name = $this->input->post('category_name');
                if($attr){
                    if($this->db->insert('category', $attr)){
                        $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    }else{
                        $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                    } 
                }
            }
            else{
                $errors = '<p>Category Name Must be Unique</p>';
                $errors .= validation_errors('<p>', '</p>');
                $this->session->set_userdata('error_msg', $errors);
            }
        }
        // for update category
        if($this->input->post('save_cat')){
            $server= "https://admin.duare.net/uploads/products/";
            $attr=array(
                'category_name' => $this->input->post('category_name'),
                
            );
            //file upload
            if($_FILES['category_image']["name"]){
               
                if($category_image = $this->do_upload('./uploads/products/','category_image')){
                    $attr['category_image']=$server.$category_image;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
            if($attr and $category_id){
                if($this->db->where('id', $category_id)->update('category', $attr)){
                    $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                }else{
                     $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                } 
             }
        }
        // $data['categories'] = $this->product_model->category($this->restaurant_id);
        $data['categories'] = $this->product_model->total_category();
        // $data['count_pro'] = $this->product_model->count_products($this->restaurant_id);
        // echo "<pre>";
        // print_r($data['categories']);

        if($category_id){
            $data['cat_id'] = $category_id;
            
            $data['single_cat']=$this->product_model->edit_category_by_id($category_id);
        }
        $this->load->view('restaurant/products/category', $data);
    }

    public function subcategory($subcategory_id=null){
        $data=array();
        if($this->input->post('save_new_cat')){
            $server= "https://admin.duare.net/uploads/products/";
            $attr=array(
                'category_id' => $this->input->post('category_id'),
                'subCategory_name' => $this->input->post('subCategory_name'),
                
            );
            //file upload
            if($_FILES['image']["name"]){
               
                if($image = $this->do_upload('./uploads/products/','image')){
                    $attr['image']=$server.$image;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
            // $category_id = $this->input->post('category_id');
            // $subCategory_name = $this->input->post('subCategory_name');
            if($attr ){
               if($this->db->insert('sub_category', $attr)){
                   $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
               }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
               } 
            }
        }
        // for update category
        if($this->input->post('save_cat')){
            $server= "https://admin.duare.net/uploads/products/";
            $attr=array(
                'category_id' => $this->input->post('category_id'),
                'subCategory_name' => $this->input->post('subCategory_name'),
                
            );
            //file upload
            if($_FILES['image']["name"]){
               
                if($image = $this->do_upload('./uploads/products/','image')){
                    $attr['image']=$server.$image;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
            if($attr and $subcategory_id){
               if($this->db->where('id', $subcategory_id)->update('sub_category', $attr)){
                   $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
               }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
               } 
            }
        }
        // $data['categories'] = $this->product_model->category();
        $data['categories'] = $this->product_model->total_category();
        $data['subCategories'] = $this->product_model->subCategory();
        if($subcategory_id){
            $data['subcat_id'] = $subcategory_id;
            $data['single_subcat']=$this->product_model->subcategory_by_id($subcategory_id);
        }
        $this->load->view('restaurant/products/subCategory', $data);
    }
    
    // This method for delete a category
    public function delete_category($category_id){
        if(is_numeric($category_id) and $this->permission->can()){
            
            // $this->db->where('restaurant_id', $this->restaurant_id);
            if($this->db->where('id', $category_id)->delete('category')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                redirect('restaurant/products/category');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('restaurant/products/category');
            }
        }else{
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('restaurant/products/category');
        }
    }
    public function delete_subcategory($id){
        if(is_numeric($id) and $this->permission->can()){
            if($this->db->where('id', $id)->delete('sub_category')){
                $this->session->set_userdata('success_msg', $this->lang->line('delete_success_msg'));
                redirect('restaurant/products/subcategory');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('delete_error_msg'));
                redirect('restaurant/products/subcategory');
            }
        }else{
            $this->session->set_userdata('error_msg', $this->lang->line('permission_deny'));
            redirect('restaurant/products/subcategory');
        }
    }

    
    
    
     /*File upload method*/
    public function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF','webp' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
    //This function for remove file from location
    public function remove_file($file){
        if(is_file($file)){
            unlink($file);
        }
    }
    // public function get_order_details($csid){
    //     $order_details = $this->product_model->order_details($csid);
    //     echo "<pre>";
    //     print_r($order_details);
    //     if($order_details){
    //          echo json_encode($order_details);
    //     }else{
    //          echo json_encode(null);
    //     }
    // }
    public function discount()
    {


        $duscount = $this->m_m->discountInAll($this->restaurant_id);

        $data  = array(
            'duscounts' => $duscount, 
        );

        $this->load->view('restaurant/inc/header');
        $this->load->view('restaurant/inc/sidebar');
        $this->load->view('restaurant/products/discount', $data);
        $this->load->view('restaurant/inc/footer');

        // $this->load->view('Medicine/inc/header');
        // $this->load->view('Medicine/inc/sidebar');
        // $this->load->view('Medicine/MedicineDiscount/discountAllMedicine', $data);
        // $this->load->view('Medicine/inc/footer');
    }
    public function AddDiscount()
    {

        $id = $this->restaurant_id;
        $discount_percent =  $this->input->post('discountValue');
        $productPrices =$this->product_model->getResProPrice($id);
        // echo"<pre>";
        // print_r($productPrices);
        // return;
        foreach($productPrices as $productPrice){

            $discounted_price=$this->product_model->price_in_discount($productPrice->price, $discount_percent);

            // $attr=array(
            //     'discount_percent'=> $discount_percent,
            //     'discount_price'  => $discounted_price
            // );
            
            $updateInAllProduct = $this->m_m->updateResAllProduct($discount_percent, $discounted_price, $id);
            //     if ($updateInAllProduct == 1) {
            // $this->db->where('restaurant_id', $id)->update('all_products', $attr);
        }

        $this->load->view('restaurant/products/all_products', $data);

        // $this->load->view('restaurant/inc/header');
        // $this->load->view('restaurant/inc/sidebar');
        // $this->load->view('restaurant/products/discount', $data);
        // $this->load->view('restaurant/inc/footer');
        

        // $date =  date('Y-m-d H:i');

        // $data =  array(
        //     'restaurant_id' => $id, 
        //     'discountValue' => $discountValue, 
        //     'created_at' => $date 
        // );

       

      

    }
    
}

