<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Restaurant_Controller{
    public $user_types;
    public function __construct(){
        parent::__construct();
        $this->user_types = array('admin', 'restaurant');
        $this->user_id = $this->session->userdata('current_user_id');
        $this->restaurant_id = $this->session->userdata('restaurant_id');
        $this->load->model('restaurant_model');
    }
	public function index()
	{
		$this->edit_user();
	}
    
    
    /*This funciton for edit users*/
    public function edit_user(){
        $user_id = $this->user_id;
        $data['user_id']=$this->user_id;
        if($this->input->post('save_user')){
            $update_attr=array(
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'birthday'=>$this->input->post('birthday'),
                'address1'=>$this->input->post('address1'),
                'address2'=>$this->input->post('address2'),
                'country'=>$this->input->post('country'),
                'city'=>$this->input->post('city'),
                'state'=>$this->input->post('state'),
                'nid_number'=>$this->input->post('nid_number'),
                'passport_number'=>$this->input->post('passport_number')
            );
            
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
            }
        }
        
        
        
         $data['user_data']=$this->user_model->get_user($user_id);
        $this->load->view('restaurant/users/edit', $data);
    }
    
    /*This fucntion for edit user photo photo*/
    public function edit_photo(){
        $user_id=$this->user_id;
        if($filename=$this->do_upload('./uploads/users/', 'user_image')){
            $update_attr=array( 'image'=>$filename );
            /*If file uploaded then insert file name in database*/
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                 redirect('restaurant/users/edit_user/');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                 redirect('restaurant/users/edit_user/');
            }
        }else{
            $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
            redirect('restaurant/users/edit_user/');
        }
    }
    
    
    
    /*This function for change password*/
    public function change_password(){
        $user_id=$this->user_id;
        if($this->input->post('change_password')){
            $update_attr=array( 'password'=>md5($this->input->post('password')) );
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                 redirect('restaurant/users/edit_user/');
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                redirect('restaurant/users/edit_user/');
            }
        }
        $data['user_data']=$this->user_model->get_user($user_id);
        $data['user_id']=$user_id;
         $this->load->view('restaurant/users/change_password', $data);
    }
    
    
    // this function for change restaurant information
    public function restaurant(){
        $restaurant = $this->restaurant_model->restaurant_by_id($this->restaurant_id);
        if($this->input->post('save_restaurnt')){
            $update_attr=array(
                'name'=>$this->input->post('restaurant_name'),
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'website'=>$this->input->post('website'),
                'delivery_time'=>$this->input->post('delivery_time'),
                'opening_time'=>$this->input->post('opening_time'),
                'closing_time'=>$this->input->post('closing_time'),
                'address'=>$this->input->post('address'),
                'availability'=>$this->input->post('availability'),
            );
            if($this->db->where('id', $this->restaurant_id)->update('tbl_restaurant', $update_attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
            }else{
                $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
            }
        }
        
        // save files
        if($this->input->post('save_restaurnt_files')){
            $update_attr = array();
            // logo upload
            if($_FILES['restaurant_logo']["name"]){
                if($logo = $this->do_upload('./uploads/restaurant/', 'restaurant_logo')){
                    $update_attr['logo']=$logo;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
                
            // cover upload
            if($_FILES['restaurant_cover']["name"]){
                if($cover = $this->do_upload('./uploads/restaurant/', 'restaurant_cover')){
                    $update_attr['cover']=$cover;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
                
            // trade_licence_image upload
            if($_FILES['trade_licence_image']["name"]){
                if($trade_licence_image = $this->do_upload('./uploads/restaurant/', 'trade_licence_image')){
                    $update_attr['trade_licence_image']=$trade_licence_image;
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('image_upload_error'));
                }
            }
            
            
            // push database
            if($update_attr){
                if($this->db->where('id', $this->restaurant_id)->update('tbl_restaurant', $update_attr)){
                $this->session->set_userdata('success_msg', $this->lang->line('save_success_msg'));
                    // this function for remove files
                    
                    foreach($update_attr as $file_id=>$value){
                        if($file_id == 'logo'){
                            $path='./uploads/restaurant/'.$restaurant->logo;
                        }
                        if($file_id == 'cover'){
                            $path='./uploads/restaurant/'.$restaurant->cover;
                        }
                        if($file_id == 'trade_licence_image'){
                            $path='./uploads/restaurant/'.$restaurant->trade_licence_image;
                        }
                        $this->remove_file($path);
                        
                    }
                }else{
                    $this->session->set_userdata('error_msg', $this->lang->line('save_error_msg'));
                }
            }
            
            
            
        }
        
        $data['restaurant']=$this->restaurant_model->restaurant_by_id($this->restaurant_id);
        $this->load->view('restaurant/users/edit_restaurant', $data);
    }
    

    
       /*File upload method*/
    public function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
    //This function for remove file from location
    public function remove_file($file){
        if(is_file($file)){
            unlink($file);
        }
    }
    
    
}
