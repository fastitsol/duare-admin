<?php
if( ! function_exists('mileageIsActive')){
    function mileageIsActive(){
        $CI =& get_instance();
        return $CI->Home_model->checkPcSettings();
    }
}

if( ! function_exists('get_product_by_id')){
    function get_product_by_id($product_id = null){
        $CI =& get_instance();
        return $CI->MedicineModel->get_product_by_id($product_id);
    }
}

?>
