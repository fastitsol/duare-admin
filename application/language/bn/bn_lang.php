<?php
//Commong languages
$lang['log_in']='লগ ইন';
$lang['log_in_error']='<strong> ভুল লগইন  </strong> ইউজার নেম এবং পাসওয়ার্ড মিলছেনা।';
$lang['remember_me']='আমাকে মনে রাখ';
$lang['password']='পাসওয়ার্ড';
$lang['change_password']='পাসওয়ার্ড পরিবর্তন ';
$lang['new_passord']='নতুন পাসওয়ার্ড ';
$lang['user_name']=' ইউজার নেম ';
$lang['profile']=' প্রোফাইল ';
$lang['log_out']=' লগ আউট ';
$lang['home']=' হোম';
$lang['currency_symbol']='&#x9f3;';
$lang['serial_number']='ক্রমিক নং';
$lang['category_name']='বিভাগের নাম';
$lang['change']='সংশোধন';
$lang['save_button']=' সংরক্ষণ ';
$lang['upload_button']=' অউপ্লোড ';
$lang['worning_delete']='আপনি কী ইহা ডিলিট করতে চাচ্ছেন। একবার ডিলিট হলে এই তথ্যটি আর পাওয়া যাবেনা।';
$lang['save_success_msg']='আপনি তথ্যটি যথাযথ ভাবে সংরক্ষণ করতে সক্ষম হয়েছেন।';
$lang['image_upload_error']='এই ইমেইজটি আপলোড করা সম্ভব হচ্ছেনা। অন্য আরেকটি ইমেইজ আপলোড দিন।';
$lang['save_error_msg']='তথ্যটি যথাযথ ভাবে সংরক্ষণ করা সম্ভব হয়নি।';
$lang['delete_success_msg']='আপনি তথ্যটি যথাযথ ভাবে বাতিল করতে সক্ষম হয়েছেন।';
$lang['delete_error_msg']='তথ্যটি যথাযথ ভাবে বাতিল করা সম্ভব হয়নি।';
$lang['permission_deny']='এই কাজটি করার জন্য আপনার অনুমতি নাই';
$lang['product']=' পণ্য ';
$lang['product_name']=' পণ্যের নাম';
$lang['buy_rate']='ক্রয় মূল্য';
$lang['sale_rate']='বিক্রয় মূল্য';
$lang['stock']='মযুদ';
$lang['description']='বর্ননা';
$lang['add_new_product']='নতুন পণ্য যুক্ত করুন';
$lang['product_id']='পণ্যের আই.ডি.';
$lang['footer_copy_rigt_text']='সফটওয়ার ভার্সন ১.০.১ । সফটওয়ারটি তৈরি করেছে  <a href="https://www.engineerbd.net" target="_blank"> ENGINEER BD </a>';
$lang['name']='নাম';
$lang['email']=' ই-মেইল ';
$lang['phone']=' ফোন ';
$lang['institute']=' প্রতিষ্ঠান ';
$lang['address']=' ঠিকানা ';
$lang['zone']=' এলাকা ';
$lang['customer_name']='কাস্টমারের নাম ';
$lang['date']=' তারিখ ';
$lang['not_found']=' কাঙ্ক্ষিত তথ্যটি সার্ভারে নাই। ';
$lang['print_button']='<i class="fa fa-print"></i> প্রিন্ট ';
$lang['authority_signature'] = ' কর্তৃপক্ষের স্বাক্ষর  ';
$lang['customer_signature']=' ক্রেতার স্বাক্ষর ';
$lang['db_backup']='বেকআপ';

//dashbaord languages
$lang['information']=' বিস্তারিত তথ্য';
$lang['total_customer']=' মোট কাস্টমার সংখ্যা';
$lang['total_importar']=' মোট আমদানী কারক সংখ্যা';
$lang['total_protucts']=' মোট মজুদ পণ্য';
$lang['total_product_value']=' মোট মজুদ পণ্যের মূল্য';
$lang['total_cost_account']='মোট খরচ';
$lang['total_bank_balance']='মোট ব্যাংক ব্যালেন্স';
$lang['today_total_sale_product']='আজকের মোট পণ্য বিক্রয় ';
$lang['today_total_sale_product_value']='আজকের মোট পণ্য বিক্রয় মূল্য';
$lang['total_sale_product']='মোট পণ্য বিক্রয় ';
$lang['total_sale_product_value']='মোট পণ্য বিক্রয় মূল্য';
$lang['total_due_buy_product']='মোট বাকিতে ক্রয়';
$lang['total_due_sale_product']='মোট বাকিতে বিক্রয়';
$lang['person']=' জন ';


//menu languages
$lang['menu_main_menu']='প্রধান মেনু ';
$lang['menu_dashboard']=' ড্যাশবোর্ড ';
$lang['menu_control_panel']=' কন্ট্রল পেনেল  ';
$lang['menu_user']='ইউজার';
$lang['menu_all_user']='সব ইউজার';
$lang['menu_new_user']='নতুন ইউজার';
$lang['menu_settings']='সেটিংস';
$lang['menu_cash_memo']=' ক্যাশ মেমো ';
$lang['menu_product_return']=' পণ্য রিটার্ন ';
$lang['menu_product_import']=' পণ্য আমদাণী ';


$lang['menu_product']=' পণ্য ';
$lang['menu_product_category']='পণ্যের বিভাগ';
$lang['menu_all_product']='সকল পণ্য ';
$lang['menu_new_product']='নতুন পণ্য ';
$lang['menu_product_new_category']='নতুন পণ্যের বিভাগ';
$lang['menu_product_edit_category']='পণ্যের বিভাগ পরিবর্তন';
$lang['product_category_list']=' পণ্যের বিভাগের তালিকা';

$lang['menu_customer']='কাস্টমার';
$lang['menu_all_customer']='সকল কাস্টমার';
$lang['menu_new_customer']='নতুন কাস্টমার';

$lang['menu_report']='রিপোর্ট ';
$lang['menu_report_daily']='দৈনিক বিক্রয় রিপোর্ট ';
$lang['menu_report_customer']='কাস্টমার বিক্রয়  রিপোর্ট ';
$lang['menu_report_sr']='এস.আর. বিক্রয় রিপোর্ট ';
$lang['menu_report_product']='পণ্য অনুসারে বিক্রয় ';
$lang['menu_report_daily_import']='দৈনিক আমদানি রিপোর্ট';
$lang['menu_report_daily_return']='দৈনিক পণ্য রিটার্ন  ';
$lang['menu_report_service']='দৈনিক সার্ভিস রিপোর্ট';
$lang['menu_report_status']='স্ট্যাটাস ';

$lang['menu_account']=' হিসাব ';
$lang['menu_account_diposit']=' জমা হিসাব / ব্যাংক ';
$lang['menu_account_cost']=' খরচ হিসাব ';

$lang['menu_importer']=' আমদানি কারক ';
$lang['menu_all_importer']='সকল আমদানি কারক ';
$lang['menu_new_importer']='নতুন আমদানি কারক ';

$lang['menu_sr']='এস.আর.';
$lang['menu_all_sr']='সকল এস.আর.';
$lang['menu_new_sr']='নতুন এস.আর.';


//products module
$lang['product_list']=' পণ্যের তালিকা ';


// This language for customers
$lang['customer_list']=' কাস্টমার এর তালিকা ';
$lang['customer_id']=' কাস্টমার আই.ডি. ';
$lang['add_new_customer']=' নতুন কাস্টমার যুক্ত করুন ';

// This languages for importers
$lang['importer_name']=' আমদানি কারকের নাম  ';
$lang['importer_list']=' আমদানি কারকের  তালিকা ';
$lang['importer_id']=' আমদানি কারকের আই.ডি. ';
$lang['add_new_importer']=' নতুন আমদানি কারকের যুক্ত করুন ';

// this language for srs 
$lang['sr_list']='এস.আর. এর তালিকা';
$lang['add_new_sr']='নতুন  এস.আর. যুক্ত করুন';
$lang['sr_id']='এস.আর. আই.ডি.';
$lang['sr_name']='এস.আর. নাম';



//salse language 
$lang['order_memo']='অর্ডার মেমো';
$lang['new_sale']='নতুন বিক্রয়';
$lang['menu_all_sale']='সকল বিক্রয় মেমো';
$lang['memo_number']=' মেমো নং ';
$lang['serial']=' ক্রঃ নং ';
$lang['size']=' সাইজ / মডেল ';
$lang['amount']=' পরিমান ';
$lang['price']=' মূল্য ';
$lang['discount_percent']=' ডিস্কাউন্ট % ';
$lang['discount_price']=' ডিস্কাউন্ট মূল্য ';
$lang['total_price']=' মোট মূল্য ';
$lang['all_total_price']=' সর্বমোট মূল্য ';
$lang['type_product_code_or_bar_code']=' বার কোড স্ক্যান করুন অথবা প্রডাক্টের আইডি দিন';
$lang['customer_and_sr']=' কাস্টমার এবং এস.আর.';
$lang['payment_option']=' পেমেন্ট অপশন ';
$lang['payment']=' পেমেন্ট  ';
$lang['total_due']=' মোট বাকি ';
$lang['all_total_due']='সর্বমোট বাকি ';
$lang['prev_total_due']='সাবেক বাকি ';
$lang['sale_complete_btn']=' বিক্রয় সম্পাদন করুন ';
$lang['import_complete_btn']=' ক্রয় সম্পাদন করুন ';
$lang['return_complete_btn']=' রিটার্ন সম্পাদন করুন ';
$lang['total_bill']=' মোট বিল';
$lang['paymented_bill']=' বিল পরিশোধ';
$lang['due_bill']=' বাকি বিল ';
$lang['emi']='ই.এম.আই.';
$lang['warranty']=' ওয়ারেন্টি ';
$lang['close']=' বন্ধ ';

//imports language_attributes
$lang['new_import']='নতুন আমদানি ';
$lang['all_import']='সকল আমদানি';
$lang['total_import']='মোট আমদানি';
$lang['total_product']='মোট পণ্য';
$lang['total_sale']='মোট বিক্রয়';
$lang['total_sale_price']='মোট বিক্রয় মূল্য';
$lang['total_buy_price']='মোট ক্রয় মূল্য';
$lang['sale_memo']=' বিক্রয়  ম্যামো ';
$lang['return_memo']='পণ্য ফেরত  ম্যামো ';
$lang['import_memo']='আমদানি  ম্যামো ';

//reutrns language_attributes
$lang['new_return']='নতুন রিটার্ন';
$lang['all_return']='সকল রিটার্ন';
$lang['total_return']='মোট রিটার্ন';




//Users language 
$lang['profile_picture_size']=' ২১৫*২০১৫ সাইজের ইমেইজ আপলোড করুন ';
$lang['first_name']=' নামের প্রথম অংশ ';
$lang['last_name']=' নামের দ্বিতীয় অংশ ';
$lang['type']=' টাইপ ';
$lang['add_new_user']=' নতুন ইউজার যুক্ত করুন  ';
$lang['user_list']=' ইউজার তালিকা ';
$lang['picture']=' ছবি ';
$lang['country']=' দেশ ';
$lang['nid_number']=' ন্যাশনাল আই.ডি. নাম্বার ';
$lang['passport_number']=' পাসপোর্ট নাম্বার ';
$lang['birth_day']=' জন্ম তারিখ ';
$lang['present_address']=' বর্তমান ঠিকানা ';
$lang['permanent_address']=' স্থায়ী ঠিকানা ';
$lang['city']=' শহর ';
$lang['state']=' এলাকা ';


//Accounts languages
$lang['add_new_cost']='নতুন খরচের হিসাব';
$lang['add_edit_cost']='খরচের হিসাব সংশোধন';
$lang['add_new_deposit']='নতুন জমা হিসাব যুক্ত করা';
$lang['add_edit_deposit']=' জমা হিসাব সংশোধন';
$lang['purpose']='উদ্যেশ্য';
$lang['comment']=' মন্ত্যব্য ';
$lang['total_cost']=' সর্বমোট খরচ ';
$lang['total_deposit']=' সর্বমোট জমা ';
$lang['button_search']=' সার্চ করুন ';


// Report language 
$lang['all_sale_report']="সকল বিক্রয় রিপোর্ট";
$lang['to_sale_report']="তারিখ পর্যন্ত বিক্রয় রিপোর্ট";
$lang['delete']="বাতিল";



//Service language 
$lang['menu_new_service']='নতুন সার্ভিস মেনু';
$lang['menu_all_service']='সকল সার্ভিস মেনু';
$lang['type_service_name']='সার্ভিসের নাম টাইপ করুন';
$lang['type_service_price']='সার্ভিসের প্রাইস টাইপ করুন';
$lang['new_service']='নতুন সার্ভিস';
$lang['service_name']='সার্ভিসের নাম';
$lang['service_memo']='সার্ভিসে মেমো';
$lang['total_service']='মোট সার্ভিস';
$lang['service']='সার্ভিস';
$lang['total_service_price']='মোট সার্ভিস মূল্য';


?>