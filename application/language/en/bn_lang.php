<?php
//Commong languages
$lang['log_in']='LOG IN';
$lang['sign_up']='Create an Account';
$lang['register_title']='Restaurant Sign up';
$lang['have_account']='Have an account';
$lang['log_in_error']='<strong> LOG IN ERROR  </strong> User name and Password doesnot metch';
$lang['remember_me']='Remember me';
$lang['password']='Password';
$lang['confirm_password']='Confirm Password';
$lang['change_password']='Change Password';
$lang['new_passord']='New Password';
$lang['user_name']='User Name ';
$lang['profile']=' Profile ';
$lang['log_out']=' Log Out ';
$lang['home']=' Home';
$lang['currency_symbol']='&#x9f3;';
$lang['serial_number']='Serial No.';
$lang['category_name']='Category Name';
$lang['change']='Edit';
$lang['save_button']=' Save ';
$lang['upload_button']=' Upload ';
$lang['worning_delete']='Do you want to delete that. If you delete it you can not undo.';
$lang['save_success_msg']='Your information has been save';
$lang['sign_up_success']='Your sign up completed. Please login your account';
$lang['image_upload_error']='We can not  upload the image. Try to upload another image';
$lang['save_error_msg']='We can not save the information.';
$lang['delete_success_msg']='You have successfully delete this information.';
$lang['delete_error_msg']='We cannot delete this information';
$lang['permission_deny']='You have no access to do this task.';
$lang['product']=' Product ';
$lang['product_name']=' Product name';
$lang['buy_rate']='Buy price';
$lang['sale_rate']='Sale Price';
$lang['stock']='Stock';
$lang['description']='Description';
$lang['add_new_product']='Add new product';
$lang['product_id']='Product ID';
$lang['footer_copy_rigt_text']='Software Version 1.0.1 । Developed by  <a href="" target="_blank"> ASPECT IT </a>';
$lang['name']='Name';
$lang['email']=' E-Mail ';
$lang['phone']=' Phone ';
$lang['institute']=' Institute ';
$lang['address']=' Address ';
$lang['zone']=' Zone ';
$lang['customer_name']='Customer Name ';
$lang['date']=' Date ';
$lang['not_found']=' Information not found ';
$lang['print_button']='<i class="fa fa-print"></i> Print ';
$lang['authority_signature'] = ' Signature of authority ';  
$lang['customer_signature']=' Signature of the customer ';
$lang['db_backup']='Back Up';
$lang['forget_password']='Do you forget y our password ?';
$lang['reset_password']='Reset Password';

//dashbaord languages
$lang['information']=' Details Info';
$lang['total_customer']=' Total Customer';
$lang['total_importar']=' Total Importer';
$lang['total_protucts']=' Total Stock Products';
$lang['total_product_value']=' Stock products Value';
$lang['total_cost_account']='Total Cost';
$lang['total_bank_balance']='Total Bank Balance';
$lang['today_total_sale_product']='Total sale today ';
$lang['today_total_sale_product_value']='Today products slae Value';
$lang['total_sale_product']='Total Products Sale ';
$lang['total_sale_product_value']='Total Products Sale Value';
$lang['total_due_buy_product']='Total Purchase Due';
$lang['total_due_sale_product']='Total Sale Due';
$lang['person']=' Person ';
$lang['shipping_info']=' Shipping Info ';


//menu languages
$lang['menu_main_menu']='Main Menu ';
$lang['menu_dashboard']=' Dashbaord ';
$lang['menu_control_panel']=' Control Panel  ';
$lang['menu_user']='User';
$lang['menu_all_user']='All User';
$lang['menu_all_deliver_man']='All Deliver Man';
$lang['menu_new_user']='New user';
$lang['menu_settings']='Settings';
$lang['menu_cash_memo']=' Cash Memo ';
$lang['menu_product_return']=' Product Return ';
$lang['menu_product_import']=' Product Import ';

$lang['menu_product']=' Product ';
$lang['menu_product_category']='Product Category';
$lang['menu_all_product']='All Product ';
$lang['menu_new_product']='New Product';
$lang['menu_product_new_category']='New Product Category';
$lang['menu_product_edit_category']='Change product Category';
$lang['product_category_list']='Product Category List';
$lang['menu_order']='Orders';
$lang['menu_order_placed']='New Orders';
$lang['menu_order_processing']='Processing';
$lang['menu_order_pickup']='Pickup';
$lang['menu_order_delivered']='Delivered';
$lang['menu_order_taken']='Taken';
$lang['menu_restaurants']='Restaurants';
$lang['menu_restaurants_active']='Restaurants Active';
$lang['menu_restaurants_inactive']='Restaurants Inactive';


$lang['menu_report']='Report ';
$lang['menu_report_status']=' Status ';




//products module
$lang['product_list']=' Product List ';






//salse language 
$lang['order_memo']='Order Memo';
$lang['new_sale']='New Sale';
$lang['menu_all_sale']='All Sale Momo';
$lang['memo_number']=' Memo No.';
$lang['serial']='SL';
$lang['size']=' Size / Model ';
$lang['amount']=' Quantity ';
$lang['price']=' Price ';
$lang['discount_percent']=' Discount % ';
$lang['discount_price']=' Discount Price ';
$lang['total_price']=' Total Price ';
$lang['all_total_price']=' All Total Price ';
$lang['type_product_code_or_bar_code']='Type product ID or scan barcode';
$lang['customer_and_sr']='Customer AND S.R.';
$lang['payment_option']=' Payment Option ';
$lang['payment']=' Payment  ';
$lang['total_due']=' Total Due ';
$lang['all_total_due']='All Total Due';
$lang['prev_total_due']='Previous Due';
$lang['sale_complete_btn']=' Complete Sale ';
$lang['import_complete_btn']=' Complete Purchase ';
$lang['return_complete_btn']=' Complete Sale';
$lang['total_bill']=' Total Bill';
$lang['paymented_bill']=' Paying Bills';
$lang['due_bill']=' Due Bill ';
$lang['emi']='EMI';
$lang['warranty']='Warranty';
$lang['close']='Close';

//imports language_attributes
$lang['new_import']='New Import ';
$lang['all_import']='All Import';
$lang['total_import']='Total Import';
$lang['total_product']='Total Product';
$lang['total_sale']='Total Sale';
$lang['total_sale_price']='Total Sale Price';
$lang['total_buy_price']='Total Sale Price';
$lang['sale_memo']=' Sale Memo ';
$lang['return_memo']='Return Memo ';
$lang['import_memo']='Import Memo';





//Users language 
$lang['profile_picture_size']='Upload Image Size (215*215)';
$lang['first_name']=' First Name ';
$lang['last_name']=' Last name ';
$lang['type']=' Type ';
$lang['add_new_user']=' Add New User  ';
$lang['user_list']=' User List ';
$lang['picture']=' Picture ';
$lang['country']=' Country ';
$lang['nid_number']=' Nationa ID Number';
$lang['passport_number']=' Passport Number ';
$lang['birth_day']=' Birthday';
$lang['present_address']=' Present Address ';
$lang['permanent_address']=' Permanent Address ';
$lang['city']=' City ';
$lang['state']=' State ';




// Report language 
$lang['delete']="Delete";



// restaurant language
$lang['restaurant_name']='Restaurant name';
$lang['user_profile']='User Profile';
$lang['restaurant']='Restaurant';
$lang['website']='Website';
$lang['delivery_time']='Delivery Time';
$lang['opening_time']='Opening Time';
$lang['closing_time']='Closing Time';
$lang['logo']='Logo';
$lang['cover']='Cover Photo';
$lang['trade_licence_image']='Tradelicense Image';
$lang['edit_restaurant']='Edit Restaurant';
$lang['food_type']='Food Type';
$lang['product_image']='Product Image';
$lang['availability']='Availability';
$lang['opening_time']='Opening Time';
$lang['closing_time']='Closing Time';
$lang['status']='Status';

// Orders Lnaguage
$lang['order_list']='Order List';
$lang['order_id']='Order ID';
$lang['customer_info']='Customer Info';
$lang['invoice']='Invoice';
$lang['from']='From';
$lang['to']='To';
$lang['take']='Take';



?>