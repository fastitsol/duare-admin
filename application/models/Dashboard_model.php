<?php
/*
    #This model for dashboard data

*/
class Dashboard_model extends CI_Model{

    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    
    
    /*This method for all product count */
    public function customers_count(){
        $result=null;
        $result = $this->db->get('customers');
        if($result){

            // echo "<pre>";
            // print_r($result->num_rows());

            return $result->num_rows();

        }else{
            return null;
        }
        
    }
    
    /*This method for all product count */
    public function importers_count(){
        $result=null;
        $result = $this->db->get('importers');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    
    // this function for product stock sum
    public function products_stock(){
        $result=null;
        $this->db->select_sum('stock');
        $result = $this->db->get_where('products');
        $result = $result->result();
        return $result[0]->stock;
    }
    
    //This function will return total stock product price
    public function total_stock_product_price(){
        $totals = $this->db->select('SUM(stock * sale_rate) as total', FALSE)->get('products')->result();
        if($totals)
            return $totals[0]->total;
        return 0;
    }
    
    // this function for return all costs
    public function costs_sum($from=null, $to=null){
        $result=null;
        if($from and $to){
            $this->db->where("date_time BETWEEN '$from' AND '$to'");
        }
        $this->db->select_sum('amount');
        $result = $this->db->get_where('accounts', array('type'=>'cost'));
        $result = $result->result();
        return $result[0]->amount;
    }
    
    // this function for return all deposits
    public function deposits_sum($from=null, $to=null){
        $result=null;
        if($from and $to){
            $this->db->where("date_time BETWEEN '$from' AND '$to'");
        }
        $this->db->select_sum('amount');
        $result = $this->db->get_where('accounts', array('type'=>'deposit'));
        $result = $result->result();
        return $result[0]->amount;
    }
    
    // count all product quantity
    public function total_invoice_product_qty($from=null, $to=null, $type=null){
        $count =0; 
        if($from and $to){
            $this->db->where("date_time BETWEEN '$from' AND '$to'");
        }
        if($type)
            $this->db->where('type', $type);
        else
            $this->db->where('type', 'sale');
        
        $this->db->select('invoice_item.qty');
        $this->db->select_sum('qty', 'total');
        $this->db->from('invoice');
        $this->db->join('invoice_item', 'invoice_item.invoice_id = invoice.id');
        $result = $this->db->get();
        $result = $result->result();
        if($result){
            $count = $result[0]->total;
        }
        
        return $count;
    }
    
    // count all product quantity
    public function total_invoice_bill($from=null, $to=null, $type=null){
        $count =0; 
        if($from and $to){
            $this->db->where("date_time BETWEEN '$from' AND '$to'");
        }
        if($type)
            $this->db->where('type', $type);
        else
            $this->db->where('type', 'sale');
        
        $this->db->select('total_bill');
        $this->db->select_sum('total_bill', 'total');
        $result = $this->db->get('invoice');
        $result = $result->result();
        if($result){
            $count = $result[0]->total;
        }
        
        return $count;
    }
    
    // this function for sale paymented bill
    public function due_bill($from=null, $to=null, $type=null){
        if($from and $to){
            $this->db->where("date_time BETWEEN '$from' AND '$to'");
        }
        if($type)
            $this->db->where('type', $type);
        else
            $this->db->where('type', 'sale');
        
        $result=null;
        $this->db->select_sum('due_bill');
        $result = $this->db->get('invoice');
        $result = $result->result();
        return $result[0]->due_bill;
    }



    public function all_users()
    {
        $customer = "customer";
        $this->db->where(['type' => $customer]);  
        $query = $this->db->get('users');
        if($query->num_rows() > 0)  
        {  
            $re = $query->num_rows();
              // echo "<pre>";
              // print_r($query->num_rows());
            return   $re;
        }  
        else  
        {  
                return false;  
        } 
    }

    public function all_restaurant_active()
    {
        $active = "active";
        $this->db->where(['status' => $active]);  
        $query = $this->db->get('tbl_restaurant');
        if($query->num_rows() > 0)  
        {  
            $re = $query->num_rows();
              // echo "<pre>";
              // print_r($query->num_rows());
            return   $re;
        }  
        else  
        {  
                return false;  
        } 
    }

    public function all_restaurant_inactive()
    {
        $active = "inactive";
        $this->db->where(['status' => $active]);  
        $query = $this->db->get('tbl_restaurant');
        if($query->num_rows() > 0)  
        {  
            $re = $query->num_rows();
              // echo "<pre>";
              // print_r($query->num_rows());
            return   $re;
        }  
        else  
        {  
                return false;  
        } 
    }

    public function all_deliver_man()
    {
        $deliver_man = "deliver_man";
        $this->db->where(['type' => $deliver_man]);  
        $query = $this->db->get('users');
        if($query->num_rows() > 0)  
        {  
            $re = $query->num_rows();
              // echo "<pre>";
              // print_r($query->num_rows());
            return   $re;
        }  
        else  
        {  
                return false;  
        } 
    }
    
    public function all_customer($per_page=null, $page=null)
    {
        $result=null;
        $this->db->order_by("id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        $customer = "customer";
        $this->db->where(['type' => $customer]);
        $result = $this->db->get('users');

        // echo "<pre>";
        // print_r($result->result());
        return $result->result(); 
    }


    public function show_all_order_for_java_script(){

        $sql = "SELECT COUNT(invoice.date_time) as number,( SELECT DATE_FORMAT(invoice.date_time, '%Y %m %d')) as date_all FROM `invoice` JOIN invoice_items on invoice.id = invoice_items.invoice_id GROUP BY ( SELECT DATE_FORMAT(invoice.date_time, '%W %M %e %Y'))";

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            // echo "<pre>";
            // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    } 


    public function all_restaurants()
    {
        
        $sql = "SELECT users.first_name, users.last_name, users.address1, users.phone, users.id as user_id, users.email, tbl_restaurant.id as restaurant_id, tbl_restaurant.name, tbl_restaurant.email, tbl_restaurant.logo, tbl_restaurant_users.id FROM users, tbl_restaurant, tbl_restaurant_users where users.id = tbl_restaurant_users.user_id and tbl_restaurant_users.restaurant_id = tbl_restaurant.id";

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)  
        {  
            $re = $q->num_rows();
            // echo "<pre>";
            // print_r($q->num_rows());
            return $q->num_rows();
        }  
        else  
        {  
                return false;  
        } 
    }


    public function all_restaurnt_users($per_page=null, $page=null)
    {

        $result=null;
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }

        $sql = "SELECT users.first_name, users.last_name, users.address1, users.phone, users.id as user_id, users.email, tbl_restaurant.id as restaurant_id, tbl_restaurant.name, tbl_restaurant.email, tbl_restaurant.logo, tbl_restaurant_users.id FROM users, tbl_restaurant, tbl_restaurant_users where users.id = tbl_restaurant_users.user_id and tbl_restaurant_users.restaurant_id = tbl_restaurant.id";

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

             // echo "<pre>";
             // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }

    }
    
    public function total_earns()
    {

       


        $sql = " SELECT SUM(total_bill) as tatal_earns FROM `invoice` WHERE invoice.status= 'Delivered'";

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

              // echo "<pre>";
              // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    }

}
?>