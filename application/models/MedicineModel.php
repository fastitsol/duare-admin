<?php

class MedicineModel extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
        $this->Medicine_id=$this->session->userdata('restauarnt_id');
    }


    public function medicine_count($type=null, $Medicine_id=null)
    {
    	$result=null;
        if($type){
           $this->db->where('product_type', $type); 
        }
        if($Medicine_id){
           $this->db->where('restaurant_id', $Medicine_id); 
        }
        $result = $this->db->get('all_products');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
    }
    public function medicines($type=null, $per_page=null, $page=null, $Medicine_id=null){
        $result=null;
        $this->db->order_by("id", "ASC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        if($type){
           $this->db->where('product_type', $type); 
        }
        if($Medicine_id){
           $this->db->where('restaurant_id', $Medicine_id); 
        }
        $result = $this->db->get('all_products');
        return $result->result();
    }
    public function searchByName($query,$restaurant_id){

        $result=null;
        $this->db->order_by("id", "DESC");
        if($restaurant_id){
            $this->db->where('restaurant_id', $restaurant_id); 
         }
        $this->db->select('all_products.*');
        $this->db->from('all_products')->like('name',$query);
        $result = $this->db->get();
        return $result->result();
        
    }


    public function Medicine_by_id($medicine_id){
        
        $result = $this->db->get_where('all_products', array('id'=>$medicine_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }


    public function orders_count($type=null, $Medicine_id=null){
        $result=null;
        if($Medicine_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->order_by("invoice.id", "DESC");
            $this->db->select('*');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice_items.invoice_id = invoice.id AND invoice_items.restaurant_id='.$Medicine_id);
            $this->db->group_by('invoice.id');
            $result = $this->db->get();
        }else{
            if($type){
               $this->db->where('status', $type); 
            }
            $this->db->order_by("id", "DESC");
            $result = $this->db->get('invoice'); 
        }
        
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }


    public function orders($type=null, $per_page=null, $page=null, $Medicine_id=null){
        $result=null;    
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        if($Medicine_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            // $this->db->order_by("invoice.id", "DESC");
            // $this->db->select('*');
            // $this->db->from('invoice');
            // $this->db->join('invoice_items', 'invoice_items.invoice_id = invoice.id AND invoice_items.restaurant_id='.$Medicine_id);
            // $this->db->group_by('invoice.id');
            // $result = $this->db->get();

            $this->db->select('invoice.*');
            // $this->db->select('invoice_items.invoice_id','invoice_items.restaurant_id','invoice_items.product_id','invoice_items.qty','invoice_items.price','invoice_items.total_amount');
            $this->db->select('invoice_items.invoice_id');
            $this->db->select('invoice_items.restaurant_id');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice.id=invoice_items.invoice_id AND invoice_items.restaurant_id='.$Medicine_id);
            $this->db->group_by('invoice.id');
            $this->db->order_by("invoice.id", "DESC");
            $result = $this->db->get();
        }else{
            if($type){
               $this->db->where('status', $type); 
            }
            $this->db->order_by("id", "DESC");
            $result = $this->db->get('invoice'); 
        }
  
        return $result->result();
    }

    // public function orders($type=null, $per_page=null, $page=null, $restaurant_id=null){
    //     $result=null;
        
        
    //     //query buider
    //     $this->query_builder();
        
    //     if($per_page!=null){
    //         $this->db->limit($per_page, $page);
    //     }
        
    //     if($restaurant_id){
    //         if($type){
    //            $this->db->where('invoice.status', $type); 
    //         }
    //         $this->db->select('invoice.*');
    //         $this->db->select('invoice_items.invoice_id','invoice_items.restaurant_id','invoice_items.product_id','invoice_items.qty','invoice_items.price','invoice_items.total_amount');
    //         $this->db->from('invoice');
    //         $this->db->join('invoice_items', 'invoice.id=invoice_items.invoice_id AND invoice_items.restaurant_id='.$restaurant_id);
    //         $this->db->group_by('invoice.id');
    //         $this->db->order_by("invoice.id", "DESC");
    //         $result = $this->db->get();
    //     }else{
    //         $this->db->order_by("invoice.id", "DESC");
    //         if($type){
    //            $this->db->where('invoice.status', $type); 
    //         }
            
    //         // $result = $this->db->get('invoice');
            

    //         $this->db->select('invoice.*');
    //         $this->db->select('invoice_items.*');
    //         $this->db->from('invoice');
    //         $this->db->join('invoice_items', 'invoice.id = invoice_items.invoice_id','left');
    //         $this->db->group_by('invoice.id');
    //         $result = $this->db->get();
    
    
    //     }
        
        
    //     return $result->result();
        
    // }


     public function orderInfoAll($order_id=null,$restaurant_id=null){

        $sql = "SELECT users.user_name,users.image,users.address1,users.address2,users.phone,all_products.name,all_products.purchasePrice,all_products.price,all_products.size,all_products.image, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id 
            left join users on invoice.customer_id = users.id 
            left join all_products on invoice_items.product_id = all_products.id 
            where invoice_items.restaurant_id = ".$restaurant_id." AND invoice.id = ".$order_id;
        // $sql = "SELECT invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
        // invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.discount, invoice_items.total_amount
        //  FROM `invoice` 
        //  join invoice_items on invoice_items.invoice_id =invoice.id where invoice.id = ".$order_id;

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            // echo "<pre>";
            // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    } 
     public function orderInfoAllforApi($order_id=null,$restaurant_id=null){

        $sql = "SELECT users.user_name,users.image,users.address1,users.address2,users.phone,all_products.name,all_products.purchasePrice,all_products.price,all_products.size,all_products.image,all_products.medicine_company as product_company,all_products.medicine_mg_ml,all_products.food_type, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id 
            left join users on invoice.customer_id = users.id 
            left join all_products on invoice_items.product_id = all_products.id 
            where invoice_items.restaurant_id = ".$restaurant_id." AND invoice.id = ".$order_id;
        // $sql = "SELECT invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
        // invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.discount, invoice_items.total_amount
        //  FROM `invoice` 
        //  join invoice_items on invoice_items.invoice_id =invoice.id where invoice.id = ".$order_id;

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            // echo "<pre>";
            // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    } 
     public function orderInfoForShop($order_id=null,$restaurant_id=null){

        $sql = "SELECT users.user_name,users.image,users.address1,users.address2,users.phone,all_products.name,all_products.purchasePrice,all_products.price,all_products.size,all_products.image,all_products.medicine_company as product_company,all_products.medicine_mg_ml,all_products.food_type, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id 
            left join users on invoice.customer_id = users.id 
            left join all_products on invoice_items.product_id = all_products.id 
            where invoice_items.restaurant_id = ".$restaurant_id." AND invoice.id = ".$order_id;
        // $sql = "SELECT invoice.id, invoice.customer_id, invoice.orderdetails, invoice.prescriptionDetails, invoice.shiping_info, invoice.date_time, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
        // invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.discount, invoice_items.total_amount
        //  FROM `invoice` 
        //  join invoice_items on invoice_items.invoice_id =invoice.id where invoice.id = ".$order_id;

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            // echo "<pre>";
            // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    } 

    public function get_product_by_id($product_id = null)
    {

        $this->db->where('id', $product_id);      
        $result = $this->db->get('all_products');
        return $result->result();
        
    }
    public function get_product_by_idforMed($product_id = null)
    {
        $sql = "SELECT `all_products`.*,invoice_items.invoice_id,
        invoice_items.restaurant_id,invoice_items.product_id,invoice_items.qty,
        invoice_items.price,invoice_items.discount,invoice_items.total_amount,
        all_products.name FROM `invoice` 
        LEFT JOIN invoice_items ON invoice_items.invoice_id=invoice.id 
        LEFT JOIN all_products ON all_products.id=invoice_items.product_id 
        where `customer_id`= $csid 
        GROUP BY id ORDER BY id DESC" ;
        
        $query = $this->db->query($sql);
        return $query->result();

        // $this->db->where('id', $product_id);      
        // $result = $this->db->get('all_products');
        // return $result->result();
        
    }
    public function get_restaurent_by_id($res_id = null)
    {

        $this->db->where('id', $res_id);      
        $result = $this->db->get('tbl_restaurant');
        return $result->result();
        
    }


    public function getInvoiceTotal($invoice_id)
    {
        $sql = "SELECT invoice.total_bill,invoice.delivery_charge,invoice.discount from invoice where invoice.id = ".$invoice_id;

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            return $q->result();
        }
        else{
            return false;
        }
    }

    public function updateInvoiceItemPrice($invoice_item_id, $product_price_for_InvoiceItem_withqty)
    {

        $data = array(
            'total_amount' => $product_price_for_InvoiceItem_withqty
        );

        $this->db->where('id',$invoice_item_id);
        $this->db->update('invoice_items',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function updateInvoiceTotal($invoice_id, $totalInvoice)
    {

        $data = array(
            'total_bill' => $totalInvoice
        );

        $this->db->where('id',$invoice_id);
        $this->db->update('invoice',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        }
    }
   

    public function updateAllProductTable($product_id, $product_price_for_all_productTable ,$purchasePrice_for_all_productTable)
    {

        $data = array(
            'price' => $product_price_for_all_productTable,
            'purchasePrice' => $purchasePrice_for_all_productTable
        );

        $this->db->where('id',$product_id);
        $this->db->update('all_products',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        }

    }
    public function updateInvoiceItemAmount($invoice_id,$Medicine_id,$product_id,$product_price ,$price_for_TotalAmount_InvoiceItem_withqty)
    {

        $data = array(
            'price' => $product_price,
            'total_amount' => $price_for_TotalAmount_InvoiceItem_withqty
        );

        $this->db->where('invoice_id',$invoice_id);
        $this->db->where('restaurant_id',$Medicine_id);
        $this->db->where('product_id',$product_id);
        $this->db->update('invoice_items',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        }

    }


    public function updatePenddingToProcessing($invoice_id, $data)
    {

       $this->db->where('id',$invoice_id);
        $this->db->update('invoice',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        } 
    }




    public function discountInAll($id)
    {

        $sql = "SELECT * from discount_allproduct where restaurant_id = ".$id;

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            return $q->result();
        }
        else{
            return false;
        }
    }

    public function AddDiscountModel($data, $add=null, $id )
    {


        if ($add == "add") {
             $this->db->insert('discount_allproduct', $data);

            if($this->db->affected_rows() > 0){
                return 1;
            }else{
                return 0;
            }
        }else{

            $this->db->where('restaurant_id', $id );
            $this->db->update('discount_allproduct',$data);

            if($this->db->affected_rows() > 0){
                return 1;
            }else{
                return 0;
            }
        }

    }


    public function updateInAllProduct($discountValue, $id)
    {

        $sql = "UPDATE `all_products` SET discount_percent = ".$discountValue." WHERE restaurant_id = ".$id;

        $q = $this->db->query($sql);
        if($this->db->affected_rows() > 0)
        {

            return 1;
        }
        else{
            return 0;
        }

    }
    public function updateResAllProduct($discount_percent, $discounted_price , $id)
    {

        $sql = "UPDATE `all_products` SET discount_percent = ".$discount_percent." AND discount_price = ".$discounted_price." WHERE restaurant_id = ".$id;

        $q = $this->db->query($sql);
        if($this->db->affected_rows() > 0)
        {

            return 1;
        }
        else{
            return 0;
        }

    }




    public function thisYearReport($id)
    {

        $date = date("Y-12-30 00:00:00");
          // echo "<pre>";
          // print_r($date);
          // exit;

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = 1 and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 

        
    }

    public function pharmacyYearReport($id)
    {

        $date = date("Y-12-30 00:00:00");
          // echo "<pre>";
          // print_r($date);
          // exit;

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 

        
    }

    public function thisMonthReport($id)
    {

        $lastdate = date("Y-m-30 00:00:00");

        $startdate = date("Y-m-01 00:00:00");

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time < '".$lastdate."' AND invoice.date_time > '".$startdate."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        else  
        {  
                return 0;  
        } 
    }
}
    