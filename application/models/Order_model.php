<?php
/*
    #This class will manage all product
    #This class will manage product category
*/
class Order_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
        $this->restauarnt_id=$this->session->userdata('restauarnt_id');
    }
    
    public function query_builder(){
        
    }
   
    // this method for get all products
    //and return product by product category
    // if send product cat_id and paged it will return by page
    public function orderss($type=null, $per_page=null, $page=null, $restaurant_id=null){
        $result=null;
        
        
        //query buider
        $this->query_builder();
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        if($restaurant_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->select('invoice.*');
            $this->db->select('invoice_items.invoice_id');
            $this->db->select('invoice_items.restaurant_id');
            // $this->db->select('invoice_items.restaurant_id');'invoice_items.restaurant_id','invoice_items.product_id','invoice_items.qty','invoice_items.price','invoice_items.total_amount');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice.id=invoice_items.invoice_id AND invoice_items.restaurant_id='.$restaurant_id);
            $this->db->group_by('invoice.id');
            $this->db->order_by("invoice.id", "DESC");
            $result = $this->db->get();
        }
        
        
        return $result->result();
        
    }

    public function orders($type=null, $per_page=null, $page=null, $restaurant_id=null){
        $result=null;
        
        
        //query buider
        $this->query_builder();
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        if($restaurant_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->select('invoice.*');
            $this->db->select('invoice_items.invoice_id');
            $this->db->select('invoice_items.restaurant_id');
            // $this->db->select('invoice_items.restaurant_id');'invoice_items.restaurant_id','invoice_items.product_id','invoice_items.qty','invoice_items.price','invoice_items.total_amount');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice.id=invoice_items.invoice_id AND invoice_items.restaurant_id='.$restaurant_id,'left');
            $this->db->group_by('invoice.id');
            $this->db->order_by("invoice.id", "DESC");
            $result = $this->db->get();
        }else{
            $this->db->order_by("invoice.id", "DESC");
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            
            // $result = $this->db->get('invoice');
            

            $this->db->select('invoice.*');
            $this->db->select('invoice_items.*');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice.id = invoice_items.invoice_id','left');
            $this->db->group_by('invoice.id');
            $result = $this->db->get();
    
    
        }
        
        
        return $result->result();
        
    }
   
    public function orderInfoAll($order_id=null){

        // if($restaurant_id){
        //     $sql = "SELECT all_products.name,all_products.price,all_products.size, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
        //     invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
        //     FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id 
        //     left join all_products on invoice_items.product_id = all_products.id 
        //     where invoice_items.restaurant_id = ".$restaurant_id." AND invoice.id = ".$order_id;

        //     $q = $this->db->query($sql);
        //     if($q->num_rows() > 0)
        //     {
        //         // echo "<pre>";
        //         // print_r($q->result());
        //         return $q->result();
        //     }
        //     else{
        //         return false;
        //     }

        // }else{
            $sql = "SELECT `tbl_restaurant`.`name` as `restaurant_name`,`tbl_restaurant`.`phone`,`tbl_restaurant`.`address`,all_products.name,all_products.price,all_products.size,all_products.image,all_products.medicine_company as company_name, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id
            left join all_products on invoice_items.product_id = all_products.id 
            left join tbl_restaurant on invoice_items.restaurant_id = tbl_restaurant.id
            where invoice.id = $order_id";

            $q = $this->db->query($sql);
            if($q->num_rows() > 0)
            {
                // echo "<pre>";
                // print_r($q->result());
                return $q->result();
            }
            else{
                return false;
            }
        
    }
    //this method for get a single product by product id
    public function order_by_id($order_id){
        // $sql = "SELECT `invoice`.*,invoice_items.invoice_id,
        // invoice_items.restaurant_id,invoice_items.product_id,invoice_items.qty,
        // invoice_items.price,invoice_items.total_amount
        // FROM `invoice` 
        // LEFT JOIN invoice_items ON invoice_items.invoice_id=invoice.id 
        // where `invoice`.id= $order_id " ;
        
        // $query = $this->db->query($sql);
        // return $query->result();
       
        $result = $this->db->get_where('invoice', array('id'=>$order_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    
    
    /*This method for all product count */
    public function orders_count($type=null, $restaurant_id=null){
        $result=null;
       
        //query buider
        $this->query_builder();
        if($restaurant_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->order_by("invoice.id", "DESC");
            $this->db->select('*');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice_items.invoice_id = invoice.id AND invoice_items.restaurant_id='.$restaurant_id);
            $this->db->group_by('invoice.id');
            $result = $this->db->get();
        }else{
            if($type){
               $this->db->where('status', $type); 
            }
            $this->db->order_by("id", "DESC");
            $result = $this->db->get('invoice'); 
        }
        
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    
    // this function for return invoice items
    public function invoice_items($order_id){
        // echo "<pre>";
        // print_r($order_id);
        // return;
        // if($restaurant_id){
        //     $this->db->where('restaurant_id', $restaurant_id);
        // }
        $result =  $this->db->get_where('invoice_items', array('invoice_id', $order_id));
        // return $result->result();
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    
    // this function for return invoice items toatl price
    public function invoice_items_price($order_id, $restaurant_id=null){
        if($restaurant_id){
            $this->db->where('restaurant_id', $restaurant_id);
        }
        $this->db->select_sum('total_amount');
        $result =  $this->db->get_where('invoice_items', array('invoice_id', $order_id));
        $result =  $result->result();
        if($result[0]->total_amount){
            return $result[0]->total_amount;
        }else{
            return 0;
        }
    }

    public function ordersStatusByTake_by_id($status, $take_by_id=null)
    {

        if($take_by_id && $status){
            $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
            FROM `invoice` 
            LEFT JOIN users on users.id = invoice.`customer_id` 
            LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
            WHERE `invoice`.`take_by` = '$take_by_id' AND `invoice`.`status` = '$status' GROUP BY `invoice_id`";
           }else{
            $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
            FROM `invoice` 
            LEFT JOIN users on users.id = invoice.`customer_id` 
            LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
            WHERE `invoice`.`status` = '$status' GROUP BY `invoice_id`";
           }
        
       
    //    SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount FROM `invoice` LEFT JOIN users on users.id = invoice.`customer_id` LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id WHERE `invoice_items`.`restaurant_id` = 1 AND `invoice`.`status` = 'Placed' GROUP BY `invoice_id`
        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        // else  
        // {  
        //         return 0;  
        // } 
    }

    public function ordersByStatusForDeliveryMan($status, $take_by_id=null)
    {

       if($take_by_id && $status){
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name as take_by_delivery_boy,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`take_by` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice`.`take_by` = '$take_by_id' AND `invoice`.`status` = '$status' GROUP BY `invoice_id`";
       }else{
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`customer_id` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice`.`status` = '$status' GROUP BY `invoice_id`";
       }
    //    SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount FROM `invoice` LEFT JOIN users on users.id = invoice.`customer_id` LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id WHERE `invoice_items`.`restaurant_id` = 1 AND `invoice`.`status` = 'Placed' GROUP BY `invoice_id`
        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        // else  
        // {  
        //         return 0;  
        // } 
    }
    public function ordersByStatus($status, $shop_id=null)
    {

        
// echo "<pre>";
            // print_r($q->result());
            // exit;
       if($shop_id){
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name as take_by_delivery_boy,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`take_by` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice_items`.`restaurant_id` = '$shop_id' AND `invoice`.`status` = '$status' GROUP BY `invoice_id` ";
       }else{
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`customer_id` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice`.`status` = '$status' GROUP BY `invoice_id`";
       }
    //    SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone,SUM(total_amount) as TotalAmount FROM `invoice` LEFT JOIN users on users.id = invoice.`customer_id` LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id WHERE `invoice_items`.`restaurant_id` = 1 AND `invoice`.`status` = 'Placed' GROUP BY `invoice_id`
        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        // else  
        // {  
        //         return 0;  
        // } 
    }
    public function ordersFromInvoiceItems($invoice_id, $shop_id=null)
    {

        
// echo "<pre>";
            // print_r($q->result());
            // exit;
       if($shop_id){
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`customer_id` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice_items`.`restaurant_id` = '$shop_id' AND `invoice`.`status` = '$status'";
       }else{
        $sql = "SELECT `invoice`.*,`invoice_items`.*,users.user_name,users.image,users.address1,users.address2,users.phone
        FROM `invoice` 
        LEFT JOIN users on users.id = invoice.`customer_id` 
        LEFT JOIN invoice_items on invoice_items.invoice_id = invoice.id 
        WHERE `invoice`.`status` = '$status'";
       }

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        // else  
        // {  
        //         return 0;  
        // } 
    }
    
    
    

    
}
?>