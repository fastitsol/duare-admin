<?php
/*
    #This model for give a user permission

*/
class Permission extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    //this method for check user can do this job
    public function can(){
        return true;
    }
    
}
?>