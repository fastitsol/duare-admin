<?php
/*
    #This class will manage all product
    #This class will manage product category
*/
class Product_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
        $this->restauarnt_id=$this->session->userdata('restauarnt_id');
    }
   
    // this method for get all products
    //and return product by product category
    // if send product cat_id and paged it will return by page
    // function myjoin(){
    //     $sql="SELECT `movies`.*,directors.directors_name,language.language_title FROM `movies` LEFT JOIN directors ON directors.directors_id=movies.movies_director_id LEFT JOIN language ON language.language_id=movies.movies_language_id";
    //     $query = $this->db->query($sql);
    //     return $query->result();
    // }
    public function productsForAdmin($per_page=null, $page=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        $this->db->select('all_products.*');
        $this->db->select('sub_category.subCategory_name');
        $this->db->from('all_products');
        $this->db->join('sub_category', 'all_products.subCategory_id = sub_category.id', 'left');
        $result = $this->db->get();
        return $result->result();
        
    }

    public function get_all_food_product($per_page=null, $page=null){

        $result=null;
        $this->db->order_by("id", "ASC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        $result = $this->db->get('all_products');
        return $result->result();
    }
    public function products($type=null, $per_page=null, $page=null, $restaurant_id=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        if($type){
           $this->db->where('product_type', $type); 
        }
        if($restaurant_id){
           $this->db->where('restaurant_id', $restaurant_id); 
        }

        $this->db->select('all_products.*');
        $this->db->select('sub_category.subCategory_name');
        $this->db->from('all_products');
        $this->db->join('sub_category', 'all_products.subCategory_id = sub_category.id','left');
        $result = $this->db->get();

        // $result = $this->db->get('all_products');
        return $result->result();
        
    }
    
    public function products_by_category($cat, $type=null){
        $result=null;
        $this->db->order_by("all_products.id", "DESC");
        if($type){
            $this->db->where('all_products.product_type', $type);
        }
        
        $this->db->where('tbl_products_category.category_id', $cat);
        $this->db->select('all_products.*');
        $this->db->from('all_products');
        $this->db->join('tbl_products_category', 'all_products.id = tbl_products_category.product_id');
        $result = $this->db->get();
        return $result->result();
        
    }
    
    public function products_by_category_count($cat, $type=null){
        $result=null;
        $this->db->order_by("all_products.id", "DESC");
        if($type){
            $this->db->where('all_products.product_type', $type);
        }
        
        $this->db->where('tbl_products_category.category_id', $cat);
        $this->db->select('all_products.*');
        $this->db->from('all_products');
        $this->db->join('tbl_products_category', 'all_products.id = tbl_products_category.product_id');
        $result = $this->db->get();
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
   
    
    //this method for get a single product by product id
    public function product_by_id($product_id, $restaurant_id=null){
        if($restaurant_id){
           $this->db->where('restaurant_id', $restaurant_id); 
        }
        $result = $this->db->get_where('all_products', array('id'=>$product_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    
    //this function for get product name
    public function product_name($product_id){
        $product = $this->product_by_id($product_id);
        $product_name = 'May be deleted';
        if($product){
            $product_name = $product->name;
        }
        return $product_name;
    }
    
    
    /*This method for all product count */
    public function products_count($type=null, $restaurant_id=null){
        $result=null;
        if($type){
           $this->db->where('product_type', $type); 
        }
        if($restaurant_id){
           $this->db->where('restaurant_id', $restaurant_id); 
        }
        $result = $this->db->get('all_products');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    //this method for return all product category
    public function category($restaurant_id=null){
        if($restaurant_id){
           $this->db->where('restaurant_id', $restaurant_id); 
        }
        $result = $this->db->get('tbl_category');
        $this->db->order_by('id', 'DESC');
        if($result){
            return $result->result();
        }else{
            return null;
        }
    }

    public function total_category(){
        // if($restaurant_id){
        //    $this->db->where('restaurant_id', $restaurant_id); 
        // }
        $result = $this->db->get('category');
        $this->db->order_by('id', 'ASC');
        if($result){
            return $result->result();
        }else{
            return null;
        }
    }
    public function subCategory(){
        
        $sql="SELECT `sub_category`.*,category.category_name FROM `sub_category` LEFT JOIN category ON category.id=sub_category.category_id ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function order_details($csid){
        
        // $sql="SELECT `sub_category`.*,category.category_name FROM `sub_category` LEFT JOIN category ON category.id=sub_category.category_id ";
        
        $sql = "SELECT `invoice`.id,invoice.date_time,invoice.total_bill,
        invoice.status,invoice.shiping_info,invoice_items.invoice_id,
        invoice_items.restaurant_id,invoice_items.product_id,invoice_items.qty,
        invoice_items.price,invoice_items.discount,invoice_items.total_amount,
        all_products.name FROM `invoice` 
        LEFT JOIN invoice_items ON invoice_items.invoice_id=invoice.id 
        LEFT JOIN all_products ON all_products.id=invoice_items.product_id 
        where `customer_id`= $csid 
        GROUP BY id ORDER BY id DESC" ;
        
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function subCategoryList(){
        
        $sql="SELECT * FROM `sub_category` ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function shopList(){
        
        $sql="SELECT `id`,`name` FROM `tbl_restaurant` ";
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    public function search($query){

        $result=null;
        $this->db->order_by("id", "DESC");
        // if($per_page!=null){
        //     $this->db->limit($per_page, $page);
        // }
        $this->db->select('all_products.*');
        $this->db->select('sub_category.subCategory_name');
        $this->db->from('all_products')->like('name',$query);
        $this->db->join('sub_category', 'all_products.subCategory_id = sub_category.id','left');
        $result = $this->db->get();
        return $result->result();

        // $q = $this->db->from('all_products')->like('name',$query)->get();
        
        // $this->db->select('all_products.*');
        // $this->db->select('sub_category.subCategory_name');
        // $this->db->from('all_products');
        // $this->db->join('sub_category', 'all_products.subCategory_id = sub_category.id');
        // // $sql="SELECT `id`,`name` FROM `tbl_restaurant` ";
        // // $query = $this->db->query($sql);
        // return $q->result();
        
    }
    public function searchByRestaurant($query,$restaurant_id){

        $result=null;
        $this->db->order_by("id", "DESC");
        if($restaurant_id){
            $this->db->where('restaurant_id', $restaurant_id); 
         }
        $this->db->select('all_products.*');
        $this->db->select('sub_category.subCategory_name');
        $this->db->from('all_products')->like('name',$query);
        $this->db->join('sub_category', 'all_products.subCategory_id = sub_category.id');
        $result = $this->db->get();
        return $result->result();
        
    }
    // function myjoin(){
    //     $sql="SELECT `movies`.*,directors.directors_name,language.language_title FROM `movies` LEFT JOIN directors ON directors.directors_id=movies.movies_director_id LEFT JOIN language ON language.language_id=movies.movies_language_id";
    //     $query = $this->db->query($sql);
    //     return $query->result();
    // }


    //this method for get single category 
    public function category_by_id($cat_id){
        $result = $this->db->get_where('tbl_category', array('id'=>$cat_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    public function subcategory_by_id($subcat_id){
        $result = $this->db->get_where('sub_category', array('id'=>$subcat_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    public function edit_category_by_id($cat_id){
        $result = $this->db->get_where('category', array('id'=>$cat_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }
    
    
    //This method for return category name
    public function category_name($cat_id){
        $category = $this->category_by_id($cat_id);
        if($category){
            return $category->name;
        }else{
            return null;
        }
    }
    
    
    // this function for products cateories
    public function product_categories($product_id){
        $results = $this->db->get_where('tbl_products_category', array('product_id'=>$product_id));
        return $results->result();
    }
    
    
    
    // public function count_products($category_id,$restaurant_id=null){
    //     // echo "<pre>";
    //     // print_r($category_id);

    //     $result=null;
    //     $restaurant_id = $this->restauarnt_id;
    //     // echo "<pre>";
    //     // print_r($restaurant_id);
    //     if($category_id){
    //        $this->db->where('food_type', $category_id); 
    //     }
    //     if($restaurant_id){
    //        $this->db->where('restaurant_id', $restaurant_id); 
    //     }
    //     $result = $this->db->get('all_products');
    //     if($result){
    //          return $result->num_rows();
    //     }else{
    //         return null;
    //     }
        
    // }
    public function count_products($restaurant_id){
        // echo "<pre>";
        // print_r($category_id);

        $result=null;
        
        if($restaurant_id){
           $this->db->where('restaurant_id', $restaurant_id); 
        }
        $result = $this->db->get('all_products');
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    public function price_in_discount($price,$discount_percent){
        // echo "<pre>";
        // print_r($price);
        // print_r($discount_percent);

        $result=null;
        $cal = ($price * $discount_percent)/100;
        $cal2 = ($price - $cal);
        return $cal2;
        // echo "<pre>";
        // print_r($cal2);
        // print_r($discount_percent);

        // $restaurant_id = $this->restauarnt_id;
        // echo "<pre>";
        // print_r($restaurant_id);
        // if($category_id){
        //    $this->db->where('food_type', $category_id); 
        // }



        // if($restaurant_id){
        //    $this->db->where('restaurant_id', $restaurant_id); 
        // }
        // $result = $this->db->get('all_products');
        // if($result){
        //      return $result->num_rows();
        // }else{
        //     return null;
        // }
        
    }
    public function getResProPrice($restaurant_id)
    {

       
        // $sql = "restaurant_id = '".$restaurant_id."' and invoice_items.invoice_id = '".$invoice_id."'";
        $sql = "SELECT `price` FROM `all_products` WHERE `restaurant_id`='".$restaurant_id."'";
        // $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }
    
}
?>