<?php
/*
    #This model for customers data

*/
class Restaurant_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    //this method for return all customers
    public function restaurants($per_page=null, $page=null, $status=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        if($status)
            $this->db->where('status', $status);
        $result = $this->db->get('tbl_restaurant');
        return $result->result();
        
    }
   
    
    //this method for ger a single customer
    public function restaurant_by_id($restaurant_id){
        $result = $this->db->get_where('tbl_restaurant', array('id'=>$restaurant_id));
        if($result){
            return $result->row(0);
        }else{
            return null;
        }
    }

    
    /*This method for all product count */
    public function restaurants_count($status=null){
        $result=null;
        $result = $this->db->get('tbl_restaurant');
        
        if($status)
            $this->db->where('status', $status);
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    
    public function get_restaurnt_name($id=null){
        $restaurant = $this->restaurant_by_id($id);
        $name = 'No Name';
        if($restaurant){
            $name=$restaurant->name;
        }
        return $name;
    } 
    
    
    // this function for get user id of the single restaurant
    public function get_user_id($restaurant_id){
        $result = $this->db->get_where('tbl_restaurant_users', array( 'restaurant_id'=>$restaurant_id));
        $id=null;
        if($result){
            $res = $result->row(0);
            $id = $res->user_id;
        }
        return $id;
    }




     
    public function restaurants_counts_for_discount($status=null){
        $result=null;
        $result = $this->db->get('tbl_restaurant');
        
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }

   
    public function restaurants_for_discount($per_page=null, $page=null){
        $result=null;
        $this->db->order_by("id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        $result = $this->db->get('tbl_restaurant');
        return $result->result();
        
    }




    public function restaurants_product_counts_for_discount($id=null){
        $result=null;
        $sql = "SELECT * FROM all_products WHERE restaurant_id = ".$id;
        
        $q = $this->db->query($sql);
        if($q){
             return $q->num_rows();
        }else{
            return null;
        }
        
    }
    

    public function restaurants_product_for_discount($id, $per_page=null, $page=null){

        $result=null;
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        $this->db->where('restaurant_id', $id);
        
        $result = $this->db->get('all_products');
        

        // echo "<pre>";
        // print_r($result->result());

        return $result->result();
          
    }



    public function get_products($id)
    {
        $result = null;
        $this->db->where('id', $id);
        $result = $this->db->get('all_products');

        return $result->result();
    }
    

    public function discount_Update_model( $product_id,$price_after_discount , $discount_number){

        $discount_percent = $discount_number." % discount";

        if($discount_number==0){

            $price_after_discount = 0;
        }
        $data = array(
            'discount_price' => $price_after_discount ,
            'discount_percent' => $discount_percent
        );

        $this->db->where('id',$product_id);
        $this->db->update('all_products',$data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    // /Discount by rastaurant done
    ///////////////////////////////////////////
    ////////////////////////////////////////
    //???????????????????????????????????????//////////////////




    public function all_food_product(){

        $result=null;
        $result = $this->db->get('all_products');
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
    }

    public function get_all_food_product($per_page=null, $page=null){

        $result=null;
        $this->db->order_by("id", "ASC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        $result = $this->db->get('all_products');
        return $result->result();
    }




    public function orders_count($type=null, $restaurant_id=null){
        $result=null;
       
        //query buider
       //$this->query_builder();
        if($restaurant_id){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->order_by("invoice.id", "DESC");
            $this->db->select('*');
            $this->db->from('invoice');
            $this->db->join('invoice_items', 'invoice_items.invoice_id = invoice.id AND invoice_items.restaurant_id='.$restaurant_id);
            $this->db->group_by('invoice.id');
            $result = $this->db->get();
        }else{
            if($type){
               $this->db->where('status', $type); 
            }
            $this->db->order_by("id", "DESC");
            $result = $this->db->get('invoice'); 
        }
        
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    public function deliveryBoy_orders_count($type=null, $deliveryBoyId=null){
        $result=null;
       
        //query buider
       //$this->query_builder();
        if($deliveryBoyId){
            if($type){
               $this->db->where('invoice.status', $type); 
            }
            $this->db->order_by("invoice.id", "DESC");
            $this->db->select('*');
            $this->db->from('invoice');
            $this->db->where('take_by', $deliveryBoyId);
            // $this->db->join('invoice_items', 'invoice_items.invoice_id = invoice.id AND invoice_items.restaurant_id='.$restaurant_id);
            $this->db->group_by('invoice.id');
            $result = $this->db->get();
        }else{
            if($type){
               $this->db->where('status', $type); 
            }
            $this->db->order_by("id", "DESC");
            $result = $this->db->get('invoice'); 
        }
        
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }


     public function show_all_order_for_java_script($id=null){

        $sql = "SELECT COUNT(invoice.date_time) as number,( SELECT DATE_FORMAT(invoice.date_time, '%Y %m %d')) as date_all FROM `invoice` JOIN invoice_items on invoice.id = invoice_items.invoice_id WHERE invoice_items.restaurant_id = ".$id." GROUP BY ( SELECT DATE_FORMAT(invoice.date_time, '%W %M %e %Y'))";

        $q = $this->db->query($sql);
        if($q->num_rows() > 0)
        {

            // echo "<pre>";
            // print_r($q->result());
            return $q->result();
        }
        else{
            return false;
        }
    } 



    ///coupon site

    public function all_restaurants_for_coupon()
    {

        $this->db->order_by("id", "ASC");
        $result = $this->db->get('tbl_restaurant');
        return $result->result();
    }

    public function new_coupon_add($restaurant_id,$coupon , $coupon_value ,$coupon_percentage, $coupon_start_date, $coupon_end_date)
    {
        $data = array(
            'restaurant_id' => $restaurant_id ,
            'coupon' => $coupon,
            'coupon_value_percentage' => $coupon_value ,
            'coupon_percentage' => $coupon_percentage ,
            'coupon_start_date' => $coupon_start_date ,
            'coupon_end_date' => $coupon_end_date 
        );

       
        $this->db->insert('tbl_coupon', $data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }


    // coupon all

    public function coupon_count(){
        $result=null;
        $result = $this->db->get('tbl_coupon');
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
        
    }
    

    public function all_coupon($per_page=null, $page=null)
    {
        $result=null;
        $this->db->order_by("coupon_id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        $result = $this->db->get('tbl_coupon');
        return $result->result();
    }

    public function coupons_delete($coupon_id)
    {
        $this -> db -> where('coupon_id', $coupon_id);
        $this -> db -> delete('tbl_coupon');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }



    // Delivery_place and area part

    public function addDelivery_place_name($area_id , $Delivery_place_name,$Delivery_charge , $active_date)
    {
        $data = array(
            'area_id' => $area_id,
            'delivery_place_name' => $Delivery_place_name ,
            'delivery_charge' => $Delivery_charge,
            'active_date' => $active_date 
        );

       
        $this->db->insert('tbl_delivery_place', $data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function allPlacesDelivery()
    {
        $result=null;
        $result = $this->db->get('tbl_delivery_place');
        
        if($result){
             return $result->num_rows();
        }else{
            return null;
        }
    }
    

    public function Delivery_allPlaces($per_page=null, $page=null)
    {
        $result=null;
        $this->db->order_by("delivery_id", "DESC");
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }
        
        $result = $this->db->get('tbl_delivery_place');
        return $result->result();
    }

    public function delivery_place_delete($delivery_id)
    {
        $this -> db -> where('delivery_id', $delivery_id);
        $this -> db -> delete('tbl_delivery_place');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    


    public function total_available_products($id)
    {
        $result=null;
        $sql = "SELECT * FROM all_products WHERE availability = 'available' AND restaurant_id = ".$id;
        
        $q = $this->db->query($sql);
        if($q){
             return $q->num_rows();
        }else{
            return null;
        }
    }


    // ----------------------------


    public function area_delivery_all()
    {
        $result = $this->db->get('tbl_delivery_area');
        return $result->result();
    }

    public function add_delivery_area_model($area_name, $area_Charge)
    {

        $data = array(
            'area_name' => $area_name,
            'area_charge' => $area_Charge
        );

        $this->db->insert('tbl_delivery_area', $data);

        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function delete_delivery_area_model($delivery_area_id)
    {
        $this -> db -> where('area_id', $delivery_area_id);
        $this -> db -> delete('tbl_delivery_area');
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

     public function get_area_name($id=null){
        $result = $this->db->get_where('tbl_delivery_area', array('area_id'=>$id));
        
        return $result->row(0);
    }



    // get area and place for app

    public function delivery_areas()
    {
        $result = $this->db->get('tbl_delivery_area');
        return $result->result();
    }

    public function delivery_areas_place($area_id=NULL)
    {
         $this -> db -> where('area_id', $area_id);
        $result = $this->db->get('tbl_delivery_place');
        return $result->result();
    }
    
    
    public function discount_all_products($id){
        $result=null;
        $sql = "SELECT * FROM all_products WHERE discount_price != '0' and restaurant_id = ".$id;
        
        $q = $this->db->query($sql);

         if($q){
              return $q->num_rows();
        }else{
             return null;
         }
        $result=null;  
    }

    public function Discount_productsForShowing($id, $per_page=null, $page=null)
    {
        $result=null;
        
        if($per_page!=null){
            $this->db->limit($per_page, $page);
        }

        $this->db->where('discount_percent',' != ', null);

        $this->db->where('restaurant_id', $id);
        
        $result = $this->db->get('all_products');
        
         // echo "<pre>";
         // print_r($result->result());

        return $result->result();
    }




    public function last_yearIncome($id)
    {
        $date = date("Y-12-30 00:00:00");
          // echo "<pre>";
          // print_r($date);
          // exit;

        $sql = "SELECT SUM(invoice_items.total_amount) as total FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."'";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        else  
        {  
                return 0;  
        } 
    }



    public function thisMonthIncome($id)
    {



        $lastdate = date("Y-m-30 00:00:00");

        $startdate = date("Y-m-01 00:00:00");

        $sql = "SELECT SUM(invoice_items.total_amount) as monthTotal FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time < '".$lastdate."' AND invoice.date_time > '".$startdate."'";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        else  
        {  
                return 0;  
        } 
    }
    
  //new code

    public function orderInfoAll($order_id=null, $restaurant_id=null){

        if($restaurant_id){
            $sql = "SELECT all_products.name,all_products.price,all_products.size, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id 
            left join all_products on invoice_items.product_id = all_products.id 
            where invoice_items.restaurant_id = ".$restaurant_id." AND invoice.id = ".$order_id;

            $q = $this->db->query($sql);
            if($q->num_rows() > 0)
            {
                // echo "<pre>";
                // print_r($q->result());
                return $q->result();
            }
            else{
                return false;
            }

        }else{
            $sql = "SELECT all_products.name,all_products.price,all_products.size, invoice.id, invoice.customer_id, invoice.orderdetails, invoice.shiping_info, invoice.date_time, invoice.discount, invoice.total_bill, invoice.delivery_charge,invoice.status, invoice.payment_type,
            invoice_items.id as invoice_item_id, invoice_items.invoice_id, invoice_items.restaurant_id, invoice_items.product_id, invoice_items.qty, invoice_items.price, invoice_items.total_amount 
            FROM `invoice` join invoice_items on invoice_items.invoice_id =invoice.id
            left join all_products on invoice_items.product_id = all_products.id 
            where invoice.id = ".$order_id;

            $q = $this->db->query($sql);
            if($q->num_rows() > 0)
            {
                // echo "<pre>";
                // print_r($q->result());
                return $q->result();
            }
            else{
                return false;
            }
        }
    }
    
     // new today

    public function rest_updatePenddingToProcessing($invoice_id, $data)
    {

       $this->db->where('id',$invoice_id);
        $this->db->update('invoice',$data);

        if($this->db->affected_rows() > 0){
            return 1;
        }else{
            return 0;
        } 
    }
    
    
     // new today
    //  public function shopthisYearReport($id)
    //  {
 
    //      $date = date("Y-12-30 00:00:00");
    //      //   echo "<pre>";
    //      //   print_r($date);
    //      //   exit;
 
    //      $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";
 
    //      $q = $this->db->query($sql);
 
    //      if($q->num_rows() > 0)  
    //      {  
    //          return $q->result();
 
    //          // echo "<pre>";
    //          // print_r($q->result());
    //          // exit;
            
    //      }  
    //      else  
    //      {  
    //              return 0;  
    //      } 
    //  }
        public function getResProductAmount($invoice_id,$restaurant_id)
    {

       
        $sql = "SELECT SUM(total_amount) as total_amount
        FROM invoice_items where invoice_items.restaurant_id = '".$restaurant_id."' and invoice_items.invoice_id = '".$invoice_id."'";
        // $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }
    
    

    public function getProductAmount($invoice_id)
    {

       
        $sql = "SELECT SUM(total_amount) as total_amount
        FROM invoice_items where invoice_items.invoice_id = '".$invoice_id."'";
        // $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }
    public function getProductAmountForProfit($invoice_id)
    {

       
        $sql = "SELECT SUM(`all_products`.`purchasePrice`) as proAmount,`invoice_items`.* FROM invoice_items LEFT JOIN `all_products` on all_products.id=`invoice_items`.`product_id` where invoice_items.invoice_id = '".$invoice_id."'";
        // $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }

        public function resthisYearReport($id)
    {

        $date = date("Y-12-30 00:00:00");
        //   echo "<pre>";
        //   print_r($date);
        //   exit;

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }
        public function showthisYearReportToAdmin($id)
    {

        if($id){
            $date = date("Y-12-30 00:00:00");
        //   echo "<pre>";
        //   print_r($date);
        //   exit;

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);
        }else{
            $date = date("Y-12-30 00:00:00");
        //   echo "<pre>";
        //   print_r($date);
        //   exit;

        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice.status = 'Delivered' and invoice.date_time <= '".$date."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);
        }

        if($q->num_rows() > 0)  
        {  
            return $q->result();

            // echo "<pre>";
            // print_r($q->result());
            // exit;
           
        }  
        else  
        {  
                return 0;  
        } 
    }



    public function resthisMonthReport($id)
    {

        $lastdate = date("Y-m-31 00:00:00");
        

        $startdate = date("Y-m-01 00:00:00");
// echo "<pre>";
            // print_r($q->result());
            // exit;
        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time < '".$lastdate."' AND invoice.date_time > '".$startdate."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        else  
        {  
                return 0;  
        } 
    }
    public function showthisMonthReporttoAdmin($id)
    {

       if($id){
        $lastdate = date("Y-m-31 00:00:00");
        

        $startdate = date("Y-m-01 00:00:00");
// echo "<pre>";
            // print_r($q->result());
            // exit;
        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice_items.restaurant_id = '".$id."' and invoice.status = 'Delivered' and invoice.date_time < '".$lastdate."' AND invoice.date_time > '".$startdate."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);
       }else{
        $lastdate = date("Y-m-30 00:00:00");
        

        $startdate = date("Y-m-01 00:00:00");
// echo "<pre>";
            // print_r($q->result());
            // exit;
        $sql = "SELECT * FROM `invoice_items` join invoice on invoice_items.invoice_id = invoice.id where invoice.status = 'Delivered' and invoice.date_time < '".$lastdate."' AND invoice.date_time > '".$startdate."' GROUP by invoice_items.invoice_id";

        $q = $this->db->query($sql);
       }

        if($q->num_rows() > 0)  
        {  
            return $q->result();
           
        }  
        else  
        {  
                return 0;  
        } 
    }
}
?>