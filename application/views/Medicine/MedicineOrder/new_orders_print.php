


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-left: 10px;">
      <h1>
        <?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php //echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i>
         <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Order View"; ?> </li>
      </ol>
    </section>
    
    <section class="invoice">
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     
     
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-home"></i> <?php echo $this->user_model->get_setting_data('site_title'); ?>
            <strong>
            <small class="pull-right"><?php echo "Order Date :"; ?>
             <?php 
                echo $this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)));
             ?>
             </small>
            </strong>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row ">

        <div class="col-sm-6 invoice-col" style="margin-bottom: 20px;">
          <b><?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?></b><br>
          <br>
          <b><?php echo $this->lang->line('date'); ?>:</b>
           <?php echo "<b>".$this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)))."</b>"; ?>
           <br>
          <b><?php echo $this->lang->line('total_bill'); ?>:</b> 
          <?php echo "<b>".$this->lang->line('currency_symbol').' '.$this->user_model->convert_number($orders[0]->total_bill)."</b>"; ?>
        </div>


        <div class="col-sm-6 ">
          <h3 style="margin-top: -10px;"><u><?php echo "Coustomer Information"; ?></u></h3>
          <?php $customer = $this->user_model->get_user($orders[0]->customer_id); 
          if($customer){
          ?>
            <strong>
            <?php echo 'Name : '.$customer->first_name.' '.$customer->last_name; ?><br> 
            <?php echo 'Address : '.$customer->address1; ?><br>
            Phone: <?php echo $customer->phone; ?><br>
            Email: <?php echo $customer->email; ?><br>
            </strong>
          <?php
          } 
          if ($orders[0]->shiping_info) {
            $shipping_info = json_decode($orders[0]->shiping_info);
          ?>

          <b><h5>Shipping Info : </h5></b>
          <?php if($shipping_info){echo 'Shipping Phone:-'.$customer->phone.'<br>';} ?>
          <?php if($shipping_info){echo 'Shipping Address:-'.$shipping_info->address;} ?>
          <?php
          }
          ?>
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 10px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Medicine Name</th>
              <th>Qty</th>
              <th>Size</th>
              <th>Power</th>
             <<th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php  if($orders){
              foreach($orders as $order){

                $medicine = get_product_by_id($order->product_id) ;

                 // echo "<pre>";
                 // print_r($medicine);
                 // exit;
              ?>
            <tr>
              <td><?php echo $medicine[0]->name ; ?></td>
              <td><?php echo $this->user_model->convert_number($order->qty); ?></td>
              <td><?php 
              if ($medicine[0]->size != 'nan') {
                echo $medicine[0]->size;
              }else{
                echo "N/A";
              }
              ?></td>
              <td><?php 
              if ($medicine[0]->medicine_mg_ml != 'nan') {
                echo $medicine[0]->medicine_mg_ml;
              }else{
                echo "N/A";
              }
              ?></td>


              <td><?php 
              echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($medicine[0]->price); ?></td>
              
            </tr>
            <?php }} ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->












      <div class="row">
        <!-- /.col -->
        <!-- <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table table-bordered">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php //echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($order->total_bill); ?></td>
              </tr>
              <tr>
                <th>Tax (0%)</th>
                <td><?php echo $this->lang->line('currency_symbol') ?> 0.00</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td><?php echo $this->lang->line('currency_symbol') ?> 0.00</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?php //echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($order->total_bill); ?></td>
              </tr>
            </tbody></table>
          </div>
        </div> -->
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-sm-6">
          <a href="<?php echo site_url('admin/orders/printpreview/'.$order->id); ?>" target="_blank" class="btn btn-default"><?php echo $this->lang->line('print_button'); ?></a>
          
        </div>





<!-- 
        <div class="col-sm-6">
          <div class="pull-right">
             <?php //if($this->session->userdata('current_user_type')=='admin'){ ?>
              <?php //echo form_open('admin/orders/view/'.$order->id.'/'.$type); ?>
                  <div class="input-group input-group-sm">
                    <select name="status" id="" class="form-control select-custom">
                          <Option value="Placed"  <?php //if($order->status=='Placed'){echo 'selected';} ?>>Placed</Option>
                          <Option value="Processing" <?php //if($order->status=='Processing'){echo 'selected';} ?>>Processing</Option>
                          <Option value="Pickup" <?php //if($order->status=='Pickup'){echo 'selected';} ?>>Pickup</Option>
                          <Option value="Delivered" <?php //if($order->status=='Delivered'){echo 'selected';} ?>>Delivered</Option>
                      </select>
                    <span class="input-group-btn">
                      <button type="submit" name="change_status" value="submit" class="btn btn-primary btn-flat" style="margin-right: 5px;">
                        <i class="fa fa-refresh"></i> Save
                      </button>
                    </span>
                  </div>
              <?php //echo form_close(); ?>
              <?php //} ?>
              
             <?php //if($this->session->userdata('current_user_type')!='admin' and $type!='delivered'){ ?>
              <?php //echo form_open('admin/orders/view/'.$order->id.'/'.$type); ?>
                  <div class="input-group input-group-sm">
                    <select name="status" id="" class="form-control select-custom">
                          <Option value="Placed"  <?php //if($order->status=='Placed'){echo 'selected';} ?>>Placed</Option>
                          <Option value="Processing" <?php //if($order->status=='Processing'){echo 'selected';} ?>>Processing</Option>
                          <Option value="Pickup" <?php //if($order->status=='Pickup'){echo 'selected';} ?>>Pickup</Option>
                          <Option value="Delivered" <?php //if($order->status=='Delivered'){echo 'selected';} ?>>Delivered</Option>
                      </select>
                    <span class="input-group-btn">
                      <button type="submit" name="change_status" value="submit" class="btn btn-primary btn-flat" style="margin-right: 5px;">
                        <i class="fa fa-refresh"></i> Save
                      </button>
                    </span>
                  </div>
              <?php //echo form_close(); ?>
              <?php //} ?>
          </div>
        </div>
 -->



      </div>
    </section>
    <!-- /.content -->
  </div>
