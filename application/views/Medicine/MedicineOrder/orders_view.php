


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-left: 10px;">
      <h1>
        <?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php //echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i>
         <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Order View"; ?> </li>
      </ol>
    </section>
    
    <section class="invoice">
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     
     
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-home"></i> <?php echo $this->user_model->get_setting_data('site_title'); ?>
            <strong>
            <small class="pull-right"><?php echo "Order Date :"; ?>
             <?php 
                echo $this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)));
             ?>
             </small>
            </strong>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row ">

        <div class="col-sm-6 invoice-col" style="margin-bottom: 20px;">
          <b><?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?></b><br>
          <br>
          <b><?php echo $this->lang->line('date'); ?>:</b>
           <?php echo "<b>".$this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)))."</b>"; ?>
           <br>
          <!-- <b><?php echo $this->lang->line('total_bill'); ?>:</b> 
          <?php echo "<b>".$this->lang->line('currency_symbol').' '.$this->user_model->convert_number($orders[0]->total_bill)."</b>"; ?>
          <br> -->

           <b><?php echo "Payment Method : "; ?></b> 
          <?php 

          if($orders[0]->payment_type==null){
            echo "<b>N/A</b>";
          }else{
            echo "<b>".$orders[0]->payment_type."</b>";
          }
          ?>
        </div>


        <div class="col-sm-6 ">
          <h3 style="margin-top: -10px;"><u><?php echo "Coustomer Information"; ?></u></h3>
          <?php $customer = $this->user_model->get_user($orders[0]->customer_id); 
          if($customer){
          ?>
            <strong>
            <?php echo 'Name : '.$customer->user_name; ?><br> 
            <?php echo 'Address : '.$customer->address1; ?><br>
            Phone: <?php echo $customer->phone; ?><br>
            Email: <?php echo $customer->email; ?><br>
            </strong>
          <?php
          } 
          if ($orders[0]->shiping_info) {
            $shipping_info = json_decode($orders[0]->shiping_info);
          ?>

          <b><h5>Shipping Info : </h5></b>
          <?php if($shipping_info){echo 'Shipping Phone:-'.$customer->phone.'<br>';} ?>
          <?php if($shipping_info){echo 'Shipping Address:-'.$shipping_info->address;} ?>
          <?php
          }
          ?>
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 10px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Medicine Name</th>
              <th>Qty</th>
              <th>Size</th>
              <th>Power</th>
              <th>Purchase Price</th>
              <th>Selling Price</th>
              <th>product Discount</th>
              <th>Total Price</th>
            </tr>
            </thead>
            <tbody>
            <?php $a=[];?>
            <?php  if($orders[0]->orderdetails){
              $orderdetails = json_decode($orders[0]->orderdetails);
              // echo "<pre>";
              //      print_r($orders);
              //     echo "<pre>";
              $total_price =0;
              $price=0;
              $check = 0;
              $sum =0;
              foreach($orders as $order){

                $medicine = get_product_by_id($order->product_id) ;
                // echo "<pre>";
                //    print_r($medicine);
                //   echo "<pre>";
                  
                   foreach($medicine as $med){
                    if($med->discount_percent == 0){
                      $med->discount_percent = null;
                    }
                    $cal = ($med->price * $med->discount_percent)/100;
                    $discountedPrice = ($med->price - $cal) ;
                    $totalItemPrice = $order->qty*$discountedPrice;
                  }
                  array_push($a,$totalItemPrice);
                  $price= array_sum($a);
//  print_r($price);

                  if($order->discount != 0){

                    $pprice = ($price * $order->discount)/100;
                  }
                  else{
                    $pprice = $price;
                  }

                  // $Tprice = ($pprice + $order->delivery_charge);
                  $Tprice = $pprice;
                  // print_r($Tprice);
                  
                  ?>
                  <?php $this->m_m->updateInvoiceTotal($order->invoice_id,$price); ?>
            
            <tr>
              <td><?php echo $medicine[0]->name ; ?></td>
              <td><?php echo $order->qty ; ?></td>
              <td><?php 
              if ($medicine[0]->size != 'nan') {
                echo $medicine[0]->size;
              }else{
                echo "N/A";
              }
              ?></td>
              <td><?php 
              if ($medicine[0]->medicine_mg_ml != 'nan') {
                echo $medicine[0]->medicine_mg_ml;
              }else{
                echo "N/A";
              }
              ?></td>

                
              
              
              <form action="<?php echo base_url('Medicine/OrderMedicine/reiciveTK/');?>" method="POST">
              <td>
              <input type="number" name="purchasePrice" value="<?php echo $medicine[0]->purchasePrice ; ?>">
              </td>
              <td> 
                <input type="number" name="product_price" value="<?php echo $medicine[0]->price ; ?>">
                <input type="hidden" name="product_id" value="<?php echo $order->product_id; ?>">
                <input type="hidden" name="qty" value="<?php echo $order->qty; ?>">
                

                <input type="hidden" name="invoice_id" value="<?php echo $order->invoice_id; ?>">
                
                <input type="submit" name="submit" value="submit">
                </td>
              </form>

              

             
            
            <td><?php echo $medicine[0]->discount_percent ; ?> %</td>
            
            <td>

            
             <?php 


          
          echo $this->lang->line('currency_symbol').' '.$totalItemPrice; 
         
          

             ?></td>
              
            </tr>
            <?php } 

            ?>


            <!-- <tr>

              <td colspan="5" align="center" style="font-size: 14px; font-style: bold;">
               
               <b> Delivery Charge </b>
              </td>

              <td  style="font-size: 14px; font-style: bold;">
               
               <b><?php echo $this->lang->line('currency_symbol').' '.$orders[0]->delivery_charge; ?> </b>
              </td>
              
            </tr> -->


            <!-- <tr>

              <td colspan="5" align="center" style="font-size: 14px; font-style: bold;">
               
               <b> Discount </b>
              </td>

              <td  style="font-size: 14px; font-style: bold;">
               
               <b><?php 
               
               echo "(-)".$this->lang->line('currency_symbol').' '.$orders[0]->discount; ?> </b>
              </td>
              
            </tr> -->


            <tr>

              <td colspan="5" rowspan="2" align="center" style="font-size: 18px; font-style: bold;">
               
               <b> Sub-Total </b>
              </td>

              <td  style="font-size: 16px; font-style: bold;">
               
               <b><?php
              
              //  $totalbill =  $price+$orders[0]->delivery_charge + $orders[0]->discount;
               
               
               echo $this->lang->line('currency_symbol').' '.$price; ?> </b>
              </td>
              
            </tr>
            <tr>

              

              <?php

          }
          if($orders[0]->prescriptionDetails){
            $prescriptionDetails = json_decode($orders[0]->prescriptionDetails);
            foreach($prescriptionDetails as $prescriptionDetail){

              ?>
             <td><img src="<?php echo $prescriptionDetail->image; ?> " alt="Image" class="img-responsive img-thumbnail" style="max-width:200px"></td>
            <?php  
            }
         }
            ?>
              
            </tr>
           

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->





      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-sm-6">
          <a type="button" class="btn btn-success" 

          href="<?php 
              echo site_url('Medicine/OrderMedicine/newOrderMedicine/');  

         ?>"  class="btn btn-default"><?php echo "Submit"; ?></a>
          
        </div>







      </div>
    </section>
    <!-- /.content -->
  </div>
