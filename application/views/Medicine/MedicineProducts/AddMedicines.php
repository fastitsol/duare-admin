

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('Medicine/MedicineController/AddMedicine'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo "Add Medicine"; ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Add Medicine"; ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box" style="border-radius: 3px; border:1px solid gray; ">

            <div class="box-header with-border">
                 <h3 class="box-title"><?php  echo "Add New Medicine"; ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('Medicine/MedicineController/AddMedicine'); ?>
            <div class="box-body" >
              <div class="col-md-6" >
               
                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Name"; ?></label>
                  <input type="text" name="product_name" class="form-control" id="product_name" placeholder="<?php echo "Medicine Name"; ?>"  required>
                </div>

                <div class="form-group">
                  <label for="product_name"><?php echo "Product Type"; ?></label>
                  <input type="text" name="product_type" class="form-control" disabled="true" id="product_name"  value="medicine" required>
                </div>

                
                <input type="hidden" name="restaurant_id" class="form-control" id="product_name" value="<?php echo $Medicine_id; ?>"  required>
                
                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Catagory"; ?></label>
                  <input type="text" name="Medicine_Catagory" class="form-control" id="product_name" placeholder="<?php echo "Medicine Catagory"; ?>"  required>
                </div>

                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Type"; ?></label>
                  <input type="text" name="Medicine_Type" class="form-control" id="product_name" placeholder="<?php echo "Medicine Type"; ?>"  required>
                </div>

                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Size"; ?></label>
                  <input type="text" name="Medicine_Size" class="form-control" id="product_name" placeholder="<?php echo "Medicine Size"; ?>"  required>
                </div>

                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Power(Mg/Ml)"; ?></label>
                  <input type="text" name="Medicine_Power" class="form-control" id="product_name" placeholder="<?php echo "Medicine Power(Mg/Ml)"; ?>"  required>
                </div>

                <div class="form-group">
                  <label for="product_name"><?php echo "Medicine Company"; ?></label>
                  <input type="text" name="medicine_company" class="form-control" id="product_name" placeholder="<?php echo "Medicine Company"; ?>"  required>
                </div>



              </div>

              <div class="col-md-6" >
                
                <div class="form-group">
                  <label for="purchasePrice">Medicine Purchase Price</label>
                  <input type="number" min="0" step="0.01" name="purchasePrice" class="form-control" id="purchasePrice" placeholder="Purchase Price"  required>
                </div>
                <div class="form-group">
                  <label for="price">Medicine Selling Price</label>
                  <input type="number" min="0" step="0.01" name="price" class="form-control" id="price" placeholder="<?php echo "Medicine Price"; ?>"  required>
                </div>
                
               

                <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('opening_time'); ?></label>

                      <div class="input-group">
                        <input name="opening_time" type="text" class="form-control timepicker">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>

                <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('closing_time'); ?></label>

                      <div class="input-group">
                        <input name="closing_time" type="text" class="form-control timepicker">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                </div>
                  
                

                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('availability'); ?></label>
                    <select name="availability" class="form-control select2" id="">
                        <option value="available">Available</option>
                        <option value="unavailable">Unavailable</option>
                    </select>
                </div>



                
                <div class="form-group">
                  <label for="product_image"><?php echo $this->lang->line('product_image'); ?></label>
                  <input style="margin:0;padding:0" type="file" name="product_image" class="form-control" id="size" placeholder="<?php echo $this->lang->line('product_image'); ?>">
                </div>



               </div>
                
              </div>




            
            <div class="box-footer">
                <button type="submit" value="save_new_product" name="save_new_product" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>

            </div>
            <?php echo form_close(); ?>
          </div>






        </div>





       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
