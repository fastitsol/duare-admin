
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('restaurant/products/add_new'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_product'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Edit Medicine"; ?> </li>
      </ol>
    </section>
    <!-- Main content -->




    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->



     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                 <h3 class="box-title"><?php  echo $this->lang->line('change'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php if($medicine){
             echo form_open_multipart('Medicine/MedicineController/editMedicine/'.$medicine->id); 
             ?>
            <div class="box-body">
              <div class="col-sm-12">
               
                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Name"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_name" class="form-control" id="product_name" placeholder="<?php echo "Medicine Name"; ?>" value="<?php echo $medicine->name; ?>" required>
                </div>


                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Type"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_type" class="form-control" id="product_name" placeholder="<?php echo "Medicine Type"; ?>" value="<?php echo $medicine->food_type; ?>" required>
                </div>


                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Size"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_size" class="form-control" id="product_name" placeholder="<?php echo "Medicine Size"; ?>" value="<?php echo $medicine->size; ?>" required>
                </div>

                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Power(Mg/ML)"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_power" class="form-control" id="product_name" placeholder="<?php echo "Medicine Power(Mg/ML)"; ?>" value="<?php echo $medicine->medicine_mg_ml; ?>" required>
                </div>

                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Catagory"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_catagory" class="form-control" id="product_name" placeholder="<?php echo "Medicine Catagory"; ?>" value="<?php echo $medicine->description; ?>" required>
                </div>


                <div class="form-group">
                  <label for="product_name">
                  <?php 
                  echo "Medicine Company"; 
                  ?>
                    
                  </label>
                  <input type="text" name="Medicine_company" class="form-control" id="product_name" placeholder="<?php echo "Medicine Company"; ?>" value="<?php echo $medicine->medicine_company; ?>" required>
                </div>


                
                
                
                <div class="form-group">
                  <label for="price">Medicine Purchase Price</label>
                  <input type="number" min="0" step="0.01" name="purchasePrice" class="form-control" id="price" placeholder="Purchase Price" value="<?php echo $medicine->purchasePrice; ?>" required>
                </div>
                <div class="form-group">
                  <label for="price">Medicine Selling Price   
                  </label>
                  <input type="number" min="0" step="0.01" name="price" class="form-control" id="price" placeholder="<?php echo $this->lang->line('price'); ?>" value="<?php echo $medicine->price; ?>" required>
                </div>
                
              
                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('availability'); ?></label>
                    <select name="availability" class="form-control select2" id="">
                        <option value="available" <?php if($medicine->availability=='available'){ echo 'selected';} ?> >Available</option>
                        <option value="unavailable" <?php if($medicine->availability=='unavailable'){ echo 'selected';} ?>>Unavailable</option>
                    </select>
                </div>

  
                
                <div class="form-group">
                    <img src="<?php echo site_url('uploads/products/'.$medicine->image); ?>" alt="Product Image" class="img-responsive img-thumbnail" style="max-height:100px">
                    <hr>
                  <label for="product_image"><?php echo "Medicine Image"; ?></label>
                  <input style="margin:0;padding:0" type="file" name="product_image" class="form-control" id="size" placeholder="<?php echo $this->lang->line('product_image'); ?>">
                </div>
                
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_product" name="save_product" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); } ?>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
