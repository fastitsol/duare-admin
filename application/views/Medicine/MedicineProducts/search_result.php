


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('Medicine/MedicineController/AddMedicine'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_product'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('product_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> <?php echo $this->lang->line('product_list'); ?> </h3>
              <div class="form-inline active-cyan-4 pull-right">
                <?= form_open('Medicine/MedicineController/search');?>
                  <input class="form-control form-control-sm" name="query" type="search" placeholder="Search"
                    aria-label="Search" id="myInput" required>
                  <button class="btn btn-outline-success" type="submit">Search</button>
                <?= form_close();?>
                <?= form_error('query',"<p class='navbar-text'>",'</p');?>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th><?php echo "No."; ?></th>
                  <th><?php echo "Medicine Image"; ?></th>
                  <th><?php echo "Medicine Name"; ?></th>
                  <th><?php echo "Medicine Type"; ?></th>
                  <th><?php echo "Medicine Size"; ?></th>
                  <th><?php echo "Medicine Power(Mg/ML)"; ?></th>
                  <th><?php echo "Medicine Catagory"; ?></th>
                  <th><?php echo "Medicine Purchase Price"; ?></th>
                  <th><?php echo "Medicine Selling Price"; ?></th>
                  <th><?php echo "Discount Percent"; ?></th>
                  <th><?php echo "Medicine Company"; ?></th>

                  <th><?php echo "Medicine Availability"; ?></th>
                  <th><?php echo "Action"; ?></th>
                </tr>
                <?php $counter=0; if($medicines): foreach($medicines as $medicine): $counter++; ?>
                <tr>
                  <td>
                    <?php echo $medicine->id; ?>
                  </td>
                  <td><img src="<?php if($medicine->image) {echo site_url('uploads/products/'.$medicine->image);} else{echo site_url('uploads/'.'medicine.png');} ?>" alt="Product Image" class="img-responsive img-thumbnail" style="max-width:100px"></td>
                  

                  <td><?php echo $medicine->name; ?></td>
                  <td><?php echo $medicine->food_type; ?></td>
                  <td><?php  echo $medicine->size; ?></td>
                  <td><?php  echo $medicine->medicine_mg_ml; ?></td>
                  <td><?php  echo $medicine->description; ?></td>

                  <td><?php echo $medicine->purchasePrice;  ?></td>
                  <td><?php echo $medicine->price;  ?></td>

                  <td><?php echo $medicine->discount_percent." %";  ?></td>
                  <td><?php  echo $medicine->medicine_company; ?></td>
                  <td><?php  echo $medicine->availability; ?></td>

                  <td style="width:100px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('Medicine/MedicineController/editMedicine/'.$medicine->id); ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>

                        <a href="<?php echo site_url('Medicine/MedicineController/deleteMedicine/'.$medicine->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
