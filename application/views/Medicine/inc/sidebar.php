<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$restaurant_id= $this->session->userdata('restaurant_id');
$user_type = $this->session->userdata('current_user_type', $restaurant_id);
$placed = $this->order_model->orders_count('placed', $restaurant_id);
$processing = $this->order_model->orders_count('processing', $restaurant_id);
$pickup = $this->order_model->orders_count('pickup', $restaurant_id);
$delivered = $this->order_model->orders_count('delivered', $restaurant_id);
?>
  
<?php 

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(3);
$third_menu = $this->uri->segment(4);
$dashboard = '';

$products = '';
$all_product = '';
$new_product = '';
$product_category = '';


$users = '';
$all_users = '';
$add_new_user = '';


//this variable for members
$settings ='';
$user_profile ='';
$edit_restaurant ='';

//this variable for members
$orders ='';
$order_placed ='';
$order_processing ='';
$order_pickup ='';
$order_delivered ='';


if($template=='users'){
  $settings='active';   
}
elseif($template=='dashboard'){
  $dashboard='active';   
}
elseif($template=='products'){
  $products='active';   
}

elseif($template=='settings'){
  $settings='active';   
}
elseif($template=='orders'){
  $orders='active';   
}


/*Sub menu handalling*/

if($submenu=='all_user' or $submenu=='edit_user'){
  $all_users='active';   
}
if($submenu=='add_new'){
  $add_new_user='active';   
}


if($template=='settings' and $submenu=='member'){
   $member_settings ='active'; 
}
else if($template=='products' and $submenu=='all_product'){
   $all_product ='active'; 
}
else if($template=='products' and $submenu=='add_new'){
   $new_product ='active'; 
}
else if($template=='products' and $submenu=='category'){
   $product_category ='active'; 
}

else if($template=='users' and $submenu=='edit_user'){
   $user_profile ='active'; 
}
else if($template=='users' and $submenu=='restaurant'){
   $edit_restaurant ='active'; 
}

else if($template=='orders' and $submenu=='placed'){
   $order_placed ='active'; 
}

else if($template=='orders' and $submenu=='processing'){
   $order_processing ='active'; 
}

else if($template=='orders' and $submenu=='pickup'){
   $order_pickup ='active'; 
}

else if($template=='orders' and $submenu=='delivered'){
   $order_delivered ='active'; 
}



?>
 <aside class="main-sidebar"  style="background-color: black;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"  style="background-color: black;">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="background-color: #60E9F8 ;">
        <div class="pull-left image">
          <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info" style="background-color: #60E9F8 ;">
          <p><?php echo $user_data->first_name.' '.$user_data->last_name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
     
      <ul class="sidebar-menu" style="background-color: black;">
        <li class="header" style="background-color: black;"> <?php echo $this->lang->line('menu_main_menu'); ?> </li>
         <?php if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'  or 
       $this->session->userdata('current_user_type')=='pharmacy'): ?>
        <li class="<?php 

        echo $dashboard; 

        ?> treeview">


        <?php
            if($this->session->userdata('current_user_type')=='admin'){
        ?>
          <a href="<?php echo site_url('admin/dashboard'); ?>">
            <i class="fa fa-dashboard text-yellow"></i> <span> 
              <?php  echo $this->lang->line('menu_dashboard'); ?> </span>
          </a>
        <?php
      }else if($this->session->userdata('current_user_type')=='restaurant'  or 
       $this->session->userdata('current_user_type')=='pharmacy')
           {
        ?>
        <a href="<?php echo site_url('restaurant/Dashboard/'); ?>">
            <i class="fa fa-dashboard text-yellow"></i> <span> 
              <?php  echo $this->lang->line('menu_dashboard'); ?> </span>
          </a>
        <?php
      }
      ?>
    </li>





        
        <!--Product manage menu-->
        <li class="treeview <?php echo $products; ?>">
          <a href="#">
            <i class="fa fa-server"></i>
            <span>
     <?php

     if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'){

             echo $this->lang->line('menu_product'); 

    }else if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'  or 
       $this->session->userdata('current_user_type')=='pharmacy')
    {
        echo "Medicine";
    }

    ?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>


          <?php 

            if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'){
           

          ?>
          <!-- for restaurant -->
          <ul class="treeview-menu" style="background-color: black;">
            <li class="<?php echo $all_product; ?>"><a href="<?php echo site_url('restaurant/products/all_product'); ?>"><i class="fa fa-list"></i> 

              <?php 

                echo $this->lang->line('menu_all_product'); 

              ?> </a></li>
            
            <li class="<?php echo $new_product; ?>"><a href="<?php echo site_url('restaurant/products/add_new'); ?>"><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('menu_new_product'); ?></a></li>
            
            <li class="<?php echo $product_category; ?>"><a href="<?php echo site_url('restaurant/products/category'); ?>"><i class="fa  fa-th-list"></i><?php echo $this->lang->line('menu_product_category'); ?></a></li>
          </ul>
          <!-- for restaurant -->

          <!-- for madicine start -->
          <?php
        }else if($this->session->userdata('current_user_type')=='admin' or 
       $this->session->userdata('current_user_type')=='pharmacy')
           {
              
          ?>

          <ul class="treeview-menu" style="background-color: black;">
            <li class="<?php echo $all_product; ?>"><a href="<?php echo site_url('Medicine/MedicineController/'); ?>"><i class="fa fa-list"></i> 

              <?php 

                echo "All Medicine"; 

              ?> </a></li>
            
            <li class="<?php echo $new_product; ?>"><a href="<?php echo site_url('Medicine/MedicineController/AddMedicine'); ?>"><i class="fa fa-plus-circle"></i><?php 

            echo "Add New Medicine"; 
            ?></a></li>
            
            <li class="<?php echo $product_category; ?>"><a href="<?php echo site_url('restaurant/Dashboard/#'); ?>"><i class="fa  fa-th-list"></i><?php 

            echo "Medicine category"; ?></a></li>
          </ul>

      <?php

        }
        ?>
        </li>

      <!-- medicine and restaurant all product off -->











    



        
        <!--Order manage menu-->
        <li class="treeview <?php echo $orders; ?>">
          <a href="#">
            <i class="fa  fa-bullhorn"></i>
            <span> 
            <?php 

                echo $this->lang->line('menu_order'); 

            ?> 
            </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-red">
              <?php echo $placed; ?>  
              </small>
            </span>
          </a>


        <?php 

        if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'){
           

        ?>
          <ul class="treeview-menu" style="background-color: black;">
           
            <li class="<?php echo $order_placed; ?>"><a href="<?php echo site_url('restaurant/orders/placed'); ?>"><i class="fa  fa-level-down"></i> <?php echo $this->lang->line('menu_order_placed'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-red"><?php echo $placed; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_processing; ?>"><a href="<?php echo site_url('restaurant/orders/processing'); ?>"><i class="fa  fa-refresh"></i> <?php echo $this->lang->line('menu_order_processing'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-yellow"><?php echo $processing; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_pickup; ?>"><a href="<?php echo site_url('restaurant/orders/pickup'); ?>"><i class="fa  fa-hand-lizard-o"></i> <?php echo $this->lang->line('menu_order_pickup'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-blue"><?php echo $pickup; ?></small>
                </span>
            </a>
            </li>
           
            <li class="<?php echo $order_delivered; ?>"><a href="<?php echo site_url('restaurant/orders/delivered'); ?>"><i class="fa  fa-sign-out"></i> <?php echo $this->lang->line('menu_order_delivered'); ?> 
            
                <span class="pull-right-container">
                  <small class="label pull-right bg-green"><?php echo $delivered; ?></small>
                </span>
                
            </a></li>
            
          </ul>


      <?php
        }else if($this->session->userdata('current_user_type')=='admin' or 
                  $this->session->userdata('current_user_type')=='pharmacy')
           {
      ?>

        <ul class="treeview-menu" style="background-color: black;">
           
            <li class="<?php echo $order_placed; ?>"><a href="<?php echo site_url('Medicine/OrderMedicine/newOrderMedicine'); ?>"><i class="fa  fa-level-down"></i> <?php echo $this->lang->line('menu_order_placed'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-red"><?php echo $placed; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_processing; ?>"><a href="<?php echo site_url('Medicine/OrderMedicine/ProcessingOrderMedicine'); ?>"><i class="fa  fa-refresh"></i> <?php echo $this->lang->line('menu_order_processing'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-yellow"><?php echo $processing; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_pickup; ?>"><a href="<?php echo site_url('Medicine/OrderMedicine/PickupOrderMedicine'); ?>"><i class="fa  fa-hand-lizard-o"></i> <?php echo $this->lang->line('menu_order_pickup'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-blue"><?php echo $pickup; ?></small>
                </span>
            </a>
            </li>
           
            <li class="<?php echo $order_delivered; ?>"><a href="<?php echo site_url('Medicine/OrderMedicine/DeliveredOrderMedicine'); ?>"><i class="fa  fa-sign-out"></i> <?php echo $this->lang->line('menu_order_delivered'); ?> 
            
                <span class="pull-right-container">
                  <small class="label pull-right bg-green"><?php echo $delivered; ?></small>
                </span>
                
            </a></li>
            
          </ul>
        <?php

        }
        ?>
        </li>

        <!-- order part or medicine off -->









        
        
        
        <!-- restaurant - user edit  -->
        
        <li class="treeview <?php echo $settings; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span> <?php echo $this->lang->line('menu_settings'); ?>  </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        <?php 

        if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'){
          
        ?>


          <ul class="treeview-menu" style="background-color: black;">
            <li class="<?php echo $user_profile; ?>">
              <a href="<?php echo site_url('restaurant/users/edit_user'); ?>"><i class="fa fa-users"></i> <?php echo $this->lang->line('user_profile'); ?>
              </a>
            </li>
            <li class="<?php echo $edit_restaurant; ?>">
              <a href="<?php echo site_url('restaurant/users/restaurant'); ?>"><i class="fa fa-home"></i> <?php echo  $this->lang->line('restaurant'); ?>
              </a>
            </li>
            
          </ul>
        <?php
        }else if($this->session->userdata('current_user_type')=='admin' or 
                  $this->session->userdata('current_user_type')=='pharmacy')
           {
        ?>

        <!-- Phermecy - user edit  -->
        <ul class="treeview-menu" style="background-color: black;">
            <li class="<?php echo $user_profile; ?>">
              <a href="<?php echo site_url('restaurant/users/edit_user'); ?>"><i class="fa fa-users"></i> <?php echo $this->lang->line('user_profile'); ?>
              </a>
            </li>
            <li class="<?php echo $edit_restaurant; ?>">
              <a href="<?php echo site_url('restaurant/users/restaurant'); ?>"><i class="fa fa-home"></i> <?php echo  "Medicine Shop"; ?>
              </a>
            </li>
            
        </ul>
        <?php
        }
        ?>
        </li>
        <!-- Phermecy - user edit  off -->
























        <li class="treeview">

        <?php 

        if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='restaurant'){
          
        ?>
          <a href="<?php echo site_url('restaurant/Dashboard/discount_resaurant'); ?>">
            <i class="fa fa-tags" aria-hidden="true"></i>
            <span>Discount</span> 
          </a>
        <?php
        }else if($this->session->userdata('current_user_type')=='admin' or 
                  $this->session->userdata('current_user_type')=='pharmacy')
           {
        ?>
        <a href="<?php echo site_url('Medicine/OrderMedicine/discountMedicine'); ?>">
            <i class="fa fa-tags" aria-hidden="true"></i>
            <span>Discount</span> 
          </a>
        <?php
        }
        ?>
        </li>





        <li class="treeview">

        <?php 

        if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='pharmacy'){
          
        ?>
            <a href="#">
            <i class="fa fa-file" aria-hidden="true"></i>
            <span> <?php echo "Report" ?>  </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>


          <ul class="treeview-menu" style="background-color: black;">
            <li class="<?php echo $user_profile; ?>">
              <a href="<?php echo site_url('Medicine/OrderMedicine/MonthlyReport'); ?>">
               <i class="fa fa-file" aria-hidden="true"></i>
              <span>Monthly</span> 
             </a>
            </li>

            <li class="<?php echo $edit_restaurant; ?>">
              <a href="<?php echo site_url('Medicine/OrderMedicine/YearReport'); ?>">
                <i class="fa fa-file" aria-hidden="true"></i>
               <?php echo  "Yearly" ?>
              </a>
            </li>
            
          </ul>

        <?php
        }
        ?>
        </li>





        
       <?php endif; ?>
        
        <li class="treeview">
          <a href="<?php echo site_url('logout'); ?>">
            <i class="fa fa-power-off text-yellow"></i> <span> <?php echo $this->lang->line('log_out'); ?> </span>
          </a>
        </li>
       
        
      </ul>
     
    </section>
    <!-- /.sidebar -->
  </aside>