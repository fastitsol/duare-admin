<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> <?php echo $this->user_model->get_setting_data('site_title'); ?> | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/custom.css">

  <?php if($this->user_model->get_setting_data('site_language')=='bn'){ ?>
    <style>
        body {
        font-family: 'SolaimanLipi','Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{font-family: 'SolaimanLipi','Source Sans Pro',sans-serif;}
        .main-header .logo{font-family: 'SolaimanLipi',"Helvetica Neue",Helvetica,Arial,sans-serif;}
    </style>
    <?php } ?>
    
</head>
<body class="hold-transition">
<div class="login-header">
   <div class="blue-effect"></div>
    <div class="login-header-bottom"></div>
</div>
<div class="login-page">
   <div class="login-box">
  <div class="login-logo">
    <a href="#">
        <img src="<?php echo site_url('uploads/duare-logo.png'); ?>" alt="" class="img-responsive">
    </a>

  </div>
  <?php if(isset($login_error)): ?>
   <div class="col-sm-12" style="margin-top:15px">
        <div class="alert alert-danger">
         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $this->lang->line('log_in_error'); ?>
        </div>
    </div>
    <?php endif; ?>
    <!--Display the confirmation message -->
        <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
        <div class="col-sm-12 message_display_class" style="margin-top:15px"> 
            <?php if($this->session->userdata('success_msg')): ?>
            <div class="alert alert-success alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
            </div>
            <?php endif; ?>
            <?php if($this->session->userdata('error_msg')): ?>
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
            </div>
            <?php endif; ?>
            <?php if(isset($validation_errors)): ?>
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>Faield!</strong> <?php echo $validation_errors; ?>
            </div>
            <?php endif; ?>
            <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
   $this->session->set_userdata($sesattr); ?>
        </div>
        <?php endif; ?>
    
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"> <?php echo $this->lang->line('log_in'); ?> </p>
   
     <form action="<?php echo site_url('login/try_login'); ?>" class="login_form" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" id="inputEmail3" placeholder="  <?php echo $this->lang->line('user_name'); ?> " name="user_name" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password_input" placeholder="  <?php echo $this->lang->line('password'); ?> " name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <a href="<?php echo site_url('sign_up'); ?>" class="btn btn-link custom_link"  style="padding-left:0px"><?php echo $this->lang->line('sign_up'); ?></a>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <input type="submit" value=" <?php echo $this->lang->line('log_in'); ?> " class="btn btn-primary btn-block btn-flat" />
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
          <div class="col-sm-12">
              <a href="<?php echo site_url('forget'); ?>" class="btn btn-link"  style="padding-left:0px"><?php echo $this->lang->line('forget_password'); ?></a>
          </div>
      </div>
    </form>


  </div>
  <!-- /.login-box-body -->
</div> 
</div>

<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo site_url('assets/admin/'); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
