


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

    	<h3 style="font-size: 14px;">Add New Coupon</h3>
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php //echo $this->lang->line('change'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            	<?php
			      if($this->session->flashdata('error_coupon')){
			      ?>
			      <div class="alert alert-danger">
			        <?php echo $this->session->flashdata('error_coupon');?>
			        
			      </div>
			      <?php
			      }
			  ?>

			  <?php
			      if($this->session->flashdata('coupon_done')){
			      ?>
			      <div class="alert alert-success">
			        <?php echo $this->session->flashdata('coupon_done');?>
			        
			      </div>
			      <?php
			      }
			  ?>
             
              
            


			<div class="col-sm-12">
			    <assaid class="customer_profile_info">
					<?php echo form_open('admin/Discount/new_add_coupon/'); ?>

					<label for="type" style="font-size: 16px; font-style: bold; " >Enter New Coupon </label>
					<br>

					<label for="type" style="font-size: 12px;  margin-bottom: 40px;" >Noted This Coupon will be use in <b style="font-size: 14px; font-style: bold; ">All Restaurants All Product </b> or For  <b style="font-size: 14px; font-style: bold; ">Selected Restaurant All product </b>:</label>
					
					<div class="form-group row" >
						<div class="col-sm-3">
					   </div>
						<div class="col-sm-9">

							<?php 
									if($all_restaurants){



							?>
					    <select class="form-control"  style="width: 400px; text-align:center; border-radius: 4px;" id="sel1" name="restaurant_id">

					    		<option value="0">Select rastaurants name</option>
					    		<?php 
					    			foreach ($all_restaurants as $all_restaurant) {
					    				
					    			

					    		?>
					      		
						        
						        <option value="<?php echo $all_restaurant->id ;?>"><?php echo $all_restaurant->name ;?></option>
						        

						        <?php


					    			}

					    		?>
					    </select>
					    <?php 
					    	}
					    ?>

					   </div>

					</div>


					<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label">Coupon </label>
							<div class="col-sm-9">
								
								<input style="width: 400px; text-align:center; border-radius: 4px;" class="form-control" id="type" placeholder="Enter Coupon" name="coupon" type="text"  required>
							
							</div>
						</div>
					
					
					

						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label">Coupon Value in Percentage</label>
							<div class="col-sm-9">
								
								<input style="width: 400px; text-align:center; border-radius: 4px;" class="form-control" id="type" placeholder="0" name="coupon_value" type="number"  required>
							
							</div>
						</div>


						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label">Coupon Start Date</label>
							<div class="col-sm-9">
								
								<input style="width: 400px; text-align:center; border-radius: 4px;" class="form-control" id="type" value="<?php echo date('Y-m-d');?>" placeholder="0" name="coupon_start_date" type="date"  required>
							
							</div>
						</div>

						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label">Coupon End Date</label>
							<div class="col-sm-9">

								<?php 


								$date = date('Y-m-d');
								$date = strtotime($date);
								$date = strtotime("+7 day", $date);
								$nextdate = date('Y-m-d', $date);

								
								?>
								
								<input style="width: 400px; text-align:center; border-radius: 4px;" class="form-control" id="type" value="<?php echo $nextdate;?>" placeholder="0" name="coupon_end_date" type="date"  required>
							
							</div>
						</div>
					
						
						
					
						<div class="form-group row">
							<label for="df" class="col-sm-3 col-form-label">  </label>
							<div class="col-sm-9">
								<input type="submit" name="save_user" value="<?php echo "Submit"; ?>" class="btn btn-success btn-reservation" />
								
							</div>
						</div>
                    
					<?php echo form_close(); ?>
			    </assaid>
			</div>
           

           
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
