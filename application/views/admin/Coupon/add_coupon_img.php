
<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('admin/products/category'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_product_new_category'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('product_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('menu_product_edit_category'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('admin/Discount/add_new_coupon_img/'.$single_cat->id); ?>
            <div class="box-body">
            <div class="form-group">
                <label for="img_name">Coupon Image</label>
                <input style="margin:0;padding:0" type="file" name="img_name" class="form-control" id="size" placeholder="Coupon Image" required>
            </div>
            <!-- <div class="form-group row">
                <div class="col-sm-9">
                    <input type="submit" name="save_user" value="<?php echo "Submit"; ?>" class="btn btn-success btn-reservation" />
                </div>
            </div> -->
            </div>
            <div class="box-footer">
                <button type="submit" value="save_cat" name="save_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          
        </div>
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('product_category_list'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th> <?php echo $this->lang->line('serial_number'); ?> </th>
                      <th> Coupon Image </th>
                      <th> <?php echo $this->lang->line('change'); ?> </th>
                    </tr>
                    <?php  $counter=0; if($categories): foreach($categories as $category): $counter++;  ?>
                    <tr>
                      <td><?php echo $this->user_model->convert_number($counter); ?></td>
                      <td><?php echo $category->category_name; ?></td>
                      <td><img src="<?php echo $category->category_image; ?> " alt="Product Image" class="img-responsive img-thumbnail" style="max-width:100px"></td>
                      
                      <td>
                        <div class="btn-group">
                            <a href="<?php echo site_url('admin/products/category/'.$category->id); ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a>
                            <a href="<?php echo site_url('admin/products/delete_category/'.$category->id); ?>" class="btn btn-danger" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; endif; ?>

                  </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

    	<h3 style="font-size: 14px;">Add New Coupon Image</h3>
     
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php //echo $this->lang->line('change'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?php
			      if($this->session->flashdata('error_coupon')){
			      ?>
			      <div class="alert alert-danger">
			        <?php echo $this->session->flashdata('error_coupon');?>
			        
			      </div>
			      <?php
			      }
			  ?>
			  <?php
			      if($this->session->flashdata('coupon_done')){
			      ?>
			      <div class="alert alert-success">
			        <?php echo $this->session->flashdata('coupon_done');?>
			        
			      </div>
			      <?php
			      }
			  ?>
			<div class="col-sm-12">
			    <assaid class="customer_profile_info">
					<?php echo form_open_multipart('admin/Discount/add_new_coupon_img/'); ?>
					<label for="type" style="font-size: 16px; font-style: bold; " >Add New Coupon Image </label>
					<br>
					<label for="type" style="font-size: 12px;  margin-bottom: 40px;" >Noted This Coupon image will be use in <b style="font-size: 14px; font-style: bold; ">All Restaurants All Product :</label>
					    <!-- <div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label">Coupon Image</label>
							<div class="col-sm-9">
								<input style="width: 400px; text-align:center; border-radius: 4px;" class="form-control" id="type" placeholder="Enter Coupon Image" name="coupon" type="text"  required>
							</div>
						</div> -->
                        <div class="form-group">
                            <label for="img_name">Coupon Image</label>
                            <input style="margin:0;padding:0" type="file" name="img_name" class="form-control" id="size" placeholder="Coupon Image" required>
                        </div>
						<div class="form-group row">
							<div class="col-sm-9">
								<input type="submit" name="save_user" value="<?php echo "Submit"; ?>" class="btn btn-success btn-reservation" />
							</div>
						</div>
					<?php echo form_close(); ?>
			    </assaid>
			</div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
