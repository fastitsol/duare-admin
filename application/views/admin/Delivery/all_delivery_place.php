
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-home"></i> <?php echo "Places"; ?> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo "Places"; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo "Delivery Places"; ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php
                  if($this->session->flashdata('error_coupon')){
                  ?>
                  <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error_coupon');?>
                    
                  </div>
                  <?php
                  }
              ?>

              <?php
                  if($this->session->flashdata('coupon_done')){
                  ?>
                  <div class="alert alert-success">
                    <?php echo $this->session->flashdata('coupon_done');?>
                    
                  </div>
                  <?php
                  }
              ?>
             




              <div class="col-sm-12 table-responsive">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th><?php echo "Number"; ?></th>
                  <th><?php echo "Area Name"; ?></th>
                  <th><?php echo "Place Name"; ?></th>
                  <th><?php echo "Delivery Charge"; ?></th>
                  <th><?php echo "Active Date"; ?></th>
                  
                  <th><?php echo "Action"; ?></th>
                  
                </tr>

                <?php 
                $i = 0;
                if($places): foreach($places as $place): ?>
                <tr>
                  
                  <td><a href="#"><?php $i=$i+1;
                  echo $i; ?></a></td>
                  
                  <td><?php
                  

                  $area = $this->Restaurant_model->get_area_name($place->area_id);
                  if ($area) {
                    echo $area->area_name;
                  }
                    
                   ?></td>


                  <td><b><?php echo $place->delivery_place_name; ?></b></td>
                  <td><?php echo $place->delivery_charge; ?></td>
                  <td><?php echo $place->active_date; ?></td>
                  
               
                  
                  


                  
                  <td style="width:80px">
                    <div class="btn-group">
                       
                       <a  href="<?php echo base_url('admin/Delivery/delete_place/'.$place->delivery_id); ?>" class="btn btn-info btn-xs" onclick="return confirm('Are you sure to delete this record!')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
