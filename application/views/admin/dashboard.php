<head>
<style>
#big_card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 600px;
  margin: auto;
  text-align: center;
  font-family: arial;
  padding-bottom: 20px;
}




#received_restaurant {
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #0D8EDF;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#processing_relivery {
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #DAA90B ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#delivered_users{
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #249320 ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#cancelled_earnings{

  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #BB0A0A ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#big_card label {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}


</style>
</head>

<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$total_message_count = 0;





//Total mother account income
$total_mother_account_balance = 0;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <?php echo $this->lang->line('menu_dashboard'); ?>
        <small>  <?php echo $this->lang->line('menu_control_panel'); ?> </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?></a></li>
        <li class="active"><?php echo $this->lang->line('menu_dashboard'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     <div class="row" style="margin-top: 20px;">

      <div class="col-md-6">
        <div class="card" id="big_card" >
          
          <p align="left" ><label><i class="fa fa-bar-chart" aria-hidden="true"></i> &nbsp; &nbsp;Order Statistics</label></p>
          
          <div class="row" style=" margin-left: 1px; margin-right: 12px;">

            <div class="col-sm-6">
              <a href="<?php echo base_url("admin/orders/taken");?>"><button id="received_restaurant">Received 
                <br>
                <?php
                    if($order_received!=Null){
                        echo $order_received;
                    }else{
                      $order_received = 0;
                      echo $order_received;
                    }
                  ?>


              </button>
            </a>
            </div>
            <div class="col-sm-6" >
              <a href="<?php echo base_url("admin/orders/processing");?>">
                <button id="processing_relivery">Processing
                <br>
                <?php
                    if($order_processing!=Null){
                        echo $order_processing;
                    }else{
                      $order_processing = 0;
                      echo $order_processing;
                    }
                  ?>
               </button>
            </a>
            </div>

          </div>

          <div class="row" style="margin-left: 1px; margin-right: 12px; margin-top: 30px;">

            <div class="col-sm-6">
              <a href="<?php echo base_url("admin/orders/delivered");?>">
                <button id="delivered_users">Delivered
                <br>
                <?php
                    if($order_delivered!=Null){
                        echo $order_delivered;
                    }else{
                      $order_delivered = 0;
                      echo $order_delivered;
                    }
                  ?>
                </button>
            </a>
            </div>
            <div class="col-sm-6">
              <a href="<?php echo base_url("#");?>">
                <button id="cancelled_earnings">Earnings
                <br>
                <i class="fa fa-dollar" style="font-size:24px"></i>
                <?php
                     if($total_earnings!=Null){
                         echo $total_earnings[0]->tatal_earns;
                     }else{
                       echo "0";
                     }
                $order_Cancelled = 0;
                     // echo $order_Cancelled;
                  ?>
               </button>
             </a>
            </div>


          </div>

        </div>
      </div>

      <div class="col-md-6">
        <div class="card" id="big_card">
          
          <p align="left"><label><i class="fa fa-bar-chart" aria-hidden="true"></i> &nbsp; &nbsp;Site Statistics</label></p>
          <div class="card"  >
            <div class="row" style=" margin-left: 1px; margin-right: 12px;">

              <div class="col-sm-6">
                <a href="<?php echo base_url("admin/Restaurants/active");?>"><button id="received_restaurant">Restaurants<b style="font-size: 5px;">&nbsp;&nbsp;Active</b> 

                  <br>
                  <?php
                    if($restaurants!=Null){
                        echo $restaurants."+";
                    }else{
                      echo "0";
                    }
                  ?>
                </button>
                </a>

              </div>
              <div class="col-sm-6" >
               <a href="<?php echo base_url("admin/users/all_user/deliver_man");?>"> <button id="processing_relivery">Delivery People
                  <br>
                  <?php
                    if($deliver_mans!=Null){
                        echo $deliver_mans."+";
                    }else{
                      echo "0";
                    }
                  ?>
                </button>
                </a>
              </div>

            </div>

            <div class="row" style="margin-left: 1px; margin-right: 12px; margin-top: 30px;">

              <div class="col-sm-6">
                <a href="<?php echo base_url("admin/Dashboard/all_customer_user");?>"> <button id="delivered_users">Users
                  <br>
                  <?php
                    if($users!=Null){
                        echo $users."+";
                    }else{
                      echo "0";
                    }
                  ?>
                  
                </button>
              </a>
              </div>
              <div class="col-sm-6">
                <a href="<?php echo base_url("admin/restaurants/inactive");?>"><button id="cancelled_earnings">Restaurants<b style="font-size: 5px;">&nbsp;&nbsp;Inactive</b>
                  <br>
                  <?php
                    if($restaurants_inactive!=Null){
                        echo $restaurants_inactive."+";
                    }else{
                      echo "0";
                    }
                  ?>
                </button>
                </a>
              </div>


            </div>

        </div>
      </div>


    </div>
      
    </section>
    <!-- /.content -->

<p style="font-size: 34px; color: red;" id="demo"></p>

    <section class="content">

       <div class="row" style="margin-top: 20px;">

          <div class="col-md-6">
            <div class="card" id="big_card" >
              
              <p align="left" ><label><i class="fa fa-area-chart" style="font-size:24px"></i> &nbsp; &nbsp; Products Sales</label></p>
              
              
              <div id="chartContainer1" style="height: 350px; width: 100%;"></div>
              <script src="<?php echo base_url("assets/admin/bootstrap/js/canvasjs.min.js");?>"></script>
                

                </div>

             

              </div>

          <div class="col-md-6">
            <div class="card" id="big_card" >
              
              <p align="left" ><label><i class="fa fa-pie-chart" aria-hidden="true"></i> &nbsp; &nbsp;Order</label></p>
              
              

                
                  <div id="chartContainer" style="height: 350px; max-width: 600px;"></div>
                  <script src="<?php echo base_url("assets/admin/bootstrap/js/canvasjs.min.js");?>"></script>
              </div>

             

            </div>



      </div>

    </section>
  </div>













<script>
var  recived_order = parseInt('<?php echo $order_received; ?>');

var  processing_order = parseInt('<?php echo $order_processing; ?>');

var  dalivary_order = parseInt('<?php echo $order_delivered; ?>');

var  cancelled_order = parseInt('<?php echo $order_Cancelled; ?>'); 

</script>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  title:{
    text: "",
    horizontalAlign: "left"
  },


  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 9,
    indexLabel: "{label} - #percent%",
    toolTipContent: "<b>{label}:</b> {y} (#percent%)",
    dataPoints: [
      { y: recived_order, label: "Received" },
      { y: cancelled_order, label: "Cancelled" },
      { y: dalivary_order, label: "Delivered" },
      { y: processing_order, label: "Processing"}
    ]
  }]
});
chart.render();



 var chart = new CanvasJS.Chart("chartContainer1", {
   animationEnabled: true,
   title:{
     text: ""
   },
   axisX:{
     valueFormatString: "DD, MMM ,YYYY"
   },
   axisY: {
     title: "Number of Order",
     includeZero: false,
     scaleBreaks: {
       autoCalculate: true
     }
   },
   data: [{
     type: "line",
     xValueFormatString: "DD, MMM ,YYYY",
     color: "#F08080",
     dataPoints: [
        <?php if($all_order_javas){

             foreach ($all_order_javas as $all_order_java) {
                 echo '{ x: new Date("'.$all_order_java->date_all.'"), y: '.$all_order_java->number.'},';
             }
         }else{
          ?>
          { x: new Date(2017, 0, 1), y: 610 },
          { x: new Date(2017, 0, 2), y: 680 },
          { x: new Date(2017, 0, 3), y: 690 },
          { x: new Date(2017, 0, 4), y: 700 },
          { x: new Date(2017, 0, 5), y: 710 },

          <?php

         }
         ?>

       
   
     ]
   }]
 });
 chart.render();

}

</script>

<!-- SELECT COUNT(invoice.date_time ) as number,( SELECT DATE_FORMAT(invoice.date_time, '%Y %m %d')) as date_all FROM `invoice` JOIN invoice_items on invoice.id=invoice_items.invoice_id GROUP BY ( SELECT DATE_FORMAT(invoice.date_time, "%W %M %e %Y"))








SELECT users.first_name, users.last_name, users.address1, users.id as user_id, users.email, tbl_restaurant.id as restaurant_id, tbl_restaurant.name, tbl_restaurant.email, tbl_restaurant.logo, tbl_restaurant_users.id FROM users, tbl_restaurant, tbl_restaurant_users where users.id = tbl_restaurant_users.user_id and tbl_restaurant_users.restaurant_id = tbl_restaurant.id



 -->