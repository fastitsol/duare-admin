
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-home"></i> <?php echo $this->lang->line('menu_restaurants')." Lists"; ?> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->lang->line('menu_restaurants'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     

     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?php echo "Click The ".$this->lang->line('menu_restaurants')." name and Discount on its item.."; ?></b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12 table-responsive">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th><?php echo $this->lang->line('logo'); ?></th>
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th><?php echo $this->lang->line('phone'); ?></th>
                  <th><?php echo $this->lang->line('email'); ?></th>
                  <th><?php echo $this->lang->line('address'); ?></th>
                 
                  <th><?php echo $this->lang->line('delivery_time'); ?></th>
                  <th><?php echo $this->lang->line('opening_time'); ?></th>
                  <th><?php echo $this->lang->line('closing_time'); ?></th>


                  <th><?php echo $this->lang->line('status'); ?></th>
               
                </tr>
                <?php if($restaurants){

                	foreach($restaurants as $restaurant): ?>
                <tr>
                  <td style="max-width:100px">
                  <?php if($restaurant->logo){ ?>
                  <a target="_blank" href="<?php echo site_url('uploads/restaurant/'.$restaurant->logo); ?>"><img src="<?php echo site_url('uploads/restaurant/'.$restaurant->logo); ?>" alt="" class="img-responsive" style="width:80px"></a>
                  <?php } ?>
                  </td>

                  <td><a href="<?php echo base_url('admin/Discount/restaurants_all_product/'.$restaurant->id);?>"><?php echo $restaurant->name; ?></a></td>

                  <td><?php echo $restaurant->phone; ?></td>
                  <td><?php echo $restaurant->email; ?></td>
                  <td><?php echo $restaurant->address; ?></td>
                  
                  <td><?php echo $restaurant->delivery_time; ?></td>
                  <td><?php echo $restaurant->opening_time; ?></td>
                  <td><?php echo $restaurant->closing_time; ?></td>

                  
                  <td>
                  	<?php 
                  		if( $restaurant->status == 'active'){
                  	?>
                   <a href="<?php echo site_url('admin/restaurants/change_status/'.$restaurant->id.'/inactive'); ?>" class="btn btn-danger btn-xs"><?php echo $restaurant->status; ?> To Inactive</a>

                   <?php
                   		}else if($restaurant->status == 'inactive'){

                   ?>

                   <a href="<?php echo site_url('admin/restaurants/change_status/'.$restaurant->id.'/active'); ?>" class="btn btn-success btn-xs"><?php echo $restaurant->status; ?> To Active</a>

                   <?php
                   		}
                   ?>
                  </td>
                  
                </tr>
                <?php endforeach; 
            }else{

            	echo "No Restaurant";
            }
                 ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
