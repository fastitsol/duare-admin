<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$total_message_count = 0;
$total_car_booking_count = 0;
$new_reservation_count = 0;
$collapse = null;

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(3);
$third_menu = $this->uri->segment(4);
if($template=='sales' and $submenu=='order_memo'){
    $collapse = 'sidebar-collapse';
}elseif($template=='imports' and $submenu=='order_memo'){
     $collapse = 'sidebar-collapse';
}elseif($template=='returns' and $submenu=='order_memo'){
     $collapse = 'sidebar-collapse';
}
else{
    $collapse = '';
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->user_model->get_setting_data('site_title'); ?> | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/admin/'); ?>font-awesome/css/font-awesome.min.css" media="all" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.min.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/custom.css">
    <?php if($this->user_model->get_setting_data('site_language')=='bn'){ ?>
    <style>
        body {
        font-family: 'SolaimanLipi','Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{font-family: 'SolaimanLipi','Source Sans Pro',sans-serif;}
        .main-header .logo{font-family: 'SolaimanLipi',"Helvetica Neue",Helvetica,Arial,sans-serif;}
    </style>
    <?php } ?>
</head>
<body class="hold-transition skin-blue sidebar-mini <?php echo $collapse; ?>" style="background-color: black;">
<div class="wrapper" style="background-color: black;">

  <header class="main-header" style="background-color: black;">
   <input type="hidden" v-model="ajax_url" name="ajax_url" value="<?php echo site_url('admin/ajax'); ?>">
    <!-- Logo -->
    <a href="<?php echo site_url('admin/dashboard'); ?>" class="logo" style="background-color: black;">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>DUARE</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"> <b><span style="color: #2ED13B;"></span><span> DUARE </span></b> </span>
      <input type="hidden" name="site_url" value="<?php echo site_url(); ?>" id="site_url">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color: black;">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          
          <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
          <li class="dropdown messages-menu">
           
            <?php if($this->user_model->get_setting_data('site_language')!='bn'){ ?>
                <a href="<?php echo site_url('admin/settings/change_lang/bn'); ?>" >
                  <i class="fa fa-exchange"></i>  BN
                </a>
            <?php }else{ ?>
                <a href="<?php echo site_url('admin/settings/change_lang/en'); ?>" >
                  <i class="fa fa-exchange"></i>  EN
                </a>
            <?php } ?>
          </li>
          
          <?php } ?>
          
          <li class="dropdown messages-menu">
           
            <a href="<?php echo site_url('admin/dashboard/backup_db'); ?>" >
              <i class="fa fa-database"></i> <?php echo $this->lang->line('db_backup'); ?>
             
            </a>
            
          </li>
          
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $user_data->first_name.' '.$user_data->last_name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="img-circle" alt="User Image">

                <p>
                 <?php echo $user_data->first_name.' '.$user_data->last_name; ?>
                  <small>Member since <?php echo $user_data->p_c_date; ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo site_url('admin/users/edit_user/'.$this->session->userdata('current_user_id')); ?>" class="btn btn-default btn-flat"> <?php echo $this->lang->line('profile'); ?> </a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('logout'); ?>" class="btn btn-default btn-flat"> <?php echo $this->lang->line('log_out'); ?> </a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>