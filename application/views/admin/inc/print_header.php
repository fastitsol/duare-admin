<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$total_message_count = 0;
$total_car_booking_count = 0;
$new_reservation_count = 0;
$collapse = null;

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(3);
$third_menu = $this->uri->segment(4);
if($template=='sales' and $submenu=='order_memo'){
    $collapse = 'sidebar-collapse';
}else{
    $collapse = '';
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->user_model->get_setting_data('site_title'); ?> | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/custom.css">

 <?php if($this->user_model->get_setting_data('site_language')=='bn'){ ?>
    <style>
        body {
        font-family: 'SolaimanLipi','Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{font-family: 'SolaimanLipi','Source Sans Pro',sans-serif;}
        .main-header .logo{font-family: 'SolaimanLipi',"Helvetica Neue",Helvetica,Arial,sans-serif;}
    </style>
    <?php } ?>
</head>
<body class="">
