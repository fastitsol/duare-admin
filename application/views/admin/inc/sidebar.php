<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));

$user_type = $this->session->userdata('current_user_type');
$user_type = $this->session->userdata('current_user_type');
$placed = $this->order_model->orders_count('placed');
$processing = $this->order_model->orders_count('processing');
$pickup = $this->order_model->orders_count('pickup');
$delivered = $this->order_model->orders_count('delivered');

?>
  
<?php 

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(4);
$third_menu = $this->uri->segment(4);
$dashboard = '';

//this variable for members
$orders ='';
$order_placed ='';
$order_taken ='';
$order_processing ='';
$order_pickup ='';
$order_delivered ='';


$products = '';
$all_product = '';
$new_product = '';
$product_category = '';
$subCategory = '';

$restaurants = '';
$restaurants_active = '';
$restaurants_inactive = '';



$users = '';
$all_users = '';
$all_admin = '';
$all_deliver_man = '';
$add_new_user = '';


//this variable for members
$settings ='';
$member_settings ='';
//this variable for reports
$report ='';
$monthlyReport ='';
$yearlyReport ='';


if($template=='users'){
  $users='active';   
}
elseif($template=='dashboard'){
  $dashboard='active';   
}
elseif($template=='products'){
  $products='active';   
}

elseif($template=='settings'){
  $settings='active';   
}
elseif($template=='report'){
  $report='active';   
}

elseif($template=='orders'){
  $orders='active';   
}

elseif($template=='restaurants'){
  $restaurants='active';   
}


/*Sub menu handalling*/

if($template=='users' and $submenu=='all_admin' and $third_menu!='deliver_man'){
  $all_admin='active';   
}

if($template=='users' and $submenu=='all_user' and $third_menu!='deliver_man'){
  $all_users='active';   
}
if($template=='users' and $submenu=='add_new'){
  $add_new_user='active';   
}
if($template=='users' and $submenu=='all_user' and $third_menu=='deliver_man'){
  $all_deliver_man='active';   
}








if($template=='settings' and $submenu=='member'){
   $member_settings ='active'; 
}
if($template=='report' and $submenu=='monthlyReport'){
   $monthlyReport ='active'; 
}
if($template=='report' and $submenu=='yearlyReport'){
   $yearlyReport ='active'; 
}
else if($template=='products' and $submenu=='all_product'){
   $all_product ='active'; 
}
else if($template=='products' and $submenu=='add_new'){
   $new_product ='active'; 
}
else if($template=='products' and $submenu=='category'){
   $product_category ='active'; 
}
else if($template=='products' and $submenu=='subCategory'){
   $subCategory ='active'; 
}

else if($template=='orders' and $submenu=='placed'){
   $order_placed ='active'; 
}

else if($template=='orders' and $submenu=='processing'){
   $order_processing ='active'; 
}

else if($template=='orders' and $submenu=='pickup'){
   $order_pickup ='active'; 
}

else if($template=='orders' and $submenu=='delivered'){
   $order_delivered ='active'; 
}

else if($template=='orders' and $submenu=='taken'){
   $order_taken ='active'; 
}

else if($template=='restaurants' and $submenu=='active'){
   $restaurants_active ='active'; 
}

else if($template=='restaurants' and $submenu=='inactive'){
   $restaurants_inactive ='active'; 
}





?>
 <aside class="main-sidebar" style="background-color: black;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="background-color: black;">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="background-color: #60E9F8 ;">
        <div class="pull-left image">
          <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info" style="background-color: #60E9F8 ;">
          <p style="color: black;"><?php echo $user_data->first_name.' '.$user_data->last_name; ?></p>
          <a style="color: black;" href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
     
      <ul class="sidebar-menu" style="background-color: black;">
        <li class="header" style="background-color: black;"> <?php echo $this->lang->line('menu_main_menu'); ?> </li>
         <?php if($this->session->userdata('current_user_type')=='admin' or $this->session->userdata('current_user_type')=='deliver_man'): ?>
        <li class="<?php echo $dashboard; ?> treeview">
          <a href="<?php echo site_url('admin/dashboard'); ?>" style="background-color: black;">
            <i class="fa fa-dashboard text-yellow"></i> <span> <?php echo $this->lang->line('menu_dashboard'); ?> </span>
          </a>
        </li>
        
        <!--Product manage menu-->
        <li class="treeview <?php echo $products; ?>">
          <a href="#">
            <i class="fa fa-server"></i>
            <span> <?php echo $this->lang->line('menu_product'); ?> </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $all_product; ?>"><a href="<?php echo site_url('admin/products/all_product'); ?>"><i class="fa fa-list"></i> <?php echo $this->lang->line('menu_all_product'); ?> </a></li>
            
            <li class="<?php echo $new_product; ?>"><a href="<?php echo site_url('admin/products/add_new'); ?>"><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('menu_new_product'); ?></a></li>
            
            <li class="<?php echo $product_category; ?>"><a href="<?php echo site_url('admin/products/category'); ?>"><i class="fa  fa-th-list"></i>Category List</a></li>
            <li class="<?php echo $subCategory; ?>"><a href="<?php echo site_url('admin/products/subcategory'); ?>"><i class="fa  fa-th-list"></i> Sub Category List</a></li>
            
          </ul>
        </li>
        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
        <!--Product manage menu-->
        <li class="treeview <?php echo $restaurants; ?>">
          <a href="#">
            <i class="fa  fa-home"></i>
            <span>Restaurants List <?php //echo //$this->lang->line('menu_restaurants'); ?> </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;">
           
            <li class="<?php echo $restaurants_active; ?>"><a href="<?php echo site_url('admin/restaurants/active'); ?>"><i class="fa fa-list"></i> <?php echo $this->lang->line('menu_restaurants_active'); ?> </a></li>
            
            <li class="<?php echo $restaurants_inactive; ?>"><a href="<?php echo site_url('admin/restaurants/inactive'); ?>"><i class="fa fa-list"></i> <?php echo $this->lang->line('menu_restaurants_inactive'); ?> </a></li>
            
            
          </ul>
        </li>
        <?php } ?>
        
        
        
        <!--Product manage menu-->
        <li class="treeview <?php echo $orders; ?>">
          <a href="#">
            <i class="fa  fa-bullhorn"></i>
            <span> <?php echo $this->lang->line('menu_order'); ?> </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-red"><?php echo $placed; ?></small>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;">
           
            <li class="<?php echo $order_placed; ?>"><a href="<?php echo site_url('admin/orders/placed'); ?>"><i class="fa  fa-level-down"></i> <?php echo $this->lang->line('menu_order_placed'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-red"><?php echo $placed; ?></small>
                </span>
            </a></li>
            <li class="<?php echo $order_taken; ?>"><a href="<?php echo site_url('admin/orders/taken'); ?>"><i class="fa  fa-edit"></i> <?php echo "Received"; ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-red"><?php echo $placed; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_processing; ?>"><a href="<?php echo site_url('admin/orders/processing'); ?>"><i class="fa  fa-refresh"></i> <?php echo $this->lang->line('menu_order_processing'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-yellow"><?php echo $processing; ?></small>
                </span>
            </a></li>
           
            <li class="<?php echo $order_pickup; ?>"><a href="<?php echo site_url('admin/orders/pickup'); ?>"><i class="fa  fa-hand-lizard-o"></i> <?php echo $this->lang->line('menu_order_pickup'); ?> 
                <span class="pull-right-container">
                  <small class="label pull-right bg-blue"><?php echo $pickup; ?></small>
                </span>
            </a>
            </li>
           
            <li class="<?php echo $order_delivered; ?>"><a href="<?php echo site_url('admin/orders/delivered'); ?>"><i class="fa  fa-sign-out"></i> <?php echo $this->lang->line('menu_order_delivered'); ?> 
            
                <span class="pull-right-container">
                  <small class="label pull-right bg-green"><?php echo $delivered; ?></small>
                </span>
                
            </a></li>
            
          </ul>
        </li>
        
        
        
        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
        <li class="treeview <?php echo $users; ?>">
          <a href="#">
            <i class="fa fa-users"></i>
            <span> <?php echo $this->lang->line('menu_user'); ?> </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;" >
            <li class="<?php echo $all_admin; ?>"><a href="<?php echo site_url('admin/users/all_user'); ?>"><i class="fa fa-list"></i>All Admin</a></li>

            <li class="<?php echo $all_deliver_man; ?>"><a href="<?php echo site_url('admin/Dashboard/all_restaurant_users'); ?>"><i class="fa fa-list"></i>All Restaurants Users</a></li>

            <li class="<?php echo $all_deliver_man; ?>"><a href="<?php echo site_url('admin/users/all_user/deliver_man'); ?>"><i class="fa fa-list"></i> <?php echo $this->lang->line('menu_all_deliver_man'); ?> </a></li>

            <li class="<?php echo $all_users; ?>"><a href="<?php echo site_url('admin/Dashboard/all_customer_user'); ?>"><i class="fa fa-list"></i>All Users</a></li>
            
            <li class="<?php echo $add_new_user; ?>"><a href="<?php echo site_url('admin/users/add_new'); ?>"><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('menu_new_user'); ?></a></li>
            
          </ul>
        </li>
        <?php } ?>
        


        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-percent" aria-hidden="true"></i>
            <span> Discount </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;" >
            <li >
              <a href="<?php echo base_url('admin/Discount'); ?>"><i class="fa fa-tags" aria-hidden="true"></i>By Restaurant</a></li>

            <li class="<?php  ?>"><a href="<?php echo base_url('admin/Discount/all_product'); ?>">  <span class="fa-stack fa-lg"><i class="fa fa-certificate fa-stack-2x"></i><i class="fa fa-tag fa-stack-1x fa-inverse"></i></span>By Item</a></li>            
          </ul>
        </li>
        <?php } ?>




        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gift" aria-hidden="true"></i>
            <span>Coupon </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;" >
            <li >
              <a href="<?php echo base_url('admin/Discount/Coupon_add/'); ?>"><i class="fa fa-gift" aria-hidden="true"></i>Add Coupon</a></li>

            <li class="<?php  ?>"><a href="<?php echo base_url('admin/Discount/Coupon_add_img'); ?>"> <i class="fa fa-gift" aria-hidden="true"></i>Add Coupon Image</a></li>            
            <li class="<?php  ?>"><a href="<?php echo base_url('admin/Discount/show_all_coupon'); ?>"> <i class="fa fa-gift" aria-hidden="true"></i>All Coupon</a></li>            
          </ul>
          
        </li>
        <?php } ?>
        
        
        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
        <li class="treeview">
          <a href="#">
           <i class="fa fa-truck" aria-hidden="true"></i>
            <span>Delivery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="background-color: black;" >
            <li>
              <a href="<?php echo base_url('admin/Delivery/delivery_area'); ?>"><i class="fa fa-truck" aria-hidden="true"></i>Delivery Area</a>
            </li>
            <li >
              <a href="<?php echo base_url('admin/Delivery/Add_Delivery'); ?>"><i class="fa fa-truck" aria-hidden="true"></i>Add Delivery Place</a></li>

            <li class="<?php  ?>"><a href="<?php echo base_url('admin/Delivery/allDelivery_place/'); ?>"> <i class="fa fa-truck" aria-hidden="true"></i>Delivery Places</a></li>            
          </ul>
          
        </li>

        <!-- <li class="treeview <?php echo $settings; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span> Report </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $member_settings; ?>">
              <a href="<?php echo site_url('admin/settings/member'); ?>"><i class="fa fa-users"></i> Member Settings
              </a>
            </li>
            
          </ul>
        </li> -->
        <li class="treeview <?php echo $report; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span> Report </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $monthlyReport; ?>">
              <a href="<?php echo site_url('admin/Dashboard/monthlyReport'); ?>"><i class="fa fa-users"></i> Monthly report 
              </a>
            </li>
            
          </ul>
          <ul class="treeview-menu">
            <li class="<?php echo $yearlyReport; ?>">
              <a href="<?php echo site_url('admin/Dashboard/yearlyReport'); ?>"><i class="fa fa-users"></i> Yearly report
              </a>
            </li>
            
          </ul>
        </li>
        <?php } ?>

        
        
       <?php endif; ?>
        
        <li class="treeview">
          <a href="<?php echo site_url('logout'); ?>">
            <i class="fa fa-power-off text-yellow"></i> <span> <?php echo $this->lang->line('log_out'); ?> </span>
          </a>
        </li>
       
        
      </ul>
     
    </section>
    <!-- /.sidebar -->
  </aside>