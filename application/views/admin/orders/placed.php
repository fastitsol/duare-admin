
  <!-- Left side column. contains the logo and sidebar -->




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->lang->line('order_list'); ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('order_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('order_list'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th>#<?php echo $this->lang->line('order_id'); ?></th>
                  <th><?php echo $this->lang->line('date'); ?></th>
                  <th><?php echo $this->lang->line('customer_info'); ?></th>
                  <th><?php echo $this->lang->line('shipping_info'); ?></th>
                  <th><?php echo $this->lang->line('total_bill'); ?></th>
                  <th><?php echo $this->lang->line('take'); ?></th>
                  <th><?php echo $this->lang->line('change'); ?></th>
                </tr>
                <?php $counter=0; if($orders): foreach($orders as $order): $counter++; 
                    $shipping_info = json_decode($order->shiping_info);
                    $couponDiscount = $shipping_info->discount;
                    $resProAmount = $this->restaurant_model->getProductAmount($order->invoice_id);
                    // echo "<pre>";
                    // print_r($info);
                    ?>
                <tr>
                  <td>#<?php echo $this->user_model->convert_number($order->invoice_id); ?></td>
                  <td><?php echo $this->user_model->convert_number(date('d/m/Y',strtotime($order->date_time))); ?></td>
                  <td><?php 
                      $customer = $this->user_model->get_user($order->customer_id);
                      if($customer){ ?>
                          <b>Name:- </b> <?php echo $customer->user_name; ?> <br>
                          <b>Phone:- </b> <?php echo $customer->phone; ?> <br>
                          <b>Address:- </b> <?php echo $customer->address1; ?> <br>
                          
                        <?php } ?>
                  </td>
                  <td>
                      <?php if($shipping_info /*and $info->phone*/){echo 'Phone:-'.$customer->phone.'</br>';} ?>
                      <?php if($shipping_info /*and $info->phone*/){echo 'Address:-'.$shipping_info->address;} ?>
                  </td>
                  <td><?php

// $totalwithDeliveryCharge=$totalAmount+$order->delivery_charge;
// $discountConvertToPrice= (($totalAmount+$orders[0]->delivery_charge) * $orders[0]->discount)/100;
// $total = $totalwithDeliveryCharge - $discountConvertToPrice;
// echo $this->lang->line('currency_symbol').' '.ceil($total);


                  $totalBill=($resProAmount[0]->total_amount);
                  $discountConvertToPrice= ($totalBill * $couponDiscount)/100;
                  $total = ($totalBill - $discountConvertToPrice)+($order->delivery_charge);
                  echo $this->lang->line('currency_symbol').' '.($total);
                  // echo $this->lang->line('currency_symbol').' '.$order->total_bill; 
                  ?></td>
                  
                  <td>
                      <?php if($order->take_by){ ?>
                          <?php echo $this->user_model->get_user_name($order->take_by); ?>
                      <?php }else{ ?>
                          <a href="<?php echo site_url('admin/orders/take/'.$order->invoice_id); ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                      <?php } ?>
                  </td>
                  <td style="width:100px">
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/orders/view/'.$order->invoice_id.'/placed'); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                        <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
                        <a href="<?php echo site_url('admin/orders/delete/'.$order->invoice_id.'/placed'); ?>" class="btn btn-danger btn-sm" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>


<!-- SELECT * FROM `invoice` join users, invoice_items WHERE invoice.customer_id=users.id and invoice.id = invoice_items.invoice_id GROUP BY invoice.id -->