<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->lang->line('invoice'); ?> #<?php echo $orders[0]->id; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('order_list'); ?> </li>
      </ol>
    </section>
    
    <section class="invoice">
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     
     
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-home"></i> <?php echo $this->user_model->get_setting_data('site_title'); ?>
            <small class="pull-right"><?php echo $this->lang->line('date'); ?>: <?php echo $this->user_model->convert_number(date('d/m/Y',strtotime($orders[0]->date_time))); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <!-- <div class="col-sm-4 invoice-col">
          <?php echo $this->lang->line('from'); ?>
          
          <address>
          
            <strong><?php echo $this->user_model->get_setting_data('site_title'); ?></strong><br>
            <?php echo $this->user_model->get_setting_data('site_address'); ?><br>
            Phone: <?php echo $this->user_model->get_setting_data('site_phone'); ?><br>
            Email: <?php echo $this->user_model->get_setting_data('site_email'); ?>
          </address>

          
        </div> -->
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <?php echo $this->lang->line('to'); ?>
          <address>
           <?php $customer = $this->user_model->get_user($orders[0]->customer_id); if($customer){?>
            <strong><?php echo 'Name : '.$customer->user_name; ?></strong><br>
            <?php echo 'Address : '.$customer->address1; ?><br>
            Phone: <?php echo $customer->phone; ?><br>
            Email: <?php echo $customer->email.'<br/>'; ?>
            <?php } ?>
              <?php  $shipping_info = json_decode($orders[0]->shiping_info); ?>
              <?php if($shipping_info){echo 'Shipping Phone:-'.$customer->phone.'<br/>';} ?>
              <?php if($shipping_info){echo 'Shipping Address:-'.$shipping_info->address;} ?>
              
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b><?php echo $this->lang->line('invoice'); ?> #<?php echo $orders[0]->id; ?></b><br>
          <br>
          <b>Order ID:</b> <?php echo $orders[0]->id; ?><br>
          <b><?php echo $this->lang->line('date'); ?>:</b> <?php echo $this->user_model->convert_number(date('d/m/Y',strtotime($orders[0]->date_time))); ?><br>
          <!-- <b><?php echo $this->lang->line('total_bill'); ?>:</b> <?php echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($orders[0]->total_bill); ?> -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Restaurant name</th>
              <th>Product Name</th>
              <th>Size</th>
              <th>Qty</th>
              <th>Price</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            
            if($orders[0]->orderdetails){
              $orderdetails = json_decode($orders[0]->orderdetails);
              // echo "<pre>";
              //      print_r($orderdetails);
                    // exit;
              // $total_price =0;
              // $check = 0;
              // foreach($orderdetails as $orderdetail){
                foreach($orders as $order){

                // $pro = get_product_by_id($orderdetail->pro_id) ;
                // $restaurent = $this->MedicineModel->get_restaurent_by_id($orderdetail->res_id) ;
                
                  //  echo "<pre>";
                  //  print_r($pro);
                  //  print_r($restaurent);
                    // exit;
              
            
              ?>
            <tr>
            
          
              <td><?php echo $order->restaurant_name ; ?></td>
              <td><?php
              // if($pro){
              //   echo $pro[0]->name ;
              // }else{
              //   echo " ";
              // }
               echo $order->name ; 
               ?></td>
              <td><?php echo $order->size ; ?></td>
              <td><?php echo $order->qty ; ?></td>
              <td><?php echo $order->price ; ?></td>
              
            </tr>
            <?php }} ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table table-bordered">
              <tbody>
              <tr>
                <th style="width:50%">Total:</th>
                <td><?php 
                $totalAmount=0;
                foreach($orders as $order){
                $totalAmount = $totalAmount + $order->total_amount;
                $totalItemPrice = $order->qty*$order->price;
                }
                echo $this->lang->line('currency_symbol'); ?><?php echo $totalAmount ; ?></td>
              </tr>
              <tr>
                <th>Coupon Discount</th>
                <td> <?php
                $discountConvertToPrice= (($totalAmount) * $orders[0]->discount)/100;
                echo $orders[0]->discount ; ?> % ( - <?php echo $this->lang->line('currency_symbol'); echo $discountConvertToPrice; ?>)</td>
              </tr>
              <tr>
                <th style="width:50%">Delivery charge:</th>
                <td><?php echo $this->lang->line('currency_symbol'); ?><?php echo $orders[0]->delivery_charge ; ?></td>
              </tr>
              <tr>
                <th>Tax (0%)</th>
                <td><?php echo $this->lang->line('currency_symbol') ?> 0.00</td>
              </tr>
              
              <tr>
                <th><strong>Sub-Total:</strong></th>
                <td>
                  
                <?php 
                // $totalwithDeliveryCharge=0;
                // $discountConvertToPrice= 0;
                // $totalBill=($resProAmount[0]->total_amount);
                //   $discountConvertToPrice= ($totalBill * $couponDiscount)/100;
                //   $total = ($totalBill - $discountConvertToPrice)+($order->delivery_charge);
                //   echo $this->lang->line('currency_symbol').' '.($total);

                // $totalwithDeliveryCharge=$totalAmount+$order->delivery_charge;
                $discountConvertToPrice= (($totalAmount) * $orders[0]->discount)/100;

                $total = ($totalAmount - $discountConvertToPrice)+$order->delivery_charge;
                echo $this->lang->line('currency_symbol').' '.($total);
                
                // $this->user_model->convert_number($order->total_bill);
                
                ?>
              
              </td>
              </tr>
              <tr>
                <th><strong>Payment Method:</strong></th>
                <td><?php echo $orders[0]->payment_type ; ?></td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-sm-6">
          <a href="<?php echo site_url('admin/orders/printpreview/'.$order->id); ?>" target="_blank" class="btn btn-default"><?php echo $this->lang->line('print_button'); ?></a>
          
        </div>
        <div class="col-sm-6">
          <div class="pull-right">
             <?php if($this->session->userdata('current_user_type')=='admin'){ ?>
              <?php echo form_open('admin/orders/view/'.$order->id.'/'.$type); ?>
                  <div class="input-group input-group-sm">
                    <select name="status" id="" class="form-control select-custom">
                          <Option value="Placed"  <?php if($order->status=='Placed'){echo 'selected';} ?>>Placed</Option>
                          <Option value="Processing" <?php if($order->status=='Processing'){echo 'selected';} ?>>Processing</Option>
                          <Option value="Pickup" <?php if($order->status=='Pickup'){echo 'selected';} ?>>Pickup</Option>
                          <Option value="Delivered" <?php if($order->status=='Delivered'){echo 'selected';} ?>>Delivered</Option>
                      </select>
                    <span class="input-group-btn">
                      <button type="submit" name="change_status" value="submit" class="btn btn-primary btn-flat" style="margin-right: 5px;">
                        <i class="fa fa-refresh"></i> Save
                      </button>
                    </span>
                  </div>
              <?php echo form_close(); ?>
              <?php } ?>
              
             <?php if($this->session->userdata('current_user_type')!='admin' and $type!='delivered'){ ?>
              <?php echo form_open('admin/orders/view/'.$order->id.'/'.$type); ?>
                  <div class="input-group input-group-sm">
                    <select name="status" id="" class="form-control select-custom">
                          <Option value="Placed"  <?php if($order->status=='Placed'){echo 'selected';} ?>>Placed</Option>
                          <Option value="Processing" <?php if($order->status=='Processing'){echo 'selected';} ?>>Processing</Option>
                          <Option value="Pickup" <?php if($order->status=='Pickup'){echo 'selected';} ?>>Pickup</Option>
                          <Option value="Delivered" <?php if($order->status=='Delivered'){echo 'selected';} ?>>Delivered</Option>
                      </select>
                    <span class="input-group-btn">
                      <button type="submit" name="change_status" value="submit" class="btn btn-primary btn-flat" style="margin-right: 5px;">
                        <i class="fa fa-refresh"></i> Save
                      </button>
                    </span>
                  </div>
              <?php echo form_close(); ?>
              <?php } ?>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>