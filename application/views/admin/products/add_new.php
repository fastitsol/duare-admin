<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('admin/products/add_new'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_product'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('add_new_product'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                 <h3 class="box-title"><?php  echo $this->lang->line('add_new_product'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('admin/products/add_new/'); ?>
            <div class="box-body">
              <div class="col-sm-12">
              <div class="form-group">
                    <label>Shop name</label>
                    <select name="restaurant_id" class="form-control select2" style="width: 100%;">
                    <option value="0">Select shop</option>
                     <?php if($shopList){foreach($shopList as $shop){ 
                        $selected = null;
                        if($this->input->post('restaurant_id')==$shop->id){$selected='selected="selected"';}else{$selected = null;}
                        ?>
                      <option value="<?php echo $shop->id; ?>" <?php echo $selected; ?> > <?php echo $shop->name; ?> </option>
                      <?php }} ?>
                    </select>
                </div>
              <div class="form-group">
                    <label>Product Type</label>
                    <select name="product_type" class="form-control select2" style="width: 100%;" required>
                      <option value="food">Food</option>
                      <option value="medicine">Medicine</option>
                     
                    </select>
                </div>

                
                <div class="form-group">
                  <label for="food_type">Medicine Type</label>
                  <input type="text" name="food_type" class="form-control" id="food_type" placeholder=" Add medicine type if product type is medicine" value="<?php echo $this->input->post('food_type'); ?>">
                </div>
                <div class="form-group">
                  <label for="product_name"><?php echo $this->lang->line('product_name'); ?></label>
                  <input type="text" name="product_name" class="form-control" id="product_name" placeholder="<?php echo $this->lang->line('product_name'); ?>" value="<?php echo $this->input->post('product_name'); ?>" required>
                </div>
                <div class="form-group">
                  <label for="company_name">Company name</label>
                  <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" value="<?php echo $this->input->post('company_name'); ?>" required>
                </div>
                <div class="form-group">
                    <label>Sub category name</label>
                    <select name="subCategory_id" class="form-control select2" style="width: 100%;" required>
                    <option value="0">Select Subcategory if product type is food</option>
                     <?php if($subcategoryList){foreach($subcategoryList as $subCategory){ 
                        $selected = null;
                        if($this->input->post('subCategory_id')==$subCategory->id){$selected='selected="selected"';}else{$selected = null;}
                        ?>
                      <option value="<?php echo $subCategory->id; ?>" <?php echo $selected; ?> > <?php echo $subCategory->subCategory_name; ?> </option>
                      <?php }} ?>
                    </select>
                </div>
                <!-- <div class="form-group">
                    <label><?php echo $this->lang->line('food_type'); ?></label>
                    
                       <div class="checkbox">
                         <?php if($categories){foreach($categories as $category){    ?>
                          <label>
                            <input name="cat_id[]" value="<?php echo $category->id; ?>" type="checkbox"> <?php echo $category->name; ?> &nbsp;
                          </label>
                          <?php }} ?>
                        </div>
                    
                </div> -->
                <div class="form-group">
                  <label for="purchasePrice">Purchase Price</label>
                  <input type="number" min="0" step="0.01" name="purchasePrice" class="form-control" id="purchasePrice" placeholder="Purchase Price" value="<?php echo $this->input->post('purchasePrice'); ?>" required>
                </div>
                <div class="form-group">
                  <label for="price">Selling Price</label>
                  <input type="number" min="0" step="0.01" name="price" class="form-control" id="price" placeholder="Selling Price" value="<?php echo $this->input->post('price'); ?>" required>
                </div>
                <div class="form-group">
                  <label for="discount_percent">Discount</label>
                  <input type="number" min="0" step="0.01" name="discount_percent" class="form-control" id="discount_percent" placeholder="Discount" value="<?php echo $this->input->post('discount_percent'); ?>">
                </div>
               
                <div class="form-group">
                  <label for="size"><?php echo $this->lang->line('size'); ?></label>
                  <input type="text" name="size" class="form-control" id="size" placeholder="<?php echo $this->lang->line('size'); ?>" value="<?php echo $this->input->post('size'); ?>">
                </div>
                <div class="form-group">
                  <label for="medicine_mg_ml">Product Power</label>
                  <input type="text" name="medicine_mg_ml" class="form-control" id="medicine_mg_ml" placeholder="Add medicine power if product type is medicine" value="<?php echo $this->input->post('medicine_mg_ml'); ?>">
                </div>
                  
                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('description'); ?></label>
                    <textarea name="description" class="form-control" id="description" placeholder="<?php echo $this->lang->line('description'); ?>"><?php echo $this->input->post('description'); ?></textarea>
                </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('closing_time'); ?></label>

                      <div class="input-group">
                        <input name="opening_time" type="text" class="form-control timepicker">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>
                <div class="bootstrap-timepicker">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('opening_time'); ?></label>

                      <div class="input-group">
                        <input name="closing_time" type="text" class="form-control timepicker">
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>
                  
                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('availability'); ?></label>
                    <select name="availability" class="form-control select2" id="">
                        <option value="available">Available</option>
                        <option value="unavailable">Unavailable</option>
                    </select>
                </div>
                
                <div class="form-group">
                  <label for="product_image"><?php echo $this->lang->line('product_image'); ?></label>
                  <input style="margin:0;padding:0" type="file" name="product_image" class="form-control" id="size" placeholder="<?php echo $this->lang->line('product_image'); ?>">
                </div>
                
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_new_product" name="save_new_product" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>