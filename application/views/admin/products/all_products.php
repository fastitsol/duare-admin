<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>

<!-- <script>
$(document).ready(function(){
  $("#myInput").on("keyup",function(){
    var value = $($this).val().toLowerCase();
    $("#myTable tr").filter(function(){
      $(this).toggle($(this).text().toLowerCase().indexOf(value)> -1)
    });
  });
});
</script> -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('admin/products/add_new'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_product'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('product_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> <?php echo $this->lang->line('product_list'); ?> </h3>
              <div class="form-inline active-cyan-4 pull-right">
                <?= form_open('admin/products/search/');?>
                  <input class="form-control form-control-sm" name="query" type="search" placeholder="Search"
                    aria-label="Search" id="myInput" required>
                  <button class="btn btn-outline-success" type="submit">Search</button>
                <?= form_close();?>
                <?= form_error('query',"<p class='navbar-text'>",'</p');?>
              </div>
            </div>
            

            <!-- Search form -->
            <!-- <nav class="navbar navbar-light bg-light justify-content-between">
              <a class="navbar-brand">Navbar</a>
              <form class="form-inline">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
            </nav> -->
            
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
              <div class="table-responsive">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th><?php echo $this->lang->line('serial_number'); ?></th>
                  <th><?php echo $this->lang->line('product_image'); ?></th>
                  <th><?php echo $this->lang->line('product_name'); ?></th>
                  <th>Product Type</th>
                  <th>Company name</th>
                  <th>Sub category name</th>
                  <th><?php echo $this->lang->line('size'); ?></th>
                  <th>Medicine Power(mg/ml)</th>
                  <th>Medicine Type</th>
                  <th>Purchase Price</th>
                  <th>Selling Price</th>
                  <th>Discount</th>
                  <th>Discounted Price</th>
                  <th><?php echo $this->lang->line('description'); ?></th>
                  <th>Availability</th>
                  <th><?php echo $this->lang->line('change'); ?></th>
                </tr>
                <?php $counter=0; if($products): foreach($products as $product): $counter++; ?>
                <tr>
                  <td><?php echo $this->user_model->convert_number($counter); ?></td>
                  <td><img src="<?php echo site_url('uploads/products/'.$product->image); ?>" alt="Product Image" class="img-responsive img-thumbnail" style="max-width:100px"></td>
                  <td><?php echo $product->name; ?></td>
                  <td><?php echo $product->product_type; ?></td>
                  <td><?php echo $product->medicine_company; ?></td>
                  <td><?php echo $product->subCategory_name; ?></td>
                  <td><?php echo $product->size; ?></td>
                  <td><?php echo $product->medicine_mg_ml; ?></td>
                  <td><?php echo $product->food_type; ?></td>
                  <td><?php echo $product->purchasePrice; ?></td>
                  
                  <td><?php echo $this->user_model->convert_number(number_format($product->price, 2)); ?></td>
                  
                  <td>
                    <?php if($product->discount_percent) echo $product->discount_percent; ?>
                    <?php if(!$product->discount_percent) echo 0; ?> %
                    
                  </td>
                  <td>
                    
                    
                    
                    <?php echo $product->discount_price; ?>
                  </td>
                  
                  <td><?php echo $product->description; ?></td>
                  <td><?php echo $product->availability; ?></td>
                  <td>
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/products/edit_product/'.$product->id); ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a>
                        <a href="<?php echo site_url('admin/products/delete/'.$product->id); ?>" class="btn btn-danger" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>