


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo "Monthly Report"; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('order_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     

     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php 

                $monthTotal = 0;
                $monthTotalPurchasePrice = 0;
                echo "Monthly Report"; 

                ?> </h3>

                <div class="dropdown show">
                    <a class="btn btn-secondary dropdown-toggle select_res" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select Restaurant Name
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <ul class="_dropdown">
                        <?php 
                        
                        if($restaurants){foreach($restaurants as $restaurant){ 
                       
                        
                        ?>
                      
                            <li><a class="" href="<?php echo site_url('admin/Dashboard/monthlyReport/'.$restaurant->id); ?>"><?php echo $restaurant->name; ?></a></li>
                      <?php }} ?>
                        </ul>
                    </div>
                </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                <tbody>
                  <tr>
                  <th>#<?php echo $this->lang->line('order_id'); ?></th>
                  <th><?php echo $this->lang->line('date'); ?></th>
                  <th><?php echo $this->lang->line('customer_info'); ?></th>
                  <th><?php echo "Status"; ?></th>
                  <th><?php echo "View"; ?></th>
                  <th><?php echo $this->lang->line('total_bill'); ?></th>
                  <th><?php echo "Product Purchase Price"; ?></th>
                  <th><?php echo "Profit"; ?></th>

                </tr>
                <?php $counter=0; if($orders): foreach($orders as $order): $counter++; 
                    $shipping_info = json_decode($order->shiping_info);
                    $resProAmount = $this->restaurant_model->getProductAmount($order->invoice_id);
                    $resProAmountForProfit = $this->restaurant_model->getProductAmountForProfit($order->invoice_id);
                    
                    // $couponDiscount = $shipping_info->discount;
                    // $resProAmount = $this->restaurant_model->getProductAmount($order->invoice_id);
                    // echo "<pre>";
                    // print_r($resProAmount);
                    // return;
                    ?>
                <tr>
                  <td>#<?php echo $this->user_model->convert_number($order->invoice_id); ?></td>
                  <td><?php 

                  echo $this->user_model->convert_number(date('h:i A',strtotime($order->date_time)))."<br>";
                  echo $this->user_model->convert_number(date('d/m/Y',strtotime($order->date_time))); 

                  ?></td>
                  <td><?php 
                      $customer = $this->user_model->get_user($order->customer_id);
                      if($customer){ ?>
                          <b>Name:- </b> <?php echo $customer->user_name; ?> <br>
                          <b>Phone:- </b> <?php echo $customer->phone; ?> <br>
                          <b>Address:- </b> <?php echo $customer->address1; ?> <br>
                          
                        <?php } ?>
                  </td>
                  <td>
                    <?php 

                    if($order->status == 'Delivered'){

                      echo "<b>Completed</b>";

                    }
                     ?>
                      
                  </td>
                  <td style="width:80px">
                    <div class="btn-group">
                        <a href="<?php  //.'$order->id.'/placed
                        echo site_url('admin/orders/OrderView/'.$order->invoice_id); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye" style="font-size: 20px;"></i>
                      </a>

                    </div>
                  </td>

                  
                  <td align="center"><?php 

$couponDiscount = ($resProAmount[0]->total_amount * $order->discount)/100;
$invoiceTotal =($resProAmount[0]->total_amount - $couponDiscount)+$order->delivery_charge ;
// $invoiceTotal = $resProAmount[0]->total_amount ;

$monthTotal = $monthTotal + $invoiceTotal ;
echo $this->lang->line('currency_symbol').' '.$invoiceTotal;

                  // $invoiceTotal = $order->total_bill - $order->delivery_charge;

                  // $monthTotal = $monthTotal + $invoiceTotal ;
                  // echo $this->lang->line('currency_symbol').' '.$invoiceTotal; 

                  ?></td>
<td align="center"><?php 

$totalPurchasePrice = $resProAmountForProfit[0]->proAmount ;
// $invoiceTotal =($resProAmount[0]->total_amount - $couponDiscount)+$order->delivery_charge ;
// $invoiceTotal = $resProAmount[0]->total_amount ;

$monthTotalPurchasePrice = $monthTotalPurchasePrice + $totalPurchasePrice ;
echo $this->lang->line('currency_symbol').' '.$totalPurchasePrice;

                  

                  ?></td>
                  <td align="center"><?php 

// $totalPurchasePrice = $resProAmountForProfit[0]->proAmount ;
// $invoiceTotal =($resProAmount[0]->total_amount - $couponDiscount)+$order->delivery_charge ;
// $invoiceTotal = $resProAmount[0]->total_amount ;

$profit = $invoiceTotal - $totalPurchasePrice;
echo $this->lang->line('currency_symbol').' '.$profit;

                  

                  ?></td>

                </tr>
                <?php endforeach; ?>

                <tr>
                  
                  <td colspan="5" align="center">
                  <?php 

                      echo "<b>Month Total :  "."</b>"   ; 

                  ?>
                    
                  </td>
                  <td align="center"><?php 

                      echo "<b>".$this->lang->line('currency_symbol')." ".$monthTotal."</b>"   ; 

                  ?></td>

                </tr>

                    
              
              <?php endif; ?>
                
              </tbody>
              </table>


              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

<style>
._dropdown{}
._dropdown li a{
  color: #333;
}
._dropdown li{
  display: block;
    padding: 7px 15px;
}
.select_res{
  border: 1px solid #e7e7e7;
  border-radius: 3px;
}
</style>
