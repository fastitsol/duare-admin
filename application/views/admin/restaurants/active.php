
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-home"></i> <?php echo $this->lang->line('menu_restaurants'); ?> 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->lang->line('menu_restaurants'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('menu_restaurants'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12 table-responsive">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th><?php echo $this->lang->line('logo'); ?></th>
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th><?php echo $this->lang->line('phone'); ?></th>
                  <th><?php echo $this->lang->line('email'); ?></th>
                  <th><?php echo $this->lang->line('address'); ?></th>
                  <th><?php echo $this->lang->line('website'); ?></th>
                  <th><?php echo $this->lang->line('cover'); ?></th>
                  <th><?php echo $this->lang->line('delivery_time'); ?></th>
                  <th><?php echo $this->lang->line('opening_time'); ?></th>
                  <th><?php echo $this->lang->line('closing_time'); ?></th>
                  <th><?php echo $this->lang->line('availability'); ?></th>
                  <th><?php echo $this->lang->line('trade_licence_image'); ?></th>
                  <th><?php echo $this->lang->line('status'); ?></th>
                  <th><?php echo $this->lang->line('change'); ?></th>
                </tr>
                <?php if($restaurants): foreach($restaurants as $restaurant): ?>
                <tr>
                  <td style="max-width:100px">
                  <?php if($restaurant->logo){ ?>
                  <a target="_blank" href="<?php echo site_url('uploads/restaurant/'.$restaurant->logo); ?>"><img src="<?php echo site_url('uploads/restaurant/'.$restaurant->logo); ?>" alt="" class="img-responsive" style="width:80px"></a>
                  <?php } ?>
                  </td>
                  <td><a href="#"><?php echo $restaurant->name; ?></a></td>
                  <td><?php echo $restaurant->phone; ?></td>
                  <td><?php echo $restaurant->email; ?></td>
                  <td><?php echo $restaurant->address; ?></td>
                  <td><?php echo $restaurant->website; ?></td>
                  <td>
                  
                  <?php if($restaurant->cover){ ?>
                      <a target="_blank" class="btn btn-info" href="<?php echo site_url('uploads/restaurant/'.$restaurant->cover); ?>"> <i class="fa fa-image"></i> </a>
                  <?php } ?>
                  </td>
                  <td><?php echo $restaurant->delivery_time; ?></td>
                  <td><?php echo $restaurant->opening_time; ?></td>
                  <td><?php echo $restaurant->closing_time; ?></td>
                  <td><?php echo $restaurant->availability; ?></td>
                  <td>
                  <?php if($restaurant->trade_licence_image){ ?>
                      <a target="_blank" class="btn btn-info" href="<?php echo site_url('uploads/restaurant/'.$restaurant->trade_licence_image); ?>"> <i class="fa fa-image"></i> </a>
                  <?php } ?>
                  </td>
                  <td>
                   <a href="<?php echo site_url('admin/restaurants/change_status/'.$restaurant->id.'/inactive'); ?>" class="btn btn-danger btn-xs"><?php echo $restaurant->status; ?> To Inactive</a>
                  </td>
                  <td style="width:80px">
                    <div class="btn-group">
                       
                        <a href="<?php echo site_url('admin/restaurant/edit_restaurant/'.$restaurant->id); ?>" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                        
                        <a target="_blank" href="<?php echo site_url('admin/users/edit_user/'.$this->restaurant_model->get_user_id($restaurant->id)); ?>" class="btn btn-info btn-xs" ><i class="fa fa-user"></i></a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
