<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('admin/users/add_new'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_user'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->lang->line('add_new_user'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('add_new_user'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
			<div class="col-sm-8">
			    <assaid class="customer_profile_info">
					<?php echo form_open('admin/users/add_new'); ?>
						<div class="form-group row">
							<label for="first_name" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('first_name'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="first_name" placeholder="<?php echo  $this->lang->line('first_name'); ?>" name="first_name" type="text" value="<?php echo $this->input->post('first_name'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="last_name" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('last_name'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="last_name" placeholder="<?php echo  $this->lang->line('last_name'); ?>" name="last_name" type="text" value="<?php echo $this->input->post('last_name'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="user_name" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('user_name'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="user_name" placeholder="<?php echo  $this->lang->line('user_name'); ?>" name="user_name" type="text" value="<?php echo $this->input->post('user_name'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="email" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('email'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="email" placeholder="<?php echo  $this->lang->line('email'); ?>" name="email" type="email" value="<?php echo $this->input->post('email'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('type'); ?></label>
							<div class="col-sm-9">
							    <select name="type" id="type" class="form-control" required>
							        <option value="">Select type....</option>
							        
							        <option value="deliver_man" <?php if($this->input->post('type') and $this->input->post('type')=='deliver_man'){echo 'selected';} ?> > Deliver Man</option>
							        
							        
							        <option value="admin" <?php if($this->input->post('type') and $this->input->post('type')=='admin'){echo 'selected';} ?> >Admin</option>
							    </select>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="password" class="col-sm-3 col-form-label"><?php echo  $this->lang->line('password'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="password" placeholder="<?php echo  $this->lang->line('password'); ?>" name="password" type="password" value="<?php echo $this->input->post('password'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="sign_up" class="col-sm-3 col-form-label">        </label>
							<div class="col-sm-9">
							    <input name="sign_up" value="<?php echo  $this->lang->line('save_button'); ?>" class="btn btn-primary btn-reservation" type="submit">
							</div>
						</div>
						
					<?php echo form_close(); ?>
			    </assaid>
			</div>
           
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>