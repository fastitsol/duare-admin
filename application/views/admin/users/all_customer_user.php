
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('admin/users/add_new'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_new_user'); ?> </a>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">All Users</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">All Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12 table-responsive">
                  <table class="table table-bordered">
                <tbody><tr>
                  <th>Image</th>
                  <th>First Name</th>
                  <th>User Name</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>City</th>
                  <th>State</th>
                  <th>Birthday</th>
                  <th>Referal Id</th>
                  <th>Team</th>
                  <th>Status</th>
                  <th>Joined</th>
                  <th><?php echo $this->lang->line('change'); ?></th>
               
                </tr>
                <?php if($users): foreach($users as $user): ?>
                <tr>
                  <td style="max-width:100px">
                  <?php if($user->image){ ?>
                  <a target="_blank" href="<?php echo site_url('uploads/users/'.$user->image); ?>"><img src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-responsive" style="width:80px"></a>
                  <?php } ?>
                  </td>
                  <td><a href="#"><?php echo $user->first_name; ?></a></td>
                  <td><?php echo $user->user_name; ?></td>
                  <td><?php echo $user->address1; ?></td>
                  <td><?php echo $user->phone; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <!-- <td>
                  
                  <?php// if($restaurant->cover){ ?>
                      <a target="_blank" class="btn btn-info" href="<?php echo site_url('uploads/restaurant/'.$restaurant->cover); ?>"> <i class="fa fa-image"></i> </a>
                  <?php// } ?>
                  </td> -->
                  <td><?php echo $user->city; ?></td>
                  <td><?php echo $user->state; ?></td>
                  <td><?php echo $user->birthday; ?></td>
                  <td><?php echo $user->referal_id; ?></td>
                  <td>
                  <?php echo $user->team; ?>
                  </td>
                  <td>
                  <?php echo $user->status; ?>
                  </td>
                  <td>
                  <?php echo $user->joined; ?>
                  </td>
                  <!-- <td>
                   <a href="<?php //echo site_url('admin/restaurants/change_status/'.$restaurant->id.'/inactive'); ?>" class="btn btn-danger btn-xs"><?php //echo $restaurant->status; ?> To Inactive</a>
                  </td> -->
                  <td style="width:80px">
                    <div class="btn-group">
                       
                        <!-- <a href="<?php //echo site_url('admin/restaurant/edit_restaurant/'.$user->id); ?>" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o"></i></a> -->
                       
                        <a target="_blank" href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>" class="btn btn-info btn-xs" ><i class="fa fa-user"></i></a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
