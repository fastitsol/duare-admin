


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo "Yearly Report"; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('order_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     

     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php 

                $yearTotal = 0;
                echo "Yearly Report"; 

                ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                <tbody>
                  <tr>
                  <th>#<?php echo $this->lang->line('order_id'); ?></th>
                  <th><?php echo $this->lang->line('date'); ?></th>
                  <th><?php echo $this->lang->line('customer_info'); ?></th>
                  <th><?php echo "Status"; ?></th>
                  <th><?php echo "View"; ?></th>
                  <th><?php echo $this->lang->line('total_bill'); ?></th>

                </tr>
                <?php $counter=0; if($orders): foreach($orders as $order): $counter++; 
                    $shipping_info = json_decode($order->shiping_info);
                    $resProAmount = $this->restaurant_model->getResProductAmount($order->invoice_id,$order->restaurant_id);
                    
                    // echo "<pre>";
                    // print_r($shipping_info);
                    ?>
                <tr>
                  <td>#<?php echo $this->user_model->convert_number($order->invoice_id); ?></td>
                  <td><?php 

                  echo $this->user_model->convert_number(date('h:i A',strtotime($order->date_time)))."<br>";
                  echo $this->user_model->convert_number(date('d/m/Y',strtotime($order->date_time))); 

                  ?></td>
                  <td><?php 
                      $customer = $this->user_model->get_user($order->customer_id);
                      if($customer){ ?>
                          <b>Name:- </b> <?php echo $customer->first_name.' '.$customer->last_name; ?> <br>
                          <b>Phone:- </b> <?php echo $customer->phone; ?> <br>
                          <b>Address:- </b> <?php echo $customer->address1; ?> <br>
                          
                        <?php } ?>
                  </td>
                  <td>
                    <?php 

                    if($order->status == 'Delivered'){

                      echo "<b>Delivered</b>";

                    }
                     ?>
                      
                  </td>
                 
                  
                  
                  <td style="width:100px" align="center">
                    <div class="btn-group">
                        <a href="<?php  
                        echo site_url('restaurant/orders/restProcessingOrderView/'.$order->invoice_id); ?>" class="btn btn-success btn-sm"><i class="fa fa-eye" style="font-size: 20px;"></i>
                      </a>

                    </div>
                  </td>

                  <td align="center"><?php 

                  // $invoiceTotal = $order->total_bill - $order->delivery_charge;
                  // $invoiceTotal = $resProAmount[0]->total_amount + $order->delivery_charge;
                  $invoiceTotal = $resProAmount[0]->total_amount ;

                  $yearTotal = $yearTotal + $invoiceTotal;
                  echo $this->lang->line('currency_symbol').' '.$invoiceTotal; 

                  ?></td>

                </tr>
                <?php endforeach; ?>

                <tr>
                  <td colspan="5" align="center">
                  <?php 

                      echo "<b>Year Total :  "."</b>"   ; 

                  ?>
                    
                  </td>
                  
                  <td align="center"><?php 

                      echo "<b>".$this->lang->line('currency_symbol')." ".$yearTotal."</b>"   ; 

                  ?></td>

                </tr>

              <?php endif; ?>
                
              </tbody>
              </table>


              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
