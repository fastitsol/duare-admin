<head>


<style>
#big_card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-width: 700px;
  margin: auto;
  text-align: center;
  font-family: arial;
  padding-bottom: 20px;
}




#received_restaurant {
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #0D8EDF;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#processing_relivery {
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #DAA90B ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#delivered_users{
  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #249320 ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#cancelled_earnings{

  margin-top: 10px;
  border: none;
  outline: 0;
  color: white;
  background-color: #BB0A0A ;
  padding-left: 60px;
  padding-right: 60px;
  height: 100px;
  text-align: left;
  cursor: pointer;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
  width: 250px;
  font-size: 18px;
}

#big_card label {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}


</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>



<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));

 // echo "<pre>";
 // print_r(date("Y-12-30 00:00:00"));
 // exit;

$total_message_count = 0;


//$user_data->type == 'pharmacy'


//Total mother account income
$total_mother_account_balance = 0;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <?php echo $this->lang->line('menu_dashboard'); ?>
        <small>  <?php echo $this->lang->line('menu_control_panel'); ?> </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/Dashboard/'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?></a></li>
        <li class="active"><?php echo $this->lang->line('menu_dashboard'); ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
    















       <div class="row" style="background-color: #E1DEE3 ; border-color: gray; padding-top: 10px; padding-bottom: 10px;">

        <div class="col-md-6">
          <div style="width: 100%; height: 100%;" >
            <div class="col-md-3" >
             <img style="height: 100%; width: 100%;" src="

             <?php 
              if ($restaurant_data->logo != Null) {
                      echo site_url('uploads/restaurant/'.$restaurant_data->logo); 
                    }else{
                         echo site_url('uploads/users/'.$user_data->image); 
                    }

             ?>

             " class="img-circle" alt="User Image">
             </div>


             <div class="col-md-4"  align="center">
                <p style="margin-top: 10px; font-size: 20px; font-family: Times New Roman, Times, serif; ">
                  <?php
                    if ($restaurant_data->name != Null) {
                      echo $restaurant_data->name;
                    }else{
                         echo "No Restaurant";
                    }
                  ?>
                  


                </p>
                <p style="margin-top: -10px; font-family: Times New Roman, Times, serif; font-size: 12px;">

                  <?php
                    if ($restaurant_data->address != Null) {
                      echo $restaurant_data->address;
                    }else{
                         echo "No address";
                    }
                  ?>
                </p>

             </div>
          </div>
        </div>

        <div class="col-md-6"  >

           <div style="margin-left: 300px; margin-top: 35px; " >


            <!-- on/off button -->
           

            
              
                
                <?php
              

                if($restaurant_data->availability=="available")
                { 

                  ?>
                  <a style="padding: 6px;" href="<?php echo site_url('restaurant/dashboard/play_push/unavailable'); ?>">
                  <abbr title="<?php echo $restaurant_data->availability;?>">
                      <button style="margin-bottom: 20px; font-family: Times New Roman, Times, serif;" type="button" class="btn btn-success">Open</button>
                    </abbr>
                  </a>
                <?php 
              }else{
                ?>
                     <a style="padding: 6px;"  href="<?php echo site_url('restaurant/dashboard/play_push/available'); ?>"> 
                      <abbr title="<?php echo $restaurant_data->availability;?>">
                  <button style="margin-bottom: 20px; font-family: Times New Roman, Times, serif;" type="button" class="btn btn-danger">Close</button>
                  </abbr>
                </a>
                
              <?php 
            }
            ?>  
                
              
           

            <!-- on/off button -->


            
              <a style="padding: 6px;"  href="<?php echo site_url('restaurant/users/edit_user/'); ?>" ><abbr title="Edit Profile"><i class="fa fa-edit" style="font-size:34px; color: gray;"></i></abbr></a>
            


            
              <a style="padding: 6px;"  href="<?php echo site_url('logout'); ?>" ><abbr title="Logout"><i class="fa fa-sign-out" aria-hidden="true"  style="font-size:36px; color: red;"></i></abbr></a>
           
          </div>
           
        </div>
      </div>



     <!-- list of order, pending processing and others -->



      <div class="row">

        <div class="col-md-4" style="background-color: white; border:gray; padding-bottom:15px;" align="center">
            <div style="padding-top:40px;  ">
              <p style="color: #0B4EEB; font-family: Times New Roman, Times, serif; "><b>Today's Order Status</b></p>
            </div>

            <div class="col-md-4" align="center" style="padding-top:15px; width: 100%; height: 100%;  padding-bottom: 15px;">

          <a href="<?php 

            if($user_data->type == 'pharmacy'){

                echo base_url("Medicine/OrderMedicine/DeliveredOrderMedicine");

            }else{
                echo base_url("restaurant/orders/delivered");
            }

          ?>">
              <div class="col-md-2" align="center" style="width: 33%; padding:20px;">         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif; font-size: 30px; color: blue;"><b>

                  <?php
                     if($order_delivered!=Null){
                         echo $order_delivered;
                     }else{
                        $order_delivered = 0;
                       echo $order_delivered;
                     }
                  ?> 

                  </b>
                  </p>
                  <p style="color: blue; font-family: Times New Roman, Times, serif;"><b>Completed</b></p>
              </div>
           </a>
             
             <a href="<?php 
             if($user_data->type == 'pharmacy'){

                echo base_url("Medicine/OrderMedicine/newOrderMedicine");

            }else{
             echo base_url("restaurant/orders/placed");

           }

             ?>">
              <div class="col-md-2" align="center" style=" width: 33%; padding:20px;" >         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif; font-size: 30px; color: gray;"><b>

                  <?php
                     if($order_received!=null){
                         echo $order_received;
                     }else{
                       $order_received = 0;
                          echo $order_received;
                     }
                  ?>

                  </b></p>
                  <p style="color: gray; font-family: Times New Roman, Times, serif;"><b>Pending</b></p>
              </div>

              </a>
                
            
              <a href="<?php 

              if($user_data->type == 'pharmacy'){

                  echo base_url("Medicine/OrderMedicine/ProcessingOrderMedicine");

               }else{
                   echo base_url("restaurant/orders/processing");

              }

              ?>">
              <div class="col-md-2" align="center" style=" width: 33%; padding:20px;" >         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif; font-size: 30px; color: gray;"><b>

                 <?php
                     if($order_processing!=Null){
                         echo $order_processing;
                     }else{
                       $order_processing = 0;
                       echo $order_processing;
                     }
                  ?>


                  </b></p>
                  <p style="color: gray;  font-family: Times New Roman, Times, serif;"><b>Processing</b></p>
              </div>
              </a>
              
            </div>
        </div>




        <div class="col-md-4" style="background-color: white; border:gray; padding-bottom:20px; border: 1px solid #E4E7E8;" align="center">
            <div style="padding-top:40px;  ">
              <p style="color: #A10892 ; font-family: Times New Roman, Times, serif; "><b>Total Product's</b></p>
            </div>

            <div class="col-md-4" align="center" style="padding-top:15px; width: 100%; height: 100%;  padding-bottom: 10px;">

             <a href="<?php 

             if($user_data->type == 'pharmacy'){

                  echo base_url("Medicine/MedicineController/");

               }else{
             echo base_url('restaurant/products/all_product');

           }

             ?>">
              <div class="col-md-2" align="center" style="width: 50%; padding:20px;">         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif; font-size: 30px; color: #A10892;">
                    <b>

                    <?php
                     if($total_products!=Null){
                         echo $total_products;
                     }else{
                        $total_products = 0;
                       echo $total_products;
                     }
                  ?> 


                  </b>

                  </p>
                  <p style="color: #A10892; font-family: Times New Roman, Times, serif;"><b>Total Products</p>
              </div>
            </a> 

              <div class="col-md-2" align="center" style=" width: 50%; padding:20px;" >         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif; font-size: 30px; color: gray;">

                 <?php
                     if($total_available_products!=Null){
                         echo $total_available_products;
                     }else{
                        $total_available_products = 0;
                       echo $total_available_products;
                     }





                  ?>

                </p>
                  <p><b style="color: gray; font-family: Times New Roman, Times, serif;">Available Products</b></p>
              </div>
              
            </div>
        </div>




        <div class="col-md-4" style="background-color: white; border:gray; padding-bottom:15px;" align="center">
            <div style="padding-top:40px;  ">
              <p style="color: #34DA12 ; font-family: Times New Roman, Times, serif; "><b>Total Sales</b></p>
            </div>

            <div class="col-md-4" align="center" style="padding-top:15px; width: 100%; height: 100%;  padding-bottom: 15px;">

            <a href="<?php 

             if($user_data->type == 'pharmacy'){

                  echo base_url("Medicine/OrderMedicine/MonthlyReport");

               }else{
              echo base_url("restaurant/Dashboard/ResMonthlyReport");

           }

             ?>"> 
              <div class="col-md-2" align="center" style="width: 50%; padding:20px;">  

               
                <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif;  font-size: 30px; color: #34DA12;">
                 
                    <?php

                    if($thisMonthIncomes[0]->monthTotal == null){
                      echo '0' ;
                    }else{
                      echo $thisMonthIncomes[0]->monthTotal;
                    }
                     

                   ?>
                   
                    <b style="font-size: 16px;">Tk</b></p>
                  <p style="color: #34DA12; font-family: Times New Roman, Times, serif; "><b>This Month</b></p>
              </div>
            </a>


              <a href="<?php 

             if($user_data->type == 'pharmacy'){

                  echo base_url("Medicine/OrderMedicine/YearReport");

               }else{
              echo base_url("restaurant/Dashboard/ResYearReport");

           }

             ?>">
              <div class="col-md-2" align="center" style=" width: 50%; padding:20px;" >         
                  <p style="width: 100%; height: 100%; font-family: Times New Roman, Times, serif;  font-size: 30px; color: gray;">

                
                  <?php

                  if($lastYearIncomes[0]->total == null){
                    echo '0' ;
                  }else{
                    echo $lastYearIncomes[0]->total;
                  }
                   

                   ?>

                <b style="font-size: 16px;">Tk</b></p>
                  <p style="color: gray; font-family: Times New Roman, Times, serif; "><b>This Year</b></p>
              </div>
            </a>
              
            </div>
        </div>

      </div>







     <!-- <div class="row" style="margin-top: 50px;"> -->

        <!-- <div class="col-md-6" style=" padding-bottom: 100px;"> -->
          <!-- small box -->
          <!-- <div class="small-box bg-aqua" style=" padding-bottom: 100px;"> -->
            <!-- <div class="inner" style=" padding-bottom: 50px;"> -->
              <!-- <h4><?php //if($restaurant_data){//echo $restaurant_data->name;} ?></h4> -->
              
              <?php //if($restaurant_data->availability=="available"){ ?>
              <!-- <a href="<?php //echo site_url('restaurant/dashboard/play_push/unavailable'); ?>" class="btn btn-app btn-danger"> -->
                <!-- <i class="fa fa-pause"></i> Unavailable -->
              <!-- </a> -->
              <?php //} ?>
              
              <?php //if($restaurant_data->availability=="unavailable"){ ?>
              <!-- <a href="<?php// echo site_url('restaurant/dashboard/play_push/available'); ?>" class="btn btn-app btn-success">
                <i class="fa fa-play"></i> Available
              </a
              <?php //} ?>
              
            <!- </div>
            <div class="icon" style=" padding-bottom: 100px;">
              <i class="ion ion-bag" style=" padding-bottom: 100px;"></i>
            </div>
            
          </div>
        </div> -->
      
      
                <?php
                     if($order_received!=null){
                        // echo $order_received;
                     }else{
                       $order_received = 0;
                         // echo $order_received;
                     }
                  ?>
                <?php
                     if($order_processing!=Null){
                        // echo $order_processing;
                     }else{
                       $order_processing = 0;
                       //echo $order_processing;
                     }
                  ?>

                <?php
                     if($order_delivered!=Null){
                         //echo $order_delivered;
                     }else{
                        $order_delivered = 0;
                       //echo $order_delivered;
                     }
                  ?>
                <?php
                     
                $order_Cancelled = 0;
                     // echo $order_Cancelled;
                  ?>

    
   <!-- </div> -->

     












      
    </section>
    <!-- /.content -->

<p style="font-size: 34px; color: red;" id="demo"></p>

    <section class="content">

       <div class="row" style="margin-top: 20px;">

          <div class="col-md-6">
            <div class="card" id="big_card" >
              
              <p align="left" ><label><i class="fa fa-area-chart" style="font-size:24px"></i> &nbsp; &nbsp; Products Sales</label></p>
              
              
              <div id="chartContainer1" style="height: 350px; width: 100%;"></div>
              <script src="<?php echo base_url("assets/admin/bootstrap/js/canvasjs.min.js");?>"></script>
                

                </div>

             

              </div>

          <div class="col-md-6">
            <div class="card" id="big_card" >
              
              <p align="left" ><label><i class="fa fa-pie-chart" aria-hidden="true"></i> &nbsp; &nbsp;Order</label></p>
              
              

                
                  <div id="chartContainer" style="height: 350px; max-width: 600px;"></div>
                  <script src="<?php echo base_url("assets/admin/bootstrap/js/canvasjs.min.js");?>"></script>
              </div>

             

            </div>



      </div>

    </section>
  </div>













<script>
var  recived_order = parseInt('<?php echo $order_received; ?>');

var  processing_order = parseInt('<?php echo $order_processing; ?>');

var  dalivary_order = parseInt('<?php echo $order_delivered; ?>');

var  cancelled_order = parseInt('<?php echo $order_Cancelled; ?>'); 

</script>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  title:{
    text: "",
    horizontalAlign: "left"
  },


  data: [{
    type: "doughnut",
    startAngle: 60,
    //innerRadius: 60,
    indexLabelFontSize: 9,
    indexLabel: "{label} - #percent%",
    toolTipContent: "<b>{label}:</b> {y} (#percent%)",
    dataPoints: [
      { y: recived_order, label: "Received" },
      { y: cancelled_order, label: "Cancelled" },
      { y: dalivary_order, label: "Delivered" },
      { y: processing_order, label: "Processing"}
    ]
  }]
});
chart.render();



 var chart = new CanvasJS.Chart("chartContainer1", {
   animationEnabled: true,
   title:{
     text: ""
   },
   axisX:{
     valueFormatString: "DD, MMM ,YYYY"
   },
   axisY: {
     title: "Number of Order",
     includeZero: false,
     scaleBreaks: {
       autoCalculate: true
     }
   },
   data: [{
     type: "line",
     xValueFormatString: "DD, MMM ,YYYY",
     color: "#F08080",
     dataPoints: [
        <?php if($all_order_javas){

             foreach ($all_order_javas as $all_order_java) {
                 echo '{ x: new Date("'.$all_order_java->date_all.'"), y: '.$all_order_java->number.'},';
             }
         }else{
          ?>
          { x: new Date(2019, 0, 1), y: 0 },
          

          <?php

         }
         ?>

       
   
     ]
   }]
 });
 chart.render();

}

</script>
