



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

    	<h3 style="font-size: 14px;">Update / Give Discount </h3>
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php //echo $this->lang->line('change'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            	  <?php 
            	if($products){


            	
            ?>


			<div class="col-sm-12">
			    <assaid class="customer_profile_info">
					<?php echo form_open('restaurant/Dashboard/update_discount/'); ?>

					<div style="max-width:100px">
				<?php if($products[0]->image){ ?>
                  <a target="_blank" href="<?php echo site_url('uploads/products/'.$products[0]->image); ?>"><img src="<?php echo site_url('uploads/products/'.$products[0]->image); ?>" alt="" class="img-responsive" style="width:80px"></a>
                  <?php } ?>
                  </div>

						<div class="form-group row">
							<label for="first_name" class="col-sm-3 col-form-label"><?php echo "Product : "; ?></label>
							<div class="col-sm-9">
								<label for="last_name" class="col-sm-3 col-form-label"><?php echo $products[0]->name; ?></label>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="last_name" class="col-sm-3 col-form-label"><?php echo "Product Type : "; ?></label>
							<div class="col-sm-9">
								<label for="last_name" class="col-sm-3 col-form-label"><?php echo $products[0]->product_type; ?></label>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="inputError" class="col-sm-3 col-form-label"><?php  echo "Product Price : "; ?></label>
							<div class="col-sm-9">
								<label for="last_name" class="col-sm-3 col-form-label"><?php echo $products[0]->price; ?></label>

								<input class="form-control" id="type" placeholder="0%" name="real_price" type="hidden" value="<?php echo $products[0]->price;?>"  required>

								<input class="form-control" id="type" placeholder="0%" name="product_id" type="hidden" value="<?php echo $products[0]->id;?>"  required>

							</div>
						</div>
					
						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label"><?php echo "Discount Price : "; ?></label>
							<div class="col-sm-9">
								<label for="last_name" class="col-sm-3 col-form-label"><?php echo $products[0]->discount_price; ?></label>
							</div>
						</div>

						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label"><?php echo "Give Discount in Percentage :"; ?></label>
							<div class="col-sm-9">
								
								<input class="form-control" id="type" placeholder="0" name="discount" type="number"  required>
							
							</div>
						</div>
					
						
						
					
						<div class="form-group row">
							<label for="df" class="col-sm-3 col-form-label">  </label>
							<div class="col-sm-9">
								<input type="submit" name="save_user" value="<?php echo "Update Discount"; ?>" class="btn btn-danger btn-reservation" />
								
							</div>
						</div>
                    
					<?php echo form_close(); ?>
			    </assaid>
			</div>
           

           <?php 

           		}
           ?>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
