
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-home"></i> Products List 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo "Products List "; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     

     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><b><?php echo "Products List"; ?></b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="col-sm-12 table-responsive">
                  <table class="table table-bordered">
                    <?php
                      if($this->session->flashdata('error_discount')){
                      ?>
                      <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('error_discount');?>
                        
                      </div>
                      <?php
                      }
                  ?>

                  <?php
                      if($this->session->flashdata('discount_done')){
                      ?>
                      <div class="alert alert-success">
                        <?php echo $this->session->flashdata('discount_done');?>
                        
                      </div>
                      <?php
                      }
                  ?>


                    
                    <?php 
                if($products){
                  ?>
                <tbody><tr>

                  <th>Product Image</th>
                  <th>Product Name</th>
                  <th>product type</th>
                  <th>Food type</th>
                  <th>Price</th>
                  <th>Discount Price</th>
                  <th>Discount %</th>
                  <th><?php echo $this->lang->line('opening_time'); ?></th>
                  <th><?php echo $this->lang->line('closing_time'); ?></th>
                  <th>availability</th>


                  <!-- <th><?php //echo $this->lang->line('status'); ?></th> -->
               
                </tr>
                <?php 
                
                   foreach($products as $product): ?>
                <tr>
                  <td style="max-width:100px">
                  <?php if($product->image){ ?>
                  <a target="_blank" href="<?php echo site_url('uploads/products/'.$product->image); ?>"><img src="<?php echo site_url('uploads/products/'.$product->image); ?>" alt="" class="img-responsive" style="width:80px"></a>
                  <?php } ?>
                  </td>

                  <td><a href="<?php echo base_url('restaurant/Dashboard/discount_edit/'.$product->id);?>">
                    <?php echo $product->name; ?></a></td>
                  
                  <td><?php echo $product->product_type; ?></td>
                  <td><?php echo $product->food_type; ?></td>
                  
                  <td><?php echo $product->price; ?></td>
                  <td><?php echo $product->discount_price; ?></td>
                  <td><?php echo $product->discount_percent; ?></td>
                  
                  <td><?php echo $product->opening_time; ?></td>
                  <td><?php echo $product->closing_time; ?></td>
                  <td><?php echo $product->availability; ?></td>
                  
                  <!-- <td>
                   <a href="<?php //echo site_url('admin/restaurants/change_status/'.$restaurant->id.'/inactive'); ?>" class="btn btn-danger btn-xs"><?php //echo $restaurant->status; ?> To Inactive</a>
                  </td> -->
                  
                </tr>
                <?php endforeach; 
              }
              else{
                echo "No Product";
              }
                 ?>
                
              </tbody>
              </table>
              <?php echo $links; ?>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
