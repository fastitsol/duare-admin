


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="margin-left: 10px;">
      <h1>
        <?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php //echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i>
         <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Order View"; ?> </li>
      </ol>
    </section>
    
    <section class="invoice">
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     
     
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-home"></i> <?php echo $this->user_model->get_setting_data('site_title'); ?>
            <strong>
            <small class="pull-right"><?php echo "Order Date :"; ?>
             <?php 
                echo $this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)));
             ?>
             </small>
            </strong>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row ">

        <div class="col-sm-6 invoice-col" style="margin-bottom: 20px;">
          <b><?php echo "Order No."; ?> #<?php echo $orders[0]->id; ?></b><br>
          <br>
          <b><?php echo $this->lang->line('date'); ?>:</b>
           <?php echo "<b>".$this->user_model->convert_number(date('d/m/Y h:i A',strtotime($orders[0]->date_time)))."</b>"; ?>
           <br>
          <!-- <b><?php echo $this->lang->line('total_bill'); ?>:</b> 
          <?php echo "<b>".$this->lang->line('currency_symbol').' '.$this->user_model->convert_number($orders[0]->total_bill)."</b>";
           ?> -->
          <br>

           <b><?php echo "Payment Method : "; ?></b> 
          <?php 

          if($orders[0]->payment_type==null){
            echo "<b>N/A</b>";
          }else{
            echo "<b>".$orders[0]->payment_type."</b>";
          }
          ?>

        </div>


        <div class="col-sm-6 ">
          <h3 style="margin-top: -10px;"><u><?php echo "Coustomer Information"; ?></u></h3>
          <?php $customer = $this->user_model->get_user($orders[0]->customer_id); 
          if($customer){
          ?>
            <strong>
            <?php echo 'Name : '.$customer->user_name; ?><br> 
            <?php echo 'Address : '.$customer->address1; ?><br>
            Phone: <?php echo $customer->phone; ?><br>
            Email: <?php echo $customer->email; ?><br>
            </strong>
          <?php
          } 
          if ($orders[0]->shiping_info) {
            $shipping_info = json_decode($orders[0]->shiping_info);
          ?>

          <b><h5>Shipping Info : </h5></b>
          <?php if($shipping_info){echo 'Shipping Phone:-'.$customer->phone.'<br>';} ?>
          <?php if($shipping_info){echo 'Shipping Address:-'.$shipping_info->address;} ?>
          <?php
          }
          ?>
        </div>
        <!-- /.col -->

      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row" style="margin-top: 10px;">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Name</th>
              <th>Qty</th>
              <th>Size</th>
              <th>Price (pre)</th>
              <th>Total Price</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            
            $totalAmount=0;
            if($orders[0]->orderdetails){
              $orderdetails = json_decode($orders[0]->orderdetails);

              $total_price =0;
              $check = 0;
              foreach($orders as $order){
            //  if($orders[0]->orderdetails){
            //   $orderdetails = json_decode($orders[0]->orderdetails);
            //   $total_price =0;
            //   $check = 0;
            //   foreach($orderdetails as $orderdetail){

            //     $pro = get_product_by_id($orderdetail->pro_id) ;

              ?>
            <tr>
            <td><?php echo $order->name ; ?></td>
              <td><?php echo $order->qty ; ?></td>
              <td><?php 
              if ($order->size != 'nan') {
                echo $order->size;
              }else{
                echo "N/A";
              }
              ?></td>

            <td>
            <?php echo $order->price ; ?>
            </td>

            <td>
             <?php 

              // $totalItemPrice = $orderdetail->p_quantity*$pro[0]->price;
              $totalAmount = $totalAmount + $order->total_amount;
              $totalItemPrice = $order->qty*$order->price;

              echo $this->lang->line('currency_symbol').' '.$totalItemPrice; 
             ?></td>
              
            </tr>
            <?php } ?>




             <!-- <tr>

              <td colspan="4" align="center" style="font-size: 14px; font-style: bold;">
               
               <b> Delivery Charge </b>
              </td>

              <td  style="font-size: 14px; font-style: bold;">
               
               <b><?php echo "(+)".$this->lang->line('currency_symbol').' '.$orders[0]->delivery_charge; ?> </b>
              </td>
              
            </tr> -->


            <!-- <tr>

              <td colspan="4" align="center" style="font-size: 14px; font-style: bold;">
               
               <b> Discount </b>
              </td>

              <td  style="font-size: 14px; font-style: bold;">
               
               <b><?php echo "(-)".$this->lang->line('currency_symbol').' '.$orders[0]->discount; ?> </b>
              </td>
              
            </tr> -->







            <tr>

              <td colspan="4" rowspan="2" align="center" style="font-size: 18px; font-style: bold;">
               
               <b> Sub-Total </b>
              </td>

              <td  style="font-size: 16px; font-style: bold;">
               
               <b><?php 
               
              //  $total= ($totalAmount + $orders[0]->delivery_charge) - $orders[0]->discount;
              $total= ($totalAmount);
                echo $this->lang->line('currency_symbol').' '.$total; ?> </b>
              </td>
              
            </tr>

            <?php

          }
            ?>


            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->











      </div>
    </section>
    <!-- /.content -->
  </div>
