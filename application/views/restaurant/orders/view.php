<?php $this->load->view('restaurant/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('restaurant/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->lang->line('invoice'); ?> #<?php echo $order->id; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('order_list'); ?> </li>
      </ol>
    </section>
    
    <section class="invoice">
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     
     
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-home"></i> <?php echo $this->user_model->get_setting_data('site_title'); ?>
            <small class="pull-right"><?php echo $this->lang->line('date'); ?>: <?php echo $this->user_model->convert_number(date('d/m/Y',strtotime($order->date_time))); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <?php echo $this->lang->line('from'); ?>
          <address>
            <strong><?php echo $this->user_model->get_setting_data('site_title'); ?></strong><br>
            <?php echo $this->user_model->get_setting_data('site_address'); ?><br>
            Phone: <?php echo $this->user_model->get_setting_data('site_phone'); ?><br>
            Email: <?php echo $this->user_model->get_setting_data('site_email'); ?>
            <?php  $shipping_info = json_decode($order->shiping_info); ?>
            <?php if($shipping_info and $shipping_info->phone){echo 'Shipping Phone:-'.$shipping_info->phone.'<br/>';} ?>
            <?php if($shipping_info and $shipping_info->phone){echo 'Shipping Address:-'.$shipping_info->address;} ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <?php echo $this->lang->line('to'); ?>
          <address>
           <?php $customer = $this->user_model->get_user($order->customer_id); if($customer){?>
            <strong><?php echo $customer->user_name; ?></strong><br>
            <?php echo $customer->address1; ?><br>
            Phone: <?php echo $customer->phone; ?><br>
            Email: <?php echo $customer->email; ?>
            <?php } ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b><?php echo $this->lang->line('invoice'); ?> #<?php echo $order->id; ?></b><br>
          <br>
          <b>Order ID:</b> <?php echo $order->id; ?><br>
          <b><?php echo $this->lang->line('date'); ?>:</b> <?php echo $this->user_model->convert_number(date('d/m/Y',strtotime($order->date_time))); ?><br>
          <b><?php echo $this->lang->line('total_bill'); ?>:</b> <?php echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($this->order_model->invoice_items_price($order->id, $this->restaurant_id)); ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Restaurant name</th>
              <th>Qty</th>
              <th>Product</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <?php $order_items = $this->order_model->invoice_items($order->id, $this->restaurant_id); if($order_items){foreach($order_items as $item){?>
            <tr>
             <td><?php echo $this->restaurant_model->get_restaurnt_name($item->restaurant_id); ?></td>
              <td><?php echo $this->user_model->convert_number($item->qty); ?></td>
              <td><?php echo $this->product_model->product_name($item->product_id); ?></td>
              <td><?php echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($item->total_amount); ?></td>
            </tr>
            <?php }} ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table table-bordered">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($this->order_model->invoice_items_price($order->id, $this->restaurant_id)); ?></td>
              </tr>
              <tr>
                <th>Tax (0%)</th>
                <td><?php echo $this->lang->line('currency_symbol') ?> 0.00</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td><?php echo $this->lang->line('currency_symbol') ?> 0.00</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?php echo $this->lang->line('currency_symbol').' '.$this->user_model->convert_number($this->order_model->invoice_items_price($order->id, $this->restaurant_id)); ?></td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-sm-6">
          <a href="<?php echo site_url('restaurant/orders/printpreview/'.$order->id); ?>" target="_blank" class="btn btn-default"><?php echo $this->lang->line('print_button'); ?></a>
          
        </div>
        <!--<div class="col-sm-6">
          <div class="pull-right">
              <?php echo form_open('restaurant/orders/view/'.$order->id.'/'.$type); ?>
                  <div class="input-group input-group-sm">
                    <select name="status" id="" class="form-control select-custom">
                          <Option value="Placed"  <?php if($order->status=='Placed'){echo 'selected';} ?>>Placed</Option>
                          <Option value="Processing" <?php if($order->status=='Processing'){echo 'selected';} ?>>Processing</Option>
                          <Option value="Pickup" <?php if($order->status=='Pickup'){echo 'selected';} ?>>Pickup</Option>
                          <Option value="Delivered" <?php if($order->status=='Delivered'){echo 'selected';} ?>>Delivered</Option>
                      </select>
                    <span class="input-group-btn">
                      <button type="submit" name="change_status" value="submit" class="btn btn-primary btn-flat" style="margin-right: 5px;">
                        <i class="fa fa-refresh"></i> Save
                      </button>
                    </span>
                  </div>
                  
              <?php echo form_close(); ?>
          </div>
        </div>-->
      </div>
    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('restaurant/inc/footer'); ?>