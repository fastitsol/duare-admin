<?php $this->load->view('restaurant/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('restaurant/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('restaurant/products/category'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('menu_product_new_category'); ?> </a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('product_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-6">
         <?php if(isset($cat_id) and isset($single_cat)){ ?>
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('menu_product_edit_category'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('restaurant/products/category/'.$single_cat->id); ?>
            <div class="box-body">
              <div class="col-sm-12">
                    <div class="form-group">
                      <label for="category_name"><?php echo $this->lang->line('category_name'); ?></label>
                      <input type="text" name="category_name" class="form-control" id="category_name" placeholder="<?php echo $this->lang->line('category_name'); ?>" value="<?php echo $single_cat->category_name; ?>" required>
                    </div> 
                    <div class="form-group">
                      <label for="category_image">Category Image</label>
                      <img src="<?php echo $single_cat->category_image; ?>" alt="Product Image" class="img-responsive img-thumbnail" style="max-height:100px">
                      <hr>
                      <input style="margin:0;padding:0" type="file" name="category_image" class="form-control" id="size" placeholder="Category Image" >
                    </div>
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_cat" name="save_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          <?php }else{ ?>
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('menu_product_new_category'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('restaurant/products/category/'); ?>
            <div class="box-body">
              <div class="col-sm-12">
                    <div class="form-group">
                      <label for="category_name"><?php echo $this->lang->line('category_name'); ?></label>
                      <input type="text" name="category_name" class="form-control" id="category_name" placeholder="<?php echo $this->lang->line('category_name'); ?>" required>
                    </div> 
                    <div class="form-group">
                      <label for="category_image">Category Image</label>
                      <input style="margin:0;padding:0" type="file" name="category_image" class="form-control" id="size" placeholder="Category Image">
                    </div>
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_new_cat" name="save_new_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          <?php } ?>
        </div>
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('product_category_list'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th> <?php echo $this->lang->line('serial_number'); ?> </th>
                      <th> <?php echo $this->lang->line('category_name'); ?> </th>
                      <th> Category Image </th>
                      <th> <?php echo $this->lang->line('change'); ?> </th>
                    </tr>
                    <?php  $counter=0; if($categories): foreach($categories as $category): $counter++;  ?>
                    <tr>
                      <td><?php echo $this->user_model->convert_number($counter); ?></td>
                      <td><?php echo $category->category_name; ?></td>
                      <td><img src="<?php echo $category->category_image; ?> " alt="Image" class="img-responsive img-thumbnail" style="max-width:100px"></td>
                      
                      <td>
                        <div class="btn-group">
                            <a href="<?php echo site_url('restaurant/products/category/'.$category->id); ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a>
                            <!-- <a href="<?php echo site_url('restaurant/products/delete_category/'.$category->id); ?>" class="btn btn-danger" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a> -->
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; endif; ?>

                  </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('restaurant/inc/footer'); ?>
