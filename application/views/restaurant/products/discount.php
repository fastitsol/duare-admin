<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo "Discount"; ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row" style="margin-top: 50px;">


       
      <div class="col-md-6">
        
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo "Add Discount For Restaurent"; ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open('restaurant/Products/AddDiscount'); ?>
            <div class="box-body">
            		<?php
					      if($this->session->flashdata('Error')){
					      ?>
					      <div class="alert alert-danger">
					        <?php echo $this->session->flashdata('Error');?>
					        
					      </div>
					      <?php
					      }
					  ?>

					  <?php
					      if($this->session->flashdata('successfully')){
					      ?>
					      <div class="alert alert-success">
					        <?php echo $this->session->flashdata('successfully');?>
					        
					      </div>
					      <?php
					      }
					  ?>

              <div class="col-sm-12">
                    <div class="form-group">
                      <label for="category_name"><?php echo "Discount In Prescentage :"; ?></label>
                      <input type="number" name="discountValue" class="form-control" id="category_name" placeholder="<?php echo "0"; ?>"  required>
                    </div> 
              </div>



            </div>
            <div class="box-footer">
                <button type="submit"  name="save_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          
      </div>
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo "Discount "; ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<?php
					      if($this->session->flashdata('error_delete_area')){
					      ?>
					      <div class="alert alert-danger">
					        <?php echo $this->session->flashdata('error_delete_area');?>
					        
					      </div>
					      <?php
					      }
					  ?>

					  <?php
					      if($this->session->flashdata('delete_area')){
					      ?>
					      <div class="alert alert-success">
					        <?php echo $this->session->flashdata('delete_area');?>
					        
					      </div>
					      <?php
					      }
					  ?>



              <div class="col-sm-12">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th> <?php echo $this->lang->line('serial_number'); ?> </th>
                      <th> <?php echo "Discount"; ?> </th>
                      <th> <?php echo "Create At"; ?> </th>
                    </tr>
                    <?php  $i = 0; if($duscounts)
                    {
                    	foreach($duscounts as $duscount):   ?>
                    <tr>
                      <td><?php $i = $i+1; echo $i; ?></td>
                      <td><?php echo $duscount->discountValue; ?></td>
                      <td><?php echo $duscount->created_at; ?></td>
                     
                    </tr>
                    <?php endforeach; 
                }else{
                    	echo "<tr><td>No Discount</td></tr>";
                    }

                     ?>

                  </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>