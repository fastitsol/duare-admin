<?php $this->load->view('restaurant/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('restaurant/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?php echo site_url('restaurant/products/SubCategory'); ?>" class="btn btn-success"><i class="fa fa-plus-circle"></i> New Sub Category</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('product_list'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-6">
         <?php if(isset($subcat_id) and isset($single_subcat)){ ?>
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('menu_product_edit_category'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('restaurant/products/SubCategory/'.$single_subcat->id); ?>
            <div class="box-body">


              <div class="col-sm-12">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('category_name'); ?></label>
                    <select name="category_id" class="form-control select2" style="width: 100%;" required>
                     <?php if($categories){foreach($categories as $category){ 
                        $selected = null;
                        if($single_subcat->category_id==$category->id){$selected='selected="selected"';}else{$selected = null;}
                        ?>
                      <option value="<?php echo $category->id; ?>" <?php echo $selected; ?> > <?php echo $category->category_name; ?> </option>
                      <?php }} ?>
                    </select>
                  </div>
                    <div class="form-group">
                      <label for="subCategory_name">Sub Category Name</label>
                      <input type="text" name="subCategory_name" class="form-control" id="subCategory_name" placeholder="Sub Category Name" value="<?php echo $single_subcat->subCategory_name; ?>" required>
                    </div> 
                    <div class="form-group">
                      <label for="image">Sub Category Image</label>
                      <img src="<?php echo $single_subcat->image; ?>" alt="Sub Category Image" class="img-responsive img-thumbnail" style="max-height:100px">
                      <hr>
                      <input style="margin:0;padding:0" type="file" name="image" class="form-control" id="size" placeholder="Image" value="<?php echo $single_subcat->image; ?>">
                    </div>
                    
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_cat" name="save_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          <?php }else{ ?>
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> New Sub Category </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('restaurant/products/SubCategory/'); ?>
            
            <div class="box-body">
                
              <div class="col-sm-12">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('category_name'); ?></label>
                    <select name="category_id" class="form-control select2" style="width: 100%;" required>
                     <?php if($categories){foreach($categories as $category){ 
                        $selected = null;
                        if($this->input->post('category_id')==$category->id){$selected='selected="selected"';}else{$selected = null;}
                        ?>
                      <option value="<?php echo $category->id; ?>" <?php echo $selected; ?> > <?php echo $category->category_name; ?> </option>
                      <?php }} ?>
                    </select>
                  </div>
                    <div class="form-group">
                      <label for="subCategory_name">Sub Category Name</label>
                      <input type="text" name="subCategory_name" class="form-control" id="subCategory_name" placeholder="Sub Category Name" required>
                    </div> 
                    <div class="form-group">
                      <label for="image">Sub Category Image</label>
                      <input style="margin:0;padding:0" type="file" name="image" class="form-control" id="size" placeholder="Image">
                    </div>
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_new_cat" name="save_new_cat" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
          <?php } ?>
        </div>
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> <?php echo $this->lang->line('product_category_list'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                    <tbody><tr>
                      <th> <?php echo $this->lang->line('serial_number'); ?> </th>
                      <th> <?php echo $this->lang->line('category_name'); ?> </th>
                      <th> Sub Category Name </th>
                      <th> Sub Category Image </th>
                      <th> <?php echo $this->lang->line('change'); ?> </th>
                    </tr>
                    <?php  $counter=0; if($subCategories): foreach($subCategories as $subCategory): $counter++;  ?>
                    <tr>
                      <td><?php echo $this->user_model->convert_number($counter); ?></td>
                      <td><?php echo $subCategory->category_name; ?></td>
                      <td><?php echo $subCategory->subCategory_name; ?></td>
                      <td><img src="<?php echo $subCategory->image; ?> " alt="Product Image" class="img-responsive img-thumbnail" style="max-width:100px"></td>
                      
                      <td>
                        <div class="btn-group">
                            <a href="<?php echo site_url('restaurant/products/subcategory/'.$subCategory->id); ?>" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a>
                            <!-- <a href="<?php echo site_url('restaurant/products/delete_subcategory/'.$subCategory->id); ?>" class="btn btn-danger" onclick="return confirm('<?php echo $this->lang->line('worning_delete'); ?>');"><i class="fa fa-trash"></i></a> -->
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; endif; ?>

                  </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('restaurant/inc/footer'); ?>