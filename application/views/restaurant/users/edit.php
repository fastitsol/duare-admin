<?php $this->load->view('restaurant/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('restaurant/inc/sidebar'); ?>
 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $this->lang->line('profile');  ?></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('restaurant/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $this->lang->line('change'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('change'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
              <div class="col-sm-4 text-center">
               <div class="customer_left_bar">
                    <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" alt="" class="img-responsive img-thumbnail img-circle">
                   <h4><?php echo $user_data->first_name.' '.$user_data->last_name; ?></h4>
				   
				   <div class="col-sm-12">
					   <div class="update_profile_image">
							<?php echo form_open_multipart('restaurant/users/edit_photo/'); ?>
								<span class="simple_heading_upload_bar"><?php echo $this->lang->line('profile_picture_size'); ?></span>
								<div class="input-group">
									<input class="form-control" name="user_image" type="file" style="width: 100%;height: 100%;padding: 3.5px;" required>
									<label class="input-group-btn">
										<input type="submit" value="<?php echo $this->lang->line('upload_button'); ?>" class="btn btn-primary" />
									</label>
								</div>
							<?php echo form_close(); ?>
					   </div>
				   </div>
               </div>
			</div>
			<div class="col-sm-8">
			    <assaid class="customer_profile_info">
					<?php echo form_open('restaurant/users/edit_user/'); ?>
						<div class="form-group row">
							<label for="first_name" class="col-sm-3 col-form-label"><?php echo $this->lang->line('first_name'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="first_name" placeholder="<?php echo $this->lang->line('first_name'); ?>" name="first_name" type="text" value="<?php echo $user_data->first_name; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="last_name" class="col-sm-3 col-form-label"><?php echo $this->lang->line('last_name'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="last_name" placeholder="<?php echo $this->lang->line('last_name'); ?>" name="last_name" type="text" value="<?php echo $user_data->last_name; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="inputError" class="col-sm-3 col-form-label"><?php echo $this->lang->line('user_name'); ?></label>
							<div class="col-sm-9">
								<input id="inputError" class="form-control desabled"  placeholder="<?php echo $this->lang->line('user_name'); ?>" name="user_name" type="text" value="<?php echo $user_data->user_name; ?>" disabled >
							</div>
						</div>
					
						<div class="form-group row">
							<label for="type" class="col-sm-3 col-form-label"><?php echo $this->lang->line('type'); ?></label>
							<div class="col-sm-9">
								<input id="type" class="form-control desabled"  placeholder="<?php echo $this->lang->line('type'); ?>" name="type" type="text" value="<?php echo $user_data->type; ?>" disabled >
							</div>
						</div>
					
						<div class="form-group row">
							<label for="phone" class="col-sm-3 col-form-label"><?php echo $this->lang->line('phone'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="phone" placeholder="<?php echo $this->lang->line('phone'); ?>" name="phone" type="text" value="<?php echo $user_data->phone; ?>">
							</div>
						</div>
					
						<div class="form-group row">
							<label for="nid_number" class="col-sm-3 col-form-label"><?php echo $this->lang->line('nid_number'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="nid_number" placeholder="<?php echo $this->lang->line('nid_number'); ?>" name="nid_number" type="text" value="<?php echo $user_data->nid_number; ?>">
							</div>
						</div>
					
						<div class="form-group row">
							<label for="passport_number" class="col-sm-3 col-form-label"><?php echo $this->lang->line('passport_number'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="passport_number" placeholder="<?php echo $this->lang->line('passport_number'); ?>" name="passport_number" type="text" value="<?php echo $user_data->passport_number; ?>">
							</div>
						</div>
					
						<div class="form-group row">
							<label for="email" class="col-sm-3 col-form-label"><?php echo $this->lang->line('email'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="email" placeholder="<?php echo $this->lang->line('email'); ?>" name="email" type="email" value="<?php echo $user_data->email; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="birthday" class="col-sm-3 col-form-label"><?php echo $this->lang->line('birth_day'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="birthday" placeholder="<?php echo $this->lang->line('birthday'); ?>" name="birthday" type="text" value="<?php echo $user_data->birthday; ?>" data-date-format="dd/mm/yyyy">
							</div>
						</div>
						
					
						<div class="form-group row">
							<label for="address1" class="col-sm-3 col-form-label"><?php echo $this->lang->line('present_address'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="address1" placeholder="<?php echo $this->lang->line('present_address'); ?>" name="address1" type="text" value="<?php echo $user_data->address1; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="address2" class="col-sm-3 col-form-label"><?php echo $this->lang->line('permanent_address'); ?></label>
							<div class="col-sm-9">
								<input class="form-control" id="address2" placeholder="<?php echo $this->lang->line('permanent_address'); ?>" name="address2" type="text" value="<?php echo $user_data->address2; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="country" class="col-sm-3 col-form-label"><?php echo $this->lang->line('country'); ?> </label>
							<div class="col-sm-9">
								<input class="form-control" id="country" placeholder="<?php echo $this->lang->line('country'); ?>" name="country" type="text" value="<?php echo $user_data->country; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="city" class="col-sm-3 col-form-label"><?php echo $this->lang->line('city'); ?> </label>
							<div class="col-sm-9">
								<input class="form-control" id="city" placeholder="<?php echo $this->lang->line('city'); ?>" name="city" type="text" value="<?php echo $user_data->city; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="state" class="col-sm-3 col-form-label"><?php echo $this->lang->line('state'); ?> </label>
							<div class="col-sm-9">
								<input class="form-control" id="state" placeholder="<?php echo $this->lang->line('state'); ?>" name="state" type="text" value="<?php echo $user_data->state; ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="df" class="col-sm-3 col-form-label">  </label>
							<div class="col-sm-9">
								<input type="submit" name="save_user" value="<?php echo $this->lang->line('save_button'); ?>" class="btn btn-danger btn-reservation" />
								<a href="<?php echo site_url('restaurant/users/change_password/'); ?>" class="btn btn-success "> <i class="fa fa-edit"></i><?php echo $this->lang->line('change_password'); ?></a>
							</div>
						</div>
                    
					<?php echo form_close(); ?>
			    </assaid>
			</div>
           
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>