<?php $this->load->view('restaurant/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('restaurant/inc/sidebar'); ?>
 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-home"></i> <?php echo $this->lang->line('restaurant'); ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home'); ?> </a></li>
        <li class="active"><?php echo $this->lang->line('add_new_product'); ?> </li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       <?php if($restaurant){ ?>
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                 <h3 class="box-title"><?php  echo $this->lang->line('edit_restaurant'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open('restaurant/users/restaurant/'); ?>
            <div class="box-body">
              <div class="col-sm-12">
               
                <div class="form-group">
                  <label for="restaurant_name"><?php echo $this->lang->line('restaurant_name'); ?></label>
                  <input type="text" name="restaurant_name" class="form-control" id="restaurant_name" placeholder="<?php echo $this->lang->line('restaurant_name'); ?>" value="<?php echo $restaurant->name; ?>" required>
                </div>
               
                <div class="form-group">
                  <label for="phone"><?php echo $this->lang->line('phone'); ?></label>
                  <input type="text" name="phone" class="form-control" id="phone" placeholder="<?php echo $this->lang->line('phone'); ?>" value="<?php echo $restaurant->phone; ?>">
                </div>
               
                <div class="form-group">
                  <label for="email"><?php echo $this->lang->line('email'); ?></label>
                  <input type="email" name="email" class="form-control" id="email" placeholder="<?php echo $this->lang->line('email'); ?>" value="<?php echo $restaurant->email; ?>">
                </div>
               
                <div class="form-group">
                  <label for="website"><?php echo $this->lang->line('website'); ?></label>
                  <input type="text" name="website" class="form-control" id="website" placeholder="<?php echo $this->lang->line('website'); ?>" value="<?php echo $restaurant->website; ?>">
                </div>
               
                <div class="form-group">
                  <label for="delivery_time"><?php echo $this->lang->line('delivery_time'); ?></label>
                  <input type="text" name="delivery_time" class="form-control" id="delivery_time" placeholder="<?php echo $this->lang->line('delivery_time'); ?>" value="<?php echo $restaurant->delivery_time; ?>">
                </div>
               
                <div class="form-group">
                  <label for="opening_time"><?php echo $this->lang->line('opening_time'); ?></label>
                  <input type="text" name="opening_time" class="form-control" id="opening_time" placeholder="<?php echo $this->lang->line('opening_time'); ?>" value="<?php echo $restaurant->opening_time; ?>">
                </div>
               
                <div class="form-group">
                  <label for="closing_time"><?php echo $this->lang->line('closing_time'); ?></label>
                  <input type="text" name="closing_time" class="form-control" id="closing_time" placeholder="<?php echo $this->lang->line('closing_time'); ?>" value="<?php echo $restaurant->closing_time; ?>">
                </div>
                
                <div class="form-group">
                  <label for="description"><?php echo $this->lang->line('availability'); ?></label>
                    <select name="availability" class="form-control select2" id="">
                        <option value="available" <?php if($restaurant->availability=='available'){ echo 'selected';} ?> >Available</option>
                        <option value="unavailable" <?php if($restaurant->availability=='unavailable'){ echo 'selected';} ?>>Unavailable</option>
                    </select>
                </div>
                  
               
                <div class="form-group">
                  <label for="address"><?php echo $this->lang->line('address'); ?></label>
                  <input type="text" name="address" class="form-control" id="address" placeholder="<?php echo $this->lang->line('address'); ?>" value="<?php echo $restaurant->address; ?>">
                </div>
               
                
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_restaurnt" name="save_restaurnt" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
       
      <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
                 <h3 class="box-title"><?php  echo $this->lang->line('edit_restaurant'); ?> </h3>
            </div>
            <!-- /.box-header -->
            <?php echo form_open_multipart('restaurant/users/restaurant/'); ?>
            <div class="box-body">
              <div class="col-sm-12">
               
                <div class="form-group">
                  <label for="restaurant_logo"><?php echo $this->lang->line('logo'); ?></label>
                  <?php if($restaurant->logo){ ?>
                  <img style="max-height:100px" src="<?php echo site_url('uploads/restaurant/'.$restaurant->logo); ?>" alt="" class="img-responsive">
                  <?php } ?>
                  <input style="margin:0;padding:0" type="file" name="restaurant_logo" class="form-control" id="restaurant_logo" placeholder="<?php echo $this->lang->line('logo'); ?>">
                </div>
               
                <div class="form-group">
                  <label for="restaurant_cover"><?php echo $this->lang->line('cover'); ?></label>
                  <?php if($restaurant->cover){ ?>
                  <img style="max-height:100px" src="<?php echo site_url('uploads/restaurant/'.$restaurant->cover); ?>" alt="" class="img-responsive">
                  <?php } ?>
                  <input style="margin:0;padding:0" type="file" name="restaurant_cover" class="form-control" id="restaurant_cover" placeholder="<?php echo $this->lang->line('cover'); ?>">
                </div>
               
                <div class="form-group">
                  <label for="trade_licence_image"><?php echo $this->lang->line('trade_licence_image'); ?></label>
                  <?php if($restaurant->trade_licence_image){ ?>
                  <img style="max-height:100px" src="<?php echo site_url('uploads/restaurant/'.$restaurant->trade_licence_image); ?>" alt="" class="img-responsive">
                  <?php } ?>
                  <input style="margin:0;padding:0" type="file" name="trade_licence_image" class="form-control" id="trade_licence_image" placeholder="<?php echo $this->lang->line('trade_licence_image'); ?>">
                </div>
               
                
              </div>
            </div>
            <div class="box-footer">
                <button type="submit" value="save_restaurnt_files" name="save_restaurnt_files" class="btn btn-primary"> <?php echo $this->lang->line('save_button'); ?> </button>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
       
       <?php } ?>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('restaurant/inc/footer'); ?>