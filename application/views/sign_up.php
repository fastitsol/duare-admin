<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> <?php echo $this->user_model->get_setting_data('site_title'); ?> | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/custom.css">

  <?php if($this->user_model->get_setting_data('site_language')=='bn'){ ?>
    <style>
        body {
        font-family: 'SolaimanLipi','Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        }
        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{font-family: 'SolaimanLipi','Source Sans Pro',sans-serif;}
        .main-header .logo{font-family: 'SolaimanLipi',"Helvetica Neue",Helvetica,Arial,sans-serif;}
    </style>
    <?php } ?>
    
</head>
<body class="hold-transition register-page">
<div class="login-header">
   <div class="blue-effect"></div>
    <div class="login-header-bottom"></div>
</div>
<div class="register-page">
    <div class="register-box">
  <div class="register-logo">
    <a href="#"><img src="<?php echo site_url('uploads/duare-logo.png'); ?>" alt="" class="img-responsive"></a>
  </div>
  
  <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class" style="margin-top:15px"> 
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>

  <div class="register-box-body">
    <p class="login-box-msg"> Shop Sign Up </p>
    
    
 
            
            
            
    <?php echo form_open('sign_up/index'); ?>
      <div class="form-group has-feedback">
        <input name="first_name" type="text" class="form-control" placeholder="<?php echo $this->lang->line('first_name'); ?>"  value="<?php echo $this->input->post('first_name'); ?>" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="last_name" type="text" class="form-control" placeholder="<?php echo $this->lang->line('last_name'); ?>"  value="<?php echo $this->input->post('last_name'); ?>" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="user_name" type="text" class="form-control" placeholder="<?php echo $this->lang->line('user_name'); ?>" value="<?php echo $this->input->post('user_name'); ?>" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="phone" class="form-control" placeholder="<?php echo $this->lang->line('phone'); ?>"  value="<?php echo $this->input->post('phone'); ?>" required>
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="<?php echo $this->lang->line('email'); ?>"  value="<?php echo $this->input->post('email'); ?>" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="<?php echo $this->lang->line('password'); ?>" id="password"  value="<?php echo $this->input->post('password'); ?>" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        
        <span class="glyphicon glyphicon-eye-open form-control-feedback" onclick="showPassword('password', 'eye_1')" style="right:20px;pointer-events: inherit;cursor:pointer" id="eye_1"></span>
        
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="<?php echo $this->lang->line('confirm_password'); ?>"  value="<?php echo $this->input->post('confirm_password'); ?>" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        
        <span class="glyphicon glyphicon-eye-open form-control-feedback" onclick="showPassword('confirm_password', 'eye_2')" style="right:20px;pointer-events: inherit;cursor:pointer" id="eye_2"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="text" name="restaurant_name" class="form-control" placeholder="Shop name"  value="<?php echo $this->input->post('restaurant_name'); ?>" required>
        <span class="glyphicon glyphicon-home form-control-feedback"></span>
      </div>
      
      <div class="row">
        <div class="col-xs-8">
          <a href="<?php echo site_url('login'); ?>" class="btn btn-link custom_link" style="padding-left:0px"><?php echo $this->lang->line('have_account'); ?></a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button name="sign_up" value="sign_up" type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close(); ?>

    <!--<div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>-->

  </div>
  <!-- /.form-box -->
</div>
</div>

<!-- /.register-box -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo site_url('assets/admin/'); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
    
    function showPassword(box_id, obj_id) {
      var x = document.getElementById(box_id);
      var obj = document.getElementById(obj_id);
      if (x.type === "password") {
        x.type = "text";
        obj.classList.remove('glyphicon-eye-open');
        obj.classList.add('glyphicon-eye-close');
      } else {
        x.type = "password";
          
        obj.classList.add('glyphicon-eye-open');
        obj.classList.remove('glyphicon-eye-close');
      }
    } 
</script>
</body>
</html>
